﻿using BackendAPI.Helpers;
using BackendAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace BackendAPI.Services
{
    public class TransactionService
    {
        DataUtil data = new DataUtil();
        AccountService accsvc = new AccountService();
        string coreLiveToken = Startup.Configuration["Data:CORETOKEN"];

        public async Task<IEnumerable<Transaction>> getRecentTransactions(long CustomerID)
        {
            var retList = new List<Transaction>();
            var sql = String.Format(@"
        SELECT
  convert(varchar, kudiTester.DTD.posted_date, 106) AS date1
  , kudiTester.DTD.client_Tran_ref
  ,CONVERT(DECIMAL(18,2),kudiTester.DTD.tran_amount) AS amount1
  , kudiTester.DTD.part_tran_type, kudiTester.DTD.merchant
  ,kudiTester.DTD.posted_date
  ,kudiTester.DTD.value_date
  ,kudiTester.DTD.account_ID,kudiTester.DTD.sessionId
,kudiTester.DTD.name,kudiTester.DTD.charge,kudiTester.DTD.transaction_method
,kudiTester.DTD.balance, kudiTester.DTD.tran_particular
  ,Customer_Account.account_name,Customer_Account.customer_id
  ,Customer_Account.account_no
  ,Customer.address,Customer.state,Customer.city,Customer.country
FROM
  kudiTester.DTD
  INNER JOIN Customer_Account
    ON kudiTester.DTD.account_ID = Customer_Account.ID
  INNER JOIN Customer
    ON Customer_Account.customer_id = Customer.ID
WHERE
  Customer_Account.customer_id =  {0}
order by kudiTester.DTD.posted_date desc;", CustomerID);



            var result = data.GetData<Transaction>(sql);

            if(result.Rows.Count == 0)
            {
                return retList;
            }
            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Transaction()
                {
                    time = DateTime.Parse(row["posted_date"].ToString()),
                    amount = decimal.Parse(row["amount1"].ToString()),
                    sessionID = row["sessionId"].ToString(),
                    fees = (decimal)row["charge"],
                    customerID = (long)row["customer_id"],
                    paymentMethod = (int)row["transaction_method"],
                    beneficiary = row["name"].ToString(),
                    description = row["tran_particular"].ToString(),
                    destination = row["merchant"].ToString(),
                    flow = row["part_tran_type"].ToString(),
                    balance = decimal.Parse(row["balance"].ToString()).ToString(),
                    ID = i

                };
                retList.Add(temp);
            }
            // var getBalanceSQL = GetAccountBalance(temp.accountno);
            //retList.Add(temp);

            return retList;
        }

        public async Task<IEnumerable<Transaction>> getTransactions(long CustomerID, DateTime from, DateTime to)
        {
            try
            {
                var retList = new List<Transaction>();
                var sql = String.Format(@"
        SELECT
  convert(varchar, kudiTester.DTD.posted_date, 106) AS date1
  , kudiTester.DTD.client_Tran_ref
  ,CONVERT(DECIMAL(18,2),kudiTester.DTD.tran_amount) AS amount1
  , kudiTester.DTD.part_tran_type, kudiTester.DTD.merchant
  ,kudiTester.DTD.posted_date,kudiTester.DTD.sessionId
  ,kudiTester.DTD.value_date
  ,kudiTester.DTD.account_ID,kudiTester.DTD.transaction_method
,kudiTester.DTD.name,kudiTester.DTD.charge
,kudiTester.DTD.balance, kudiTester.DTD.tran_particular
  ,Customer_Account.account_name
  ,Customer_Account.account_no
  ,Customer.address,Customer.state,Customer.city,Customer.country, Customer_Account.customer_id
FROM
  kudiTester.DTD
  INNER JOIN Customer_Account
    ON kudiTester.DTD.account_ID = Customer_Account.ID
  INNER JOIN Customer
    ON Customer_Account.customer_id = Customer.ID
WHERE     kudiTester.DTD.posted_date >= {1}
  AND kudiTester.DTD.posted_date <= {2} AND
  Customer_Account.customer_id =  {0}
order by kudiTester.DTD.posted_date desc;", CustomerID, from, to);



                var result = data.GetData<Transaction>(sql);
                if (result.Rows.Count == 0)
                {
                    return retList;
                }

                for (int i = 0; i < result.Rows.Count; i++)
                {
                    var row = result.Rows[i];

                    var temp = new Transaction()
                    {
                        time = DateTime.Parse(row["posted_date"].ToString()),
                        amount = decimal.Parse(row["amount1"].ToString()),
                        sessionID = row["sessionId"].ToString(),
                        fees = (decimal)row["charge"],
                        customerID = (long)row["customer_id"],
                        paymentMethod = (int)row["transaction_method"],
                        beneficiary = row["name"].ToString(),
                        description = row["tran_particular"].ToString(),
                        flow = row["part_tran_type"].ToString(),
                        destination = row["merchant"].ToString(),
                        balance = decimal.Parse(row["balance"].ToString()).ToString(),
                        ID = i

                    };
                    retList.Add(temp);
                }
                // var getBalanceSQL = GetAccountBalance(temp.accountno);
                //retList.Add(temp);

                return retList;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public async Task<BasicResponse> setTransactionPIN(SetTransactionPINRequest req)
        {
           // var  customerid = System.Security.Claims.ClaimsPrincipal.Current.Identity.Name;
            //req.customerid = long.Parse(customerid);
            var account = await accsvc.GetAccountByCustomerID(req.customerid);

            var result = new BasicResponse();
            var request = new Svr_SetTransactionPINRequest() { kudaID = account.KudiMoneyID, pin = req.pin};
            string url = "https://kudabankcore.azurewebsites.net/api/Customers/SetTransactionPin";
            string url2 = "https://kudabankcore.azurewebsites.net/api/Utility/SetTransactionPin?kudaId=" + account.KudiMoneyID + "&pin=" + req.pin;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.GetAsync(url2))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                            //available = true;
                            string resultstring = await message.Content.ReadAsStringAsync();
                            //save result in list
                            result = JsonConvert.DeserializeObject<BasicResponse>(resultstring);
                            //string url = dm["results"][0].urls.small;
                            //return vlist;


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.message = ex.ToString();
            }
            return result;
            //return result;
        }

        public async Task<BasicResponse> changeTransactionPIN(ChangeTransactionPINRequest req1)
        {
           // var customerid = System.Security.Claims.ClaimsPrincipal.Current.Identity.Name;
            //req.customerid = long.Parse(customerid);
            var account = await accsvc.GetAccountByCustomerID(req1.customerid);

            var result = new BasicResponse();
            var request = new Svr_ChangeTransactionPINRequest();
            req1.kudaID = account.KudiMoneyID;
            request.pin = req1.pin;
            request.currentPin =req1.currentPin;
            string url1 = @"https://kudabankcore.azurewebsites.net/api/Utility/ChangeTransactionpin";
            string url2 = "https://kudabankcore.azurewebsites.net/api/Utility/ChangeTransactionpin?currentPin=" + req1.currentPin + "&kudaId=" + account.KudiMoneyID + "&pin=" + req1.pin;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11;
            try
            {

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                    var formContent = new FormUrlEncodedContent(new[]
{
                new KeyValuePair<string, string>("kudaID", account.KudiMoneyID),
                new KeyValuePair<string, string>("pin", req1.pin),
                new KeyValuePair<string, string>("currentPin", req1.currentPin)
            });
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.GetAsync(url2))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                            //available = true;Fcl
                            string resultstring = await message.Content.ReadAsStringAsync();
                            //save result in list
                            result = JsonConvert.DeserializeObject<BasicResponse>(resultstring);
                            //string url = dm["results"][0].urls.small;
                            //return vlist;


                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.message = ex.ToString();
            }
            return result;
            //return result;
        }
    }
}
