﻿using BackendAPI.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BackendAPI.Services
{
    public interface IAuthService
    {
        Task<User> Authenticate(string username, string password);
        //IEnumerable<User> GetAll();
    }
}