﻿using BackendAPI.Helpers;
using BackendAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Services
{
    public class AccountService
    {
        DataUtil data = new DataUtil();
        //GetAccount
        public Account GetAccountByID(long id)
        {
            var retList = new List<Account>();
            var sql = String.Format(@"select lastName,
	                           middleName,bvn,sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno, [Customer_Account].account_name, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [Customer] INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id  where [Customer].ID = {0};", id);


            var result = data.GetData<Account>(sql);


            var row = result.Rows[0];

            var temp = new Account()
            {
                lastName = row["lastName"].ToString(),
                firstName = row["firstName"].ToString(),
                middleName = row["middleName"].ToString(),
                ID = long.Parse(row["ID"].ToString()),
                bvn = row["bvn"].ToString(),
                sex = row["sex"].ToString(),
                accountno = row["accountno"].ToString(),
                email = row["email"].ToString(),
                phone = row["phone"].ToString(),
                accountname = row["account_name"].ToString()

            };
            //retList.Add(temp);

            return temp;
        }

        public Account GetAccountByKudaID(string kudiMoneyID)
        {
            var retList = new List<Account>();
            var sql = String.Format(@"select lastName,
	                           middleName,bvn,sex,kudiMoneyID,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno, [Customer_Account].account_name, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [Customer] INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id  where [Customer].kudiMoneyID = '{0}';", kudiMoneyID);


            var result = data.GetData<Account>(sql);


            var row = result.Rows[0];

            var temp = new Account()
            {
                lastName = row["lastName"].ToString(),
                firstName = row["firstName"].ToString(),
                middleName = row["middleName"].ToString(),
                ID = long.Parse(row["ID"].ToString()),
                bvn = row["bvn"].ToString(),
                sex = row["sex"].ToString(),
                accountno = row["accountno"].ToString(),
                email = row["email"].ToString(),
                phone = row["phone"].ToString(),
                KudiMoneyID = row["kudiMoneyID"].ToString(),
                accountname = row["account_name"].ToString(),
                balance = GetAccountBalance(row["accountno"].ToString())

            };

           // var getBalanceSQL = GetAccountBalance(temp.accountno);
            //retList.Add(temp);

            return temp;
        }

        public async Task<Account> GetAccountByCustomerID(long CustomerID)
        {
            var retList = new List<Account>();
            var sql = String.Format(@"select lastName,kudiMoneyID,
	                           middleName,bvn,sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno, [Customer_Account].account_name, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [Customer] INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id  where [Customer].ID = {0};", CustomerID);


            var result = data.GetData<Account>(sql);


            var row = result.Rows[0];

            var temp = new Account()
            {
                lastName = row["lastName"].ToString(),
                firstName = row["firstName"].ToString(),
                middleName = row["middleName"].ToString(),
                ID = long.Parse(row["ID"].ToString()),
                bvn = row["bvn"].ToString(),
                sex = row["sex"].ToString(),
                accountno = row["accountno"].ToString(),
                email = row["email"].ToString(),
                phone = row["phone"].ToString(),
                KudiMoneyID = row["kudiMoneyID"].ToString(),
                accountname = row["account_name"].ToString(),
                balance = GetAccountBalance(row["accountno"].ToString())

            };

            // var getBalanceSQL = GetAccountBalance(temp.accountno);
            //retList.Add(temp);

            return temp;
        }


        public async Task<IEnumerable<Account>> GetSubAccountsByCustomerID(long CustomerID)
        {
            var retList = new List<Account>();
            var sql = String.Format(@"select [SubAccounts].ID AS subaccountid, lastName,kudiMoneyID,
	                           middleName,bvn,sex,parentCustomerID,
                      firstName, [Customer].ID AS pid,[Customer_Account].account_no AS accountno, [Customer_Account].account_name, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [SubAccounts] INNER JOIN  [Customer]  ON [Customer].ID = [SubAccounts].ID INNER JOIN [Customer_Account]
    ON [SubAccounts].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [SubAccounts].ID= [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [SubAccounts].ID = [customerPhones].customer_id  where [SubAccounts].parentCustomerID = {0};", CustomerID);


            var result = data.GetData<Account>(sql);

            //if (result.Rows.Count == 0)
            //{
            //   retList.
            //}

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    ID = long.Parse(row["subaccountid"].ToString()),
                    bvn = row["bvn"].ToString(),
                    sex = row["sex"].ToString(),
                    accountno = row["accountno"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString(),
                    KudiMoneyID = row["kudiMoneyID"].ToString(),
                    balance = GetAccountBalance(row["accountno"].ToString()),
                    accountname = row["account_name"].ToString(),
                    parentCustomerID = long.Parse(row["parentCustomerID"].ToString())

                };
                retList.Add(temp);
            }
            // var getBalanceSQL = GetAccountBalance(temp.accountno);
            //retList.Add(temp);

            return retList;
        }

        public async Task<bool> isPrimaryAgent(string kudimoneyid)
        {
            var retList = new List<Account>();
            var CustomerID = GetAccountByKudaID(kudimoneyid).ID;
            bool isPrimaryAgent = false;
            var sql = String.Format(@"select [SubAccounts].ID AS subaccountid from [SubAccounts] where [SubAccounts].parentCustomerID = {0};", CustomerID);


            var result = data.GetData<Account>(sql);

            if (result.Rows.Count > 0)
            {
                isPrimaryAgent = true;
                
            }
            
            
            // var getBalanceSQL = GetAccountBalance(temp.accountno);
            //retList.Add(temp);

            return isPrimaryAgent;
        }







        public decimal GetAccountBalance(string accountno)
        {
            var sql = String.Format(@"
        SELECT
kudiTester.DTD.balance
FROM
  kudiTester.DTD
  INNER JOIN Customer_Account
    ON kudiTester.DTD.account_ID = Customer_Account.ID
WHERE
  Customer_Account.account_no =  {0}
order by kudiTester.DTD.posted_date desc;", accountno);


            var result = data.GetData<Account>(sql);
            decimal balance = 0;
            if (result.Rows.Count == 0)
            {
                return 0;
            }
            var row = result.Rows[0];
            balance = decimal.Parse(row["balance"].ToString());
            //retList.Add(temp);

            return balance;
        }

    }
}
