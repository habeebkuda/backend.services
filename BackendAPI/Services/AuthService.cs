﻿using BackendAPI.Helpers;
using BackendAPI.Models;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BackendAPI.Services
{
    public class AuthService: IAuthService
    {
        //public int Id { get; set; }
        //public string FirstName { get; set; }
        //public string LastName { get; set; }
        //public string Username { get; set; }
        //public string Password { get; set; }
        //public string Token { get; set; }
        AccountService accsvc = new AccountService();

        //private List<User> _users = new List<User>
        //{
        //    new User { Id = 1, FirstName = "Habeeb", LastName = "Yakubu", Username = "admin", Password = "open" }
        //};
        
        public async Task<User> Authenticate(string username, string password)
        {
            //  var user = _users.SingleOrDefault(x => x.Username == username && x.Password == password);
            var user = new User { Username = username, Password = password };
            ValidateUserResponse response = await validateUser(user);
            
            if (!response.status)
            {
                return null;
            }
            var isAgent = await accsvc.isPrimaryAgent(response.data.kudaId);
            if (!isAgent)
            {
                return null;
            }
            user.FirstName = response.data.firstName;
            user.LastName = response.data.lastName;
            user.Id = accsvc.GetAccountByKudaID(response.data.kudaId).ID;
            var Secret = Startup.Configuration["Data:SECRET"];
            // authentication successful so generate jwt token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString())
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            user.Token = tokenHandler.WriteToken(token);

            // remove password before returning
            user.Password = null;

            return user;
        }

        public async Task<ValidateUserResponse> validateUser(User user)
        {
            var result = new ValidateUserResponse();
            var request = new ValidateUserRequest() { username = user.Username, password = user.Password, grant_type = "Allow"};
            string url = "https://kudimoneyng.com/api/AccountApi/ExternalApiCheck";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {

                using (HttpClient client = new HttpClient())
                {
                   // client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, request))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                            //available = true;
                            string resultstring = await message.Content.ReadAsStringAsync();
                            //save result in list
                            result = JsonConvert.DeserializeObject<ValidateUserResponse>(resultstring);
                            //string url = dm["results"][0].urls.small;
                            //return vlist;


                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return result;
            //return result;
        }
    }
}
