﻿//using SQL.Interface;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace BackendAPI.Helpers
    {
        public class DataUtil
        {
            private static readonly string Constr  = Startup.Configuration["Data:externalContextConnectiontwo"];

            public SqlDataReader GetData(string query, SqlParameter[] param)
            {
                SqlConnection con = new SqlConnection(Constr);
                SqlCommand cmd = new SqlCommand(query, con);
                if (param != null)
                {
                    cmd.Parameters.AddRange(param);
                }
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                return dr;
            }

            /// <inheritdocs />
            public DataTable GetData<T>(string sql)
            {

                using (var connection = new SqlConnection(Constr))
                {
                    try
                    {
                        var datatable = new DataTable();

                        SqlDataAdapter sda = new SqlDataAdapter(sql, connection);
                        connection.Open();
                        sda.Fill(datatable);
                        connection.Close();
                    return datatable;
                    }
                    catch
                    {
                        CloseConnectionCheck(connection);
                        throw;
                    }
                }
            }
        private void CloseConnectionCheck(SqlConnection conn)
        {
            if (conn.State != ConnectionState.Closed)
            {
                conn.Close();
            }
        }
    }
}
