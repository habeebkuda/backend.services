﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Helpers
{
    public class ValidateUserRequest
    {
        public string username { get; set; }
        public string password { get; set; }
        public string grant_type { get; set; }
    }

    public class ValidateUserResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public UserData data { get; set; }
    }

    public class UserData
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string kudaId { get; set; }
    }

    public class GetAccountDetailsRequest
    {
        public long customerid { get; set; }
    }

    public class SetTransactionPINRequest
    {
        public long customerid { get; set; }
        public string pin { get; set; }
    }

    public class Svr_SetTransactionPINRequest
    {
        public string kudaID { get; set; }
        public string pin { get; set; }
    }
    public class ChangeTransactionPINRequest
    {
        public long customerid { get; set; }
        public string pin { get; set; }
        public string currentPin { get; set; }
        public string kudaID { get; set; }
    }

    public class Svr_ChangeTransactionPINRequest
    {
        public string kudaID { get; set; }
        public string pin { get; set; }
        public string currentPin { get; set; }
    }

    public class BasicResponse
    {
        public bool status { get; set; }
        public string message { get; set; }
        public object data { get; set; }
    }

    public class GetTransactionsRequest
    {
        public long customerid { get; set; }
        public DateTime from { get; set; }
        public DateTime to { get; set; }
    }
}
