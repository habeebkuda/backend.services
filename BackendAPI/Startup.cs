﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BackendAPI.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Cors.Internal;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace BackendAPI
{
    public class Startup
    {
        //public Startup(IConfiguration configuration)
        //{
        //    Configuration = configuration;
        //    var builder = new ConfigurationBuilder().SetBasePath(appEnv.ContentRootPath).AddJsonFile("config.json").AddEnvironmentVariables();
        //    Configuration = builder.Build();
        //}
        public static IConfigurationRoot Configuration;
        public static IHostingEnvironment hostingenv;
        //public WorldContextSeedData sd;

        public Startup(IHostingEnvironment appEnv)
        {
            var builder = new ConfigurationBuilder().SetBasePath(appEnv.ContentRootPath).AddJsonFile("appsettings.json").AddEnvironmentVariables();
            Configuration = builder.Build();
            hostingenv = appEnv;
        }

        public IConfiguration Configuration1 { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddCors(options =>
            //{
            //    options.AddPolicy("CorsPolicy",
            //        builder => builder.AllowAnyOrigin()
            //        .AllowAnyMethod()
            //        .AllowAnyHeader()
            //        .AllowCredentials());
            //});
            services.AddCors(options=>
                  options.AddPolicy("CorsPolicy",
            builder => builder.WithOrigins("http://localhost:4200", "https://kuda-agency.herokuapp.com", "https://kuda-agency-staging.herokuapp.com")
            .AllowAnyMethod()
            .AllowAnyHeader()
            .AllowCredentials())
        );
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            //services.Configure<MvcOptions>(options =>
            //{
            //    options.Filters.Add(new CorsAuthorizationFilterFactory("SiteCorsPolicy"));
            //});
            // configure strongly typed settings objects
            var appSettingsSection = Configuration.GetSection("AppSettings");
            //services.Configure<AppSettings>(appSettingsSection);

            // configure jwt authentication
            //var appSettings = appSettingsSection.Get<AppSettings>();
            var key = Encoding.ASCII.GetBytes(Configuration["Data:SECRET"]);
            services.AddAuthentication(x =>
            {
                x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(x =>
            {
                x.RequireHttpsMetadata = false;
                x.SaveToken = true;
                x.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false
                };
            });
            services.AddScoped<IAuthService, AuthService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //  app.UseAuthentication();
            app.UseAuthentication();
            app.UseCors("CorsPolicy");
            app.UseMvc();
            //app.UseAuthentication();
            //    app.UseAuthentication();

           
        }
    }
}
