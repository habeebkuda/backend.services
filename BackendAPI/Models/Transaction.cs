﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Models
{
    public class Transaction
    {
        public long ID { get; set; }
        public int paymentMethod { get; set; }
        public string beneficiary { get; set; }
        public decimal amount { get; set; }
        public decimal fees { get; set; }
        public string description { get; set; }
        public string sessionID { get; set; }
        public string flow { get; set; }
        public string destination { get; set; }
        public string balance { get; set; }
        public long customerID { get; set; }
        public DateTime time { get; set; }
    }
}
