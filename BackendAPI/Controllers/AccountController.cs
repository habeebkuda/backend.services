﻿using BackendAPI.Helpers;
using BackendAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Controllers
{
   // [EnableCors("CorsPolicy")]
    [Authorize]
    [Route("[controller]")]
    [ApiController]

    public class AccountController: ControllerBase
    {
        AccountService accsvc = new AccountService();
       // [Route("GetAccountDetails")]
        [HttpPost("getAccountDetails")]
        [Route("getAccountDetails")]
        public async Task<IActionResult> GetAccountDetails([FromBody]GetAccountDetailsRequest userParam)
        {
            var account = await accsvc.GetAccountByCustomerID(userParam.customerid);

            if (account == null)
            {
                return BadRequest(new { message = "Invalid Customer ID" });
            }

            return Ok(account);
        }

        [HttpPost("getSubAccounts")]
        [Route("getSubAccounts")]
        public async Task<IActionResult> GetSubAccounts([FromBody]GetAccountDetailsRequest userParam)
        {
            var customerid = User.Identity.Name;
            //userParam.customerid = long.Parse(customerid);
            var account = await accsvc.GetSubAccountsByCustomerID(userParam.customerid);

            if (account == null)
            {
                return BadRequest(new { message = "Invalid Parent Customer ID" });
            }

            return Ok(account);
        }
    }
}
