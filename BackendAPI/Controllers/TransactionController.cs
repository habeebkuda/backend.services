﻿using BackendAPI.Helpers;
using BackendAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackendAPI.Controllers
{
    [Authorize]
    [Route("[controller]")]
    [ApiController]
    public class TransactionController : ControllerBase
    {
        TransactionService trsvc = new TransactionService();
        // [Route("GetAccountDetails")]
        [HttpPost("getRecentTransactions")]
        [Route("getRecentTransactions")]
        public async Task<IActionResult> GetRecentTransactions([FromBody]GetAccountDetailsRequest userParam)
        {
            var account = await trsvc.getRecentTransactions(userParam.customerid);

            if (account == null)
            {
                return BadRequest(new { message = "Invalid Customer ID" });
            }

            return Ok(account);
        }

        [HttpPost("getTransactions")]
        [Route("getTransactions")]
        public async Task<IActionResult> getTransactions([FromBody]GetTransactionsRequest userParam)
        {
            var account = await trsvc.getTransactions(userParam.customerid, userParam.from,userParam.to);

            if (account == null)
            {
                return BadRequest(new { message = "Invalid Customer ID or date" });
            }

            return Ok(account);
        }

        [HttpPost("setTransactionPIN")]
        [Route("setTransactionPIN")]
        public async Task<IActionResult> SetTransactionPIN([FromBody]SetTransactionPINRequest userParam)
        {
            var account = await trsvc.setTransactionPIN(userParam);

            if (account == null)
            {
                return BadRequest(new { message = "Error with the request" });
            }

            return Ok(account);
        }

        [Route("changeTransactionPIN")]
        [HttpPost("changeTransactionPIN")]
        public async Task<IActionResult> ChangeTransactionPIN([FromBody]ChangeTransactionPINRequest userParam)
        {
            var account = await trsvc.changeTransactionPIN(userParam);

            if (account == null)
            {
                return BadRequest(new { message = "Error with the request" });
            }

            return Ok(account);
        }
    }
}
