﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.DotNet.PlatformAbstractions;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Serialization;
using Monitor.Models;
using Monitor.Services;
using Monitor.ViewModels;
using Microsoft.AspNetCore.Authentication.Cookies;

namespace Monitor
{
    public class Startup
    {
        public static IConfigurationRoot Configuration;
        public static IHostingEnvironment hostingenv;
        //public WorldContextSeedData sd;

        public Startup(IHostingEnvironment appEnv)
        {
            var builder = new ConfigurationBuilder().SetBasePath(appEnv.ContentRootPath).AddJsonFile("config.json").AddEnvironmentVariables();
            Configuration = builder.Build();
            hostingenv = appEnv;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
      options.AddPolicy("CorsPolicy",
builder => builder.WithOrigins("http://localhost:80")
.AllowAnyMethod()
.AllowAnyHeader()
.AllowCredentials())
);
            services.AddMvc().AddJsonOptions(opt =>
           {
               opt.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
           });
            services.AddIdentity<AppUser, AppRole>(config =>
            {
                config.User.RequireUniqueEmail = true;
                config.Password.RequiredLength = 8;
            }).AddEntityFrameworkStores<IdentityContext>();
            services.AddDbContext<IdentityContext>();
            services.ConfigureApplicationCookie(config =>
            {
                config.LoginPath = "/Auth/Login";
            });
            services.AddScoped<IMailService, GeneralMailService>();
            services.AddEntityFrameworkSqlServer().AddDbContext<DataContext>();
            //services.AddScoped<WorldContextSeedData>();
            services.AddScoped<IAppzoneCallRepository, AppzoneCallRepository>();
            services.AddScoped<IBulkMailRepository, BulkMailRepository>();
            services.AddScoped<IAppzoneMonitorLogic, AppzoneMonitorLogic>();
            services.AddScoped<CoordinateService>();
            services.AddLogging();

        }

        
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, ILoggerFactory loggerFactory)
        {
            app.UseCors("CorsPolicy");
            loggerFactory.AddDebug(LogLevel.Information);
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseIdentity();
            app.UseDeveloperExceptionPage();
            AuthAppBuilderExtensions.UseAuthentication(app);

            // Mapper.Initialize(config =>
            //{
            //    //config.CreateMap<Trip, TripViewModel>().ReverseMap();
            //    //config.CreateMap<Stop, StopViewModel>().ReverseMap();
            //});
            //app.UseCors("CorsPolicy");
            app.UseMvc(config =>
            {
                config.MapRoute(
                    name: "Default",
                    template: "{controller}/{action}/{id?}",
                    defaults: new { controller = "App", action = "Index" }

                );
            });
            
            //seeder.deleteData();
            
            //seeder.EnsureSeedData();
          //  seeder.EnsureSeedDataUser();

            //await Task.Run(async () =>
            //await seeder.EnsureSeedDataAsync()
            //);

        }              //if (env.IsDevelopment())
                        //{
                        //    //app.UseDeveloperExceptionPage();
                        //    app.UseStaticFiles();
                        //}

            //            app.Run(async (context) =>
            //            {
            //                var html = @"<!DOCTYPE html>
            //<html>
            //<head>
            //</head>
            //    <h2>Trippers</h2>
            //</body>
            //</html>";
            //                await context.Response.WriteAsync(html);
            //            });
        }
    }

