﻿using Monitor.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Services
{
    public interface IAppzoneMonitorLogic
    {
        Task<CreateCardRequestResponse> cardRequest_appzone(CreateCardRequestVM cr);
        Task<CreateCardRequestResponse> activatecard_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> deactivatecard_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> getcustomercard_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> unblock_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> CreateCustomerAndAccount_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> GetCustomerAccountByTrackingRef_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> AppZoneNameEnquiry_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> GetAccountTransaction_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> PlaceLien_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> UnPlaceLien_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> FreezeAccount_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> CheckFreezeStatus_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> GLToCustomer_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> CustomerToGL_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> LocalFundsTransfer_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> TransactionStatus_appzone(CreateCardRequestVM crvm);
        Task<CreateCardRequestResponse> UnFreezeAccount_appzone(CreateCardRequestVM crvm);
    }
}
