﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Monitor.Services
{
    public class CoordinateService
    {
        private ILogger<CoordinateService> _logger;

        public CoordinateService(ILogger<CoordinateService> logger)
        {
            _logger = logger;
        }

        public async Task<CoordinateServiceResult> Lookup(string location)
        {
            
            var result = new CoordinateServiceResult()
            {
                Success = false,
                Message = "Undertermined Failure while looking up coordinates"
            };

            try
            {
                var encodedName = WebUtility.UrlEncode(location);
                var BingKey = "AkwBx62kaid04r6lf4ptg5PWJAqfC1Ctg16Ph3V03a14Tedn0FrPjuFK1Q7SVf1a";
                var url = $"http://dev.virtualearth.net/REST/v1/Locations?q={encodedName}&key={BingKey} ";
                var client = new HttpClient();
                var json = await client.GetStringAsync(url);
                // Read out the results
                // Fragile, might need to change if the Bing API changes
                var results = JObject.Parse(json);
                var resources = results["resourceSets"][0]["resources"];
                if (!resources.HasValues)
                {
                    result.Message = $"Could not find '{location}' as a location";
                }
                else
                {
                    var confidence = (string)resources[0]["confidence"];
                    if (confidence != "High")
                    {
                        result.Message = $"Could not find a confident match for '{location}' as a location";
                    }
                    else
                    {
                        var coords = resources[0]["geocodePoints"][0]["coordinates"];
                        result.Latitude = (double)coords[0];
                        result.Longitude = (double)coords[1];
                        result.Success = true;
                        result.Message = "Success";
                    }
                }
                return result;
            }
            catch(Exception ex)
            {
                _logger.LogError("Communication with BING MAP API Fails" + " " + ex.Message.ToString(), ex);
                return null;
            }
        }
    }
}
