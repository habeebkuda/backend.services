﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Monitor.Models;
using Monitor.ViewModels;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace Monitor.Services
{
    public class AppzoneMonitorLogic : IAppzoneMonitorLogic
    {
        string appZoneUrlB = @"https://api.mybankone.com/thirdpartyapiservice/apiservice/";
        string etrazanctURL = @"https://kudabankcore.azurewebsites.net/api/EtranzactIntegration/GetBalance";
        string appZoneUrlA = @"https://api.mybankone.com/BankOneWebAPI/";
        string coreToken = @"HEY2yw1SrRgBdLtDeWFNy2ufkTcc0MwYSJ4bdJXE+QfftP1EM/VC9sCmMlOAsj4K2UelgDEseUQDb3JInEtRZqdVJx6Dv1hl3MOL/fkQUJo=";
        string coreToken1 = @"LVzfdhS0WacZX8EQC/C2brSEA4xbkouevFRgcOkksCGnkZe0TCTGI2UPnSmWcHWnB9ERoonq49TtuqLyOfKObtB/NKcsNQ4VpsoS+X1gUO4=";
        string coreLiveToken = @"DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=";
        private ILogger<AppzoneMonitorLogic> _logger;
        private IAppzoneCallRepository _repository;
        private UserManager<AppUser> _usermgr;
        private ExternalDataContext data = new ExternalDataContext();

        public AppzoneMonitorLogic(IAppzoneCallRepository repository, ILogger<AppzoneMonitorLogic> logger, UserManager<AppUser> usermgr = null)
        {
            _logger = logger;
            _repository = repository;
            _usermgr = usermgr;
        }
        public async Task<CreateCardRequestResponse> cardRequest_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://api.mybankone.com/thirdpartyapiservice/apiservice/Cards/RequestCard";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("RequestCard", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            // return;
                            if (!available)
                            {
                                mailer.Send2("habeeb@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "RequestCard" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> activatecard_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://api.mybankone.com/thirdpartyapiservice/apiservice/Cards/ActivateCard";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("ActivateCard", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("habeeb@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "ActivateCard" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                        {
                            // return;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> deactivatecard_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://api.mybankone.com/thirdpartyapiservice/apiservice/Cards/Hotlist";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("Hotlist", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("habeeb@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "Hotlist" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> getcustomercard_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://api.mybankone.com/thirdpartyapiservice/apiservice/Cards/GetCustomerCards";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("GetCustomerCards", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {

                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "GetCustomerCards" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> unblock_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://api.mybankone.com/thirdpartyapiservice/apiservice/Cards/UnBlock";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("Unblock", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "Unblock" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> CreateCustomerAndAccount_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = "https://api.mybankone.com/BankOneWebAPI/api/Account/CreateCustomerAndAccount/2?authtoken=" + cr.Token;
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    //var myRequest = (HttpWebRequest)WebRequest.Create(url);

                    //var response = (HttpWebResponse)myRequest.GetResponse();
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode || message.StatusCode == HttpStatusCode.InternalServerError) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("CreateCustomerAndAccount", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "CreateCustomerAndAccount" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> GetCustomerAccountByTrackingRef_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://api.mybankone.com/BankOneWebAPI/" + "api/Account/" +
     "GetAccountByTransactionTrackingRef/2?authtoken=" + cr.Token + "&transactionTrackingRef=" + cr.trackingRef + "&mfbCode=" + cr.mfbCode;
                //var myRequest = (HttpWebRequest)WebRequest.Create(url);

                //var response = (HttpWebResponse)myRequest.GetResponse();
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.GetAsync(url))
                    {
                        if (message.IsSuccessStatusCode || message.StatusCode == HttpStatusCode.NotFound) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("GetCustomerAccountByTrackingRef", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {

                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "GetCustomerAccountByTrackingRef" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> AppZoneNameEnquiry_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "account/accountEnquiry";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("AppZoneNameEnquiry", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "AppZoneNameEnquiry" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> GetAccountTransaction_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlA + "api/Account/" +
     "GetTransactions/2?authtoken=" + cr.Token + "&accountNumber=" + cr.AccountNumber + "&institutionCode=" + cr.mfbCode + "&numberOfItems=" + 1;
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.GetAsync(url))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("GetAccountTransaction", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "GetAccountTransaction" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> PlaceLien_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "Account/PlaceLien";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("PlaceLien", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {

                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "PlaceLien" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> UnPlaceLien_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "Account/UnPlaceLien";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("UnPlaceLien", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "UnPlaceLien" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> FreezeAccount_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "Account/FreezeAccount";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("FreezeAccount", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone")) { }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> UnFreezeAccount_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "Account/UnFreezeAccount";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("UnFreezeAccount", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "UnFreezeAccount" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> CheckFreezeStatus_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "Account/CheckFreezeStatus";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("CheckFreezeStatus", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {

                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "CheckFreezeStatus" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> GLToCustomer_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "transactions/credit";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("Credit", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {

                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "Credit" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> CustomerToGL_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "transactions/debit";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("Debit", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "Debit" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> LocalFundsTransfer_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "Transfer/LocalFundsTransfer";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("Transfer", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "Appzone"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "Transfer" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<CreateCardRequestResponse> TransactionStatus_appzone(CreateCardRequestVM cr)
        {
            var result = new CreateCardRequestResponse();
            cr.Token = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";
            cr.CardProfileBIN = "506184081";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = appZoneUrlB + "transactions/TransactionStatusQuery";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.ResponseStatus = statuscode.ToString();
                        result.ResponseDescription = message.ReasonPhrase;

                        if (saveResponse("TransactionStatusQuery", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "AppZone"))
                        {

                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "TransactionStatusQuery" + " " + "at" + " " + "Appzone", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.RequestStatus = false;
                result.ResponseDescription = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        /* ZenithBank*/

        public void checkZenithBank(string url, string endpoint)
        {
            saveResponse(endpoint, url, isServiceUp(url).ToString(), isServiceUp(url).ToString(), (isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503) ? "False" : "True", "SOAP", "ZenithBank");
            if ((isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503))
            {
                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + endpoint + "at" + " " + "ZenithBank", url, "Habeeb", "Yakubu", "Template");
            }
        }

        public void checkEtranzact(string url, string endpoint)
        {
            saveResponse(endpoint, url, isServiceUp(url).ToString(), isServiceUp(url).ToString(), (isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503 || isServiceUp(url) == 502) ? "False" : "True", "SOAP", "Etranzact");
            if ((isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503 || isServiceUp(url) == 502))
            {
                mailer.Send2("alert@kudabank.com", "tech@kudabank.com", "", "Service Down:" + " " + endpoint + "at" + " " + "Etranzact", url + " " + ":" + " " + isServiceUp(url), "Habeeb", "Yakubu", "Template");
            }
        }

        public async Task<PayBillResponse> checkEtranzactBalance()
        {
            var result = new PayBillResponse();
            var request = new TempRequest() { name = "Get Balance" };
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                //https://kudabankcore.azurewebsites.net/api/EtranzactIntegration/VirtualTopUp
                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token", coreToken);
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(etrazanctURL, request))
                    {
                        if (message.IsSuccessStatusCode)
                        {

                            //available = true;
                            result = await message.Content.ReadAsAsync<PayBillResponse>();
                            if (result.ResponseCode != "0")
                            {
                                available = true;
                            }
                            saveResponse("GetBalance", etrazanctURL, result.ResponseCode, result.ResponseMessage, available.ToString(), JsonConvert.SerializeObject("GetBalance"), "Etranzact");
                            //if(int.Parse(result.ResponseMessage) <= 50000)
                            //{
                            //    mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Very Low Balance:" + " " + result.ResponseMessage + " " + "at" + " " + "ETRANZACT", "", "Habeeb", "Yakubu", "Template");
                            //}

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.IsSuccessFul = false;
                result.ResponseMessage = "Error confirming availability" + " " + ex.ToString();
                saveResponse("GetBalance", etrazanctURL, result.ResponseCode, result.ResponseMessage, available.ToString(), JsonConvert.SerializeObject("GetBalance"), "Etranzact");

                // return result;
            }
            return result;
            //return result;
        }

        public void checkGTBank(string url, string endpoint)
        {
            saveResponse(endpoint, url, isServiceUp(url).ToString(), isServiceUp(url).ToString(), (isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503) ? "False" : "True", "SOAP", "GTBank");
            if ((isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503))
            {
                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + endpoint + " " + "at" + " " + "GTBank", url, "Habeeb", "Yakubu", "Template");
            }
        }

        public void checkInfoBip(string url, string endpoint)
        {
            saveResponse(endpoint, url, isServiceUp(url).ToString(), isServiceUp(url).ToString(), (isServiceUp(url) == -1 || isServiceUp(url) == 503) ? "False" : "True", "SOAP", "InfoBip");
            if ((isServiceUp(url) == -1 || isServiceUp(url) == 503))
            {
                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + endpoint + " " + "at" + " " + "InfoBip", url, "Habeeb", "Yakubu", "Template");
            }

        }

        public async Task<AccessFundsTransferResp> fundsTransfer(AccessTransferRequest o, string baseUrl, string endPoint)
        {
            AccessFundsTransferResp result = new AccessFundsTransferResp();
            bool available = false;
            int statuscode = 299;
            o.auditId = DateTime.Now.ToString();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            string url = baseUrl + endPoint;

            try
            {



                // var response = await client.PostAsJsonAsync(url, o);

                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, o))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;

                        if (saveResponse(endPoint, url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(o), "AccessBank"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + endPoint + " " + "at" + " " + "AccessBank", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.message = "Cannot complete transaction at this time" + " " + e.ToString();
                saveResponse(endPoint, url, e.ToString(), "", available.ToString(), JsonConvert.SerializeObject(o), "AccessBank");

            }


            return result;
        }

        public async void getTransactionStatus(AccessGetTransactionStatusRequest o, string baseUrl, string endPoint)
        {
            AccessFundsTransferResp result = new AccessFundsTransferResp();
            bool available = false;
            int statuscode = 299;
            o.auditId = DateTime.Now.ToString();
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string url = baseUrl + endPoint;
            try
            {



                // var response = await client.PostAsJsonAsync(url, o);

                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, o))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;

                        if (saveResponse(endPoint, url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(o), "AccessBank"))
                        {
                            if (!available)
                            {
                                mailer.Send2("alert@kudabank.com", "tech@kudabank.com", "", "Service Down:" + " " + endPoint + " " + "at" + " " + "AccessBank", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }

                }
            }
            catch (Exception e)
            {
                result.success = false;
                result.message = "Cannot complete transaction at this time" + " " + e.ToString();
                saveResponse(endPoint, url, e.ToString(), "", available.ToString(), JsonConvert.SerializeObject(o), "AccessBank");

            }
        }

        public async Task<OperationsResponse<object>> kudabank_APILoginV2()
        {
            var result = new OperationsResponse<object>();
            ApiLoginViewModel2 cr = new ApiLoginViewModel2();
            cr.username = @"damilola.olowo@kudimoney.com";
            cr.password = @"simisola2!S";
            cr.grant_type = "Test";
            RegisterDevicePrintDTO regdevice = new RegisterDevicePrintDTO();
            regdevice.deviceName = "iPhone 6";
            regdevice.deviceId = @"3A0AA383-5530-4724-ACA1-1AEF6A446885";
            cr.device = regdevice;
            cr.LoginType = LoginType.UserIdentity;
            cr.FMCToken = @"eY144mCEw3w:APA91bGgtg420VKTPO0nFjgcRZn22yDR6PoHJY2_tpY1sxvhX3NYh4t1lMrKApdWAfpbVvRa55aE-FyFl50bIQ622zDJZylhGXEo7FzhNibxyKHz3LBJmdHlBArs0fQex8ijbN1nKO1X";
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://kudimoneyng.com/api/ClientsApi/APILoginV2";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, cr))
                    {
                        if (message.IsSuccessStatusCode) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.status = true;
                        // = statuscode.ToString();
                        result.message = message.ReasonPhrase;

                        if (saveResponse("APILoginV2", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject(cr), "Kudabank"))
                        {
                            if (!available)
                            {
                                mailer.Send2("alert@kudabank.com", "tech@kudabank.com", "", "Service Down:" + " " + "APILoginV2" + " " + "at" + " " + "Kudabank", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.message = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<OperationsResponse<object>> kudabank_GetBankDetailsV2()
        {
            var result = new OperationsResponse<object>();
            int statuscode = 299;
            bool available = false;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                string url = @"https://kudimoneyng.com/api/ClientsApi/GetBankDetailsV2";
                using (HttpClient client = new HttpClient())
                {
                    // client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(NotificationAuthKey, NotificationAuthValue);
                    using (HttpResponseMessage message = await client.GetAsync(url))
                    {
                        if (message.IsSuccessStatusCode || (int)message.StatusCode == 401) { available = true; }

                        statuscode = (int)message.StatusCode;
                        result.status = true;
                        // = statuscode.ToString();
                        result.message = message.ReasonPhrase;

                        if (saveResponse("GetBankDetailsV2", url, statuscode.ToString(), message.ReasonPhrase, available.ToString(), JsonConvert.SerializeObject("Test"), "Kudabank"))
                        {
                            if (!available)
                            {
                                mailer.Send2("tech@kudabank.com", "alert@kudabank.com", "", "Service Down:" + " " + "GetBankDetailsV2" + " " + "at" + " " + "Kudabank", url, "Habeeb", "Yakubu", "Template");
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                result.status = false;
                result.message = "Error confirming availability" + " " + ex.ToString();
                return result;
            }
            return result;
        }

        public async Task<List<VerificationDetail>> getPendingVerifications()
        {
            var result = new VerificationDetail();
            var request = new TempRequest() { name = "" };
            string url = "https://kudabankcore.azurewebsites.net/api/Utility/GetDocuments";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            List<VerificationDetail> vlist = new List<VerificationDetail>();
            try
            {

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, request))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                            //available = true;
                            string resultstring = await message.Content.ReadAsStringAsync();
                            //save result in list
                            vlist = JsonConvert.DeserializeObject<List<VerificationDetail>>(resultstring);
                            //string url = dm["results"][0].urls.small;
                            //return vlist;


                        }
                    }
                }
                /*  VerificationDetail vd = new VerificationDetail() { firstname = "Ezekel", lastname = "Adeleye", middlename = "Iyanu", id_type = "NIN", documentUrl = "http://filestore/ninezekiel.png" };
                  VerificationDetail vd2 = new VerificationDetail() { firstname = "Ezekel", lastname = "Adeleye", middlename = "Iyanu", id_type = "NIN", documentUrl = "http://filestore/ninezekiel.png" };
                  vlist.Add(vd);
                  vlist.Add(vd2); */
                //  return vlist;
            }
            catch (Exception ex)
            {
            }
            return vlist;
            //return result;
        }

        //public async Task<List<DisputeTransactionVM>> getDisputeTransactions()
        //{
        //    var result = new DisputeTransactionVM();
        //    var request = new TempRequest() { name = "" };
        //    string url = "https://kudabankcore.azurewebsites.net/api/Utility/GetDocuments";
        //    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
        //    List<DisputeTransactionVM> vlist = new List<DisputeTransactionVM>();
        //    try
        //    {

        //        //using (HttpClient client = new HttpClient())
        //        //{
        //        //    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
        //        //    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
        //        //    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, request))
        //        //    {
        //        //        if (message.IsSuccessStatusCode)
        //        //        {
        //        //            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
        //        //            //available = true;
        //        //            string resultstring = await message.Content.ReadAsStringAsync();
        //        //            //save result in list
        //        //            vlist = JsonConvert.DeserializeObject<List<DisputeTransactionVM>>(resultstring);
        //        //            //string url = dm["results"][0].urls.small;
        //        //            //return vlist;


        //        //        }
        //        //    }
        //        //}
        //        DisputeTransactionVM vd = new DisputeTransactionVM() { AccountNo = "1100000190", SessionID = "090239330339233", Time = DateTime.Now.AddDays(-1), TransactionRef = "40506969605", ID = 1 };
        //        DisputeTransactionVM vd2 = new DisputeTransactionVM() { AccountNo = "1100000190", SessionID = "090239330339233", Time = DateTime.Now.AddDays(-2), TransactionRef = "40506969605", ID = 2 };
        //        vlist.Add(vd);
        //        vlist.Add(vd2);
        //        //  return vlist;
        //    }
        //    catch (Exception ex)
        //    {
        //    }
        //    return vlist;
        //}

        public async Task<List<DisputeTransactionVM>> getDisputeTransaction(long id)
        {
            var result = new DisputeTransactionVM();
            var request = new TempRequest() { name = "" };
            string url = "https://kudabankcore.azurewebsites.net/api/Utility/GetDocuments";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            List<DisputeTransactionVM> vlist = new List<DisputeTransactionVM>();
            try
            {

                //using (HttpClient client = new HttpClient())
                //{
                //    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                //    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                //    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, request))
                //    {
                //        if (message.IsSuccessStatusCode)
                //        {
                //            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                //            //available = true;
                //            string resultstring = await message.Content.ReadAsStringAsync();
                //            //save result in list
                //            vlist = JsonConvert.DeserializeObject<List<DisputeTransactionVM>>(resultstring);
                //            //string url = dm["results"][0].urls.small;
                //            //return vlist;


                //        }
                //    }
                //}
               // DisputeTransactionVM vd = new DisputeTransactionVM() { AccountNo = "1100000190", SessionID = "090239330339233", Time = DateTime.Now.AddDays(-1), TransactionRef = "40506969605", ID = 1 };
                DisputeTransactionVM vd2 = new DisputeTransactionVM() { AccountNo = "1100000190", SessionID = "090239330339233", Time = DateTime.Now.AddDays(-2), TransactionRef = "40506969605", ID = 2 };
                //vlist.Add(vd);
                vlist.Add(vd2);
                //  return vlist;
            }
            catch (Exception ex)
            {
            }
            return vlist;
            //return result;
        }

        public static HttpWebRequest CreateWebRequest(string url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            webRequest.Headers.Add(@"SOAP:Action");
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        GeneralMailService mailer = new GeneralMailService();
        private int isServiceUp(string url)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
                request.Method = "POST";
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                response.Close();
                return (int)response.StatusCode;
            }
            catch (System.Net.WebException ex)
            {
                HttpWebResponse errorResponse = ex.Response as HttpWebResponse;
                return (int)errorResponse.StatusCode;
                //Any exception will returns false.
                //mailer.Send2("habeeb@kudabank.com", "habeeb@kudabank.com", "Exception Report", "Monitor Application Report", ex.ToString(), "Habeeb", "Yakubu", "Template");
                //return ex.;
            }
            catch (Exception ex)
            {
                // mailer.Send2("habeeb@kudabank.com", "habeeb@kudabank.com", "Exception Report", "Monitor Application Report", ex.ToString(), "Habeeb", "Yakubu", "Template");
                return 404;
            }
        }

        public bool saveResponse(string endpoint, string URL, string responsecode, string responsedescription, string availability, string request, string vendor)
        {
            var appzonecall = _repository.GetResponseByEndPoint(endpoint);
            if (appzonecall == null)
            {
                AppzoneCalls appzone = new AppzoneCalls()
                {
                    Request = request,
                    URL = URL,
                    Available = availability,
                    EndPoint = endpoint,
                    ResponseCode = responsecode,
                    LastUpdated = DateTime.Now,
                    Vendor = vendor,
                    ResponseMessage = responsedescription


                };
                _repository.AddResponse(appzone);
            }

            else
            {
                //update instead
                appzonecall.Available = availability;
                appzonecall.ResponseCode = responsecode;
                appzonecall.ResponseMessage = responsedescription;
                appzonecall.LastUpdated = DateTime.Now;
                appzonecall.URL = URL;
            }

            if (_repository.SaveAll())
            {
                return true;

            }
            return false;
        }

        public async Task<IActionResult> DoRequest()
        {
            //AppzoneMonitorLogic lg = null;
            CreateCardRequestVM crvm = new CreateCardRequestVM()
            {
                AccountNumber = "1234567890"

            };

            await activatecard_appzone(crvm);
            await deactivatecard_appzone(crvm);
            await getcustomercard_appzone(crvm);
            await unblock_appzone(crvm);
            await CreateCustomerAndAccount_appzone(crvm);
            await GetCustomerAccountByTrackingRef_appzone(crvm);
            await AppZoneNameEnquiry_appzone(crvm);
            await GetAccountTransaction_appzone(crvm);
            await PlaceLien_appzone(crvm);
            await UnPlaceLien_appzone(crvm);
            await FreezeAccount_appzone(crvm);
            await UnFreezeAccount_appzone(crvm);
            await CheckFreezeStatus_appzone(crvm);
            await GLToCustomer_appzone(crvm);
            await CustomerToGL_appzone(crvm);
            await LocalFundsTransfer_appzone(crvm);
            await TransactionStatus_appzone(crvm);
            var results = await cardRequest_appzone(crvm);
            return new ObjectResult(results);
        }

        public async Task<IActionResult> DoKudabankRequest()
        {
            await kudabank_GetBankDetailsV2();
            var results = await kudabank_APILoginV2();
            return new ObjectResult(results);
        }

        public void DoZenithBankRequest()
        {
            checkZenithBank(@"https://webservicestest.zenithbank.com:8443/CIBPaymentUpload/TransmitPay.svc?wsdl", "TransferRequest");
            checkZenithBank(@"https://webservicestest.zenithbank.com:8443/CIBPaymentUpload/TransmitPay.svc?wsdl", "FetchRequest");
        }
        public async Task<GenericResponse> DoEtranzactRequest()
        {
            checkEtranzactBalance();
            checkEtranzact(@"https://www.etranzact.net/FGate/ws?wsdl", "PayBill");
            checkEtranzact(@"https://www.etranzact.net/FGate/ws?wsdl", "TransactionStatus");
            checkEtranzact(@"https://www.etranzact.net/FGate/ws?wsdl", "VirtualTopUp");
            checkEtranzact(@"https://www.etranzact.net/FGate/ws?wsdl", "BillersList");
            checkEtranzact(@"https://www.etranzact.net/FGate/ws?wsdl", "FundsTransfer");
            return new GenericResponse() { IsSuccessFul = true };

        }

        public void DoGTBRequest()
        {
            checkGTBank(@"http://gtweb.gtbank.com/Gaps_FileUploader/FileUploader.asmx", "SingleTransfers");
            checkGTBank(@"http://gtweb.gtbank.com/Gaps_FileUploader/FileUploader.asmx", "TransactionRequery");
        }

        public async Task<IActionResult> DoAccessBankRequest()
        {
            string url = @"https://api.accessbankplc.com/sandbox/imto/";
            saveResponse("bankAccountFT", url, isServiceUp(url).ToString(), isServiceUp(url).ToString(), (isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503) ? "False" : "True", "Test", "AccessBank");
            saveResponse("getBankFTStatus", url, isServiceUp(url).ToString(), isServiceUp(url).ToString(), (isServiceUp(url) == -1 || isServiceUp(url) == 404 || isServiceUp(url) == 503) ? "False" : "True", "Test", "AccessBank");
            //saveResponse("bankAccountFT", @"https://api.accessbankplc.com/sandbox/imto/", HttpStatusCode.OK.ToString(), "200", "True", JsonConvert.SerializeObject("Payload"), "AccessBank");
            //saveResponse("getBankFTStatus", @"https://api.accessbankplc.com/sandbox/imto/", HttpStatusCode.OK.ToString(), "200", "True", JsonConvert.SerializeObject("Payload"), "AccessBank");
            AccessTransferRequest trq = new AccessTransferRequest();
            AccessGetTransactionStatusRequest statreq = new AccessGetTransactionStatusRequest();
            getTransactionStatus(statreq, @"https://api.accessbankplc.com/sandbox/imto/", "getBankFTStatus");
            var results = await fundsTransfer(trq, @"https://api.accessbankplc.com/sandbox/imto/", "bankAccountFT");
            return new ObjectResult(results);
        }

        public void DoInfoBipRequest()
        {
            checkInfoBip(@"https://x4q5g.api.infobip.com/", "SendSMS");
        }

        private IdentityContext identityctx = new IdentityContext();
        private UserManager<AppUser> usermgr;

        public async Task seedUser()
        {
            // if (await usermgr.FindByEmailAsync("habeeb@kudabank.com") == null)
            //  {

            var newuser = new AppUser()
            {
                UserName = "habeeb",
                Email = "habeeb@kudabank.com"
            };
            await usermgr.CreateAsync(newuser, "P@ssw0rd!");

            //}

        }

        //public async Task<object> GetPassportUrl()
        //{
        //    var user = data.


        //    }

    }
}