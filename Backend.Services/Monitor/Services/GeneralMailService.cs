﻿using Microsoft.AspNetCore.Hosting.Internal;
using Microsoft.AspNetCore.Mvc;
using Monitor.Models;
using Monitoring.Services;
using SendGrid;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Monitor.Services
{
    public class GeneralMailService : IMailService
    {
        private ExternalDataContext externalctx = new ExternalDataContext();
        private ExternalDataContext2 externalctx2 = new ExternalDataContext2();
        private DataContext ctx = new DataContext();
        AccountManagement accmgmt = new AccountManagement();
        public bool SendMail(string to, string from, string subject, string body)
        {
            // throw new NotImplementedException();

            Debug.WriteLine($"Sending mail: To : {to}, Subject: {subject}");
            return true;
        }

        public Task Send(string to, string from, string alias, string subject, string message, string receiverfirstname, string receiverlastname, string template)        {            var myMessage = new SendGridMessage();            myMessage.AddTo(to);            myMessage.From = new MailAddress(from, alias);            myMessage.Subject = subject;
            //myMessage.AddAttachment()
            if (message != null)
            {
                myMessage.Text = message;
            }            else if (template !=null)
            {
               // myMessage.Html = readhtmlfile().Replace("##N1##", receiverfirstname);
                myMessage.Html = template.Replace("##N1##", receiverfirstname);
            }
            // var apiKey = new NetworkCredential(ConfigurationManager.AppSettings["sendgridKey"]);
            var credentials = new NetworkCredential(
             Startup.Configuration["AppSetting:mailAccount"],
             Startup.Configuration["AppSetting:mailPassword"]
);            var transportWeb = new SendGrid.Web(credentials);            if (transportWeb != null)            {                return transportWeb.DeliverAsync(myMessage);            }            else            {                Trace.TraceError("Failed to create Web transport.");                return Task.FromResult(0);            }        }

        public Task SendToMultipleAddresses(List<string> to, string from, string alias, string subject, string message, string receiverfirstname, string receiverlastname, string template)        {            var myMessage = new SendGridMessage();            myMessage.AddTo(to);            myMessage.From = new MailAddress(from, alias);            myMessage.Subject = subject;
            //myMessage.AddAttachment()
            if (message != null)
            {
                myMessage.Text = message;
            }            else if (template != null)
            {
                // myMessage.Html = readhtmlfile().Replace("##N1##", receiverfirstname);
                myMessage.Html = template.Replace("##N1##", receiverfirstname);
            }
            // var apiKey = new NetworkCredential(ConfigurationManager.AppSettings["sendgridKey"]);
            var credentials = new NetworkCredential(
             Startup.Configuration["AppSetting:mailAccount"],
             Startup.Configuration["AppSetting:mailPassword"]
);            var transportWeb = new SendGrid.Web(credentials);            if (transportWeb != null)            {                return transportWeb.DeliverAsync(myMessage);            }            else            {                Trace.TraceError("Failed to create Web transport.");                return Task.FromResult(0);            }        }


        public Task Send2(string to, string from, string alias, string subject, string message, string receiverfirstname, string receiverlastname, string template)        {            var myMessage = new SendGridMessage();            myMessage.AddTo(to);            myMessage.From = new MailAddress(from, alias);            myMessage.Subject = subject;            myMessage.Text = message;            //if (template != "Template")
            //{
            //    myMessage.Html = readhtmlfile().Replace("##N1##", receiverfirstname);
            //}
            // var apiKey = new NetworkCredential(ConfigurationManager.AppSettings["sendgridKey"]);
            var credentials = new NetworkCredential(
             Startup.Configuration["AppSetting:mailAccount"],
             Startup.Configuration["AppSetting:mailPassword"]
);            var transportWeb = new SendGrid.Web(credentials);            if (transportWeb != null)            {                return transportWeb.DeliverAsync(myMessage);            }            else            {                Trace.TraceError("Failed to create Web transport.");                return Task.FromResult(0);            }        }

        public  string readhtmlfile(string templatepath)        {
            //  string path = "C:\\Users\\damil\\desktop\\backup 2\\KudiBank\\KudiBank\\Logic\\MailTemplate.txt";
            //string filpath = HostingEnvironment.("~/");
            //new Logger().LogError("script", "get file", "started");
            //filpath += fileLocation;
            // Startup.Configuration["filePath"]
            // new Logger().LogError("script", "get file", filpath);
            //MailTemplate/mailTemp.txt;
            try            {
                // var file = Startup.Configuration["Data:filePath"];
                //var file = @"C:\Users\Habeeb\Documents\mailTemp.txt";
                //var file = Startup.Configuration["AppSetting:filePath"];                var file2 = Startup.hostingenv.WebRootPath + "/" + templatepath ;                Uri uriAddress2 = new Uri(@file2);                //string readText1 = System.IO.File.ReadAllText(file);                if (System.IO.File.Exists(uriAddress2.LocalPath))                {                    //new Logger().LogError("script", "get file", "file exist");                    string readText = System.IO.File.ReadAllText(uriAddress2.LocalPath);                    return readText;                }                else                {

                    // Console.WriteLine("File not found");
                    return null;                }            }            catch (Exception e)            {                //new Logger().LogError("script", "get file", e.ToString());                return null;            }

            // Console.WriteLine("File.Opentext Done - Neel wrote it");



            //  Console.ReadKey();
        }

        public async void BulkSend(string from, string alias, string subject, string message, string template)
        {
            try {
                // var Customers = externalctx.Clients.OrderBy(t => t.ID).ToList();
                //var Customerstest = externalctx.Clients.Where(t=> t.UserName== "omostcomputer@gmail.com").ToList();
                var nokuda = externalctx.NoKudaAccount.Where(n => n.Email != null).ToList();
                // var Customers = externalctx.Clients.OrderBy(t => t.UserName == @"thisishabeeb@gmail.com").ToList();
                // int total = Customers.Count;
                int total = nokuda.Count;
                int sent = 0;
                foreach (NoKudaAccount customer in nokuda)
                {

                    if (ctx.SentItems.Where(t => t.Subject == subject && t.To == customer.Email).FirstOrDefault() == null)
                    {
                        Send(customer.Email, from, alias, subject, message, customer.FirstName, customer.FirstName, template);
                        ctx.SentItems.Add(new SentItems { Alias = alias, From = from, Message = message, Subject = subject, Time = DateTime.Now, To = customer.Email });
                        ctx.SaveChanges();
                        sent++;
                    }
                    else { continue; }
                }

                //  await Send2(from, @"help@kudabank.com" , "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from  + "Total:" + " " + total + "|" + "Sent:" + "|" + sent,, "Admin", "Admin", "Template");
                //   await Send2(@"habeeb@kudabank.com", @"help@kudabank.com", "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from, "Total:" + " " + total + "|" + "Sent:" + "|" + sent, "Admin", "Admin", "Template");
                return;
            }            catch(Exception ex)
            {
                return;
            }            }

        public async void BulkSendToKudaAccounts(string from, string alias, string subject, string message, string template)
        {
            try
            {
                // var Customers = externalctx.Clients.OrderBy(t => t.ID).ToList();
                //var Customerstest = externalctx.Clients.Where(t=> t.UserName== "omostcomputer@gmail.com").ToList();
                //var kuda = externalctx.KudaAccount.Where(n => n.Email != null).ToList();
                var kuda = accmgmt.GetAccounts();
                // var Customers = externalctx.Clients.OrderBy(t => t.UserName == @"thisishabeeb@gmail.com").ToList();
                // int total = Customers.Count;
                int total = kuda.Count(x=>x.accountno!=null);
                int sent = 0;
                string firstname_txt = "";
                //var kuda1 = kuda.Where(x => x.accountno == "1100000190").ToList();
                foreach (Account customer in kuda)
                {

                    if (ctx.SentItems.Where(t => t.Subject == subject && t.To == customer.email).FirstOrDefault() == null)
                    {
                         firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.firstName.ToLower());
                        await Send(customer.email, from, alias, subject, message, firstname_txt, firstname_txt, template);
                        ctx.SentItems.Add(new SentItems { Alias = alias, From = from, Message = message, Subject = subject, Time = DateTime.Now, To = customer.email });
                        ctx.SaveChanges();
                        sent++;
                    }
                    else { continue; }
                }

                //  await Send2(from, @"help@kudabank.com" , "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from  + "Total:" + " " + total + "|" + "Sent:" + "|" + sent,, "Admin", "Admin", "Template");
                //   await Send2(@"habeeb@kudabank.com", @"help@kudabank.com", "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from, "Total:" + " " + total + "|" + "Sent:" + "|" + sent, "Admin", "Admin", "Template");
                return;
            }            catch (Exception ex)
            {
                return;
            }
        }

        public async void BulkSendToWaitListNoKuda(string from, string alias, string subject, string message, string template)
        {
            try
            {
                // var Customers = externalctx.Clients.OrderBy(t => t.ID).ToList();
                //var Customerstest = externalctx.Clients.Where(t=> t.UserName== "omostcomputer@gmail.com").ToList();
                //var kuda = externalctx.KudaAccount.Where(n => n.Email != null).ToList();
                var kuda = accmgmt.GetWaitListNoKuda();
                // var Customers = externalctx.Clients.OrderBy(t => t.UserName == @"thisishabeeb@gmail.com").ToList();
                // int total = Customers.Count;
                int total = kuda.Count(x => x.Email != null);
                int sent = 0;
                string firstname_txt = "";
                //var kuda1 = kuda.Where(x => x.accountno == "1100000190").ToList();
                foreach (WaitListNoKuda customer in kuda)
                {

                    if (ctx.SentItems.Where(t => t.Subject == subject && t.To == customer.Email).FirstOrDefault() == null)
                    {
                        firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.FirstName.ToLower());
                        Send(customer.Email, from, alias, subject, message, firstname_txt, firstname_txt, template);
                        ctx.SentItems.Add(new SentItems { Alias = alias, From = from, Message = message, Subject = subject, Time = DateTime.Now, To = customer.Email });
                        ctx.SaveChanges();
                        sent++;
                    }
                    else { continue; }
                }

                //  await Send2(from, @"help@kudabank.com" , "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from  + "Total:" + " " + total + "|" + "Sent:" + "|" + sent,, "Admin", "Admin", "Template");
                //   await Send2(@"habeeb@kudabank.com", @"help@kudabank.com", "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from, "Total:" + " " + total + "|" + "Sent:" + "|" + sent, "Admin", "Admin", "Template");
                return;
            }            catch (Exception ex)
            {
                return;
            }
        }

        public async Task<IActionResult> BulkSendToKudaWithNoFunding(string from, string alias, string subject, string message, string template)
        {
            try
            {
                // var Customers = externalctx.Clients.OrderBy(t => t.ID).ToList();
                //var Customerstest = externalctx.Clients.Where(t=> t.UserName== "omostcomputer@gmail.com").ToList();
                //var kuda = externalctx.KudaAccount.Where(n => n.Email != null).ToList();
                var kuda = accmgmt.GetKudaWithNoFunding();
                // var Customers = externalctx.Clients.OrderBy(t => t.UserName == @"thisishabeeb@gmail.com").ToList();
                // int total = Customers.Count;
                int total = kuda.Count(x => x.firstName != null);
                int sent = 0;
                string firstname_txt = "";
                //var kuda1 = kuda.Where(x => x.accountno == "1100000190").ToList();
                foreach (Account customer in kuda)
                {

                    if (ctx.SentItems.Where(t => t.Subject == subject && t.To == customer.email).FirstOrDefault() == null)
                    {
                        firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.firstName.ToLower());
                         await Send(customer.email, from, alias, subject, message, firstname_txt, firstname_txt, template);
                        ctx.SentItems.Add(new SentItems { Alias = alias, From = from, Message = message, Subject = subject, Time = DateTime.Now, To = customer.email });
                        ctx.SaveChanges();
                        sent++;
                    }
                    else { continue; }
                }

                //  await Send2(from, @"help@kudabank.com" , "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from  + "Total:" + " " + total + "|" + "Sent:" + "|" + sent,, "Admin", "Admin", "Template");
                //   await Send2(@"habeeb@kudabank.com", @"help@kudabank.com", "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from, "Total:" + " " + total + "|" + "Sent:" + "|" + sent, "Admin", "Admin", "Template");
                 return new OkResult();
            }            catch (Exception ex)
            {
                return new OkResult();
            }
        }

        public async void BulkSendToKudaWithNoCard(string from, string alias, string subject, string message, string template)
        {
            try
            {
                // var Customers = externalctx.Clients.OrderBy(t => t.ID).ToList();
                //var Customerstest = externalctx.Clients.Where(t=> t.UserName== "omostcomputer@gmail.com").ToList();
                //var kuda = externalctx.KudaAccount.Where(n => n.Email != null).ToList();
                var kuda = accmgmt.GetKudaWithNoCard();
                // var Customers = externalctx.Clients.OrderBy(t => t.UserName == @"thisishabeeb@gmail.com").ToList();
                // int total = Customers.Count;
                int total = kuda.Count(x => x.firstName != null);
                int sent = 0;
                string firstname_txt = "";
                //var kuda1 = kuda.Where(x => x.accountno == "1100000190").ToList();
                foreach (Account customer in kuda)
                {

                    if (ctx.SentItems.Where(t => t.Subject == subject && t.To == customer.email).FirstOrDefault() == null)
                    {
                        firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.firstName.ToLower());
                        Send(customer.email, from, alias, subject, message, firstname_txt, firstname_txt, template);
                        ctx.SentItems.Add(new SentItems { Alias = alias, From = from, Message = message, Subject = subject, Time = DateTime.Now, To = customer.email });
                        ctx.SaveChanges();
                        sent++;
                    }
                    else { continue; }
                }

                //  await Send2(from, @"help@kudabank.com" , "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from  + "Total:" + " " + total + "|" + "Sent:" + "|" + sent,, "Admin", "Admin", "Template");
                //   await Send2(@"habeeb@kudabank.com", @"help@kudabank.com", "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from, "Total:" + " " + total + "|" + "Sent:" + "|" + sent, "Admin", "Admin", "Template");
                return;
            }            catch (Exception ex)
            {
                return;
            }
        }

        public async void BulkSendToKudaNotUpgraded(string from, string alias, string subject, string message, string template)
        {
            try
            {
                // var Customers = externalctx.Clients.OrderBy(t => t.ID).ToList();
                //var Customerstest = externalctx.Clients.Where(t=> t.UserName== "omostcomputer@gmail.com").ToList();
                //var kuda = externalctx.KudaAccount.Where(n => n.Email != null).ToList();
                var kuda = accmgmt.GetKudaNotUpgraded();
                // var Customers = externalctx.Clients.OrderBy(t => t.UserName == @"thisishabeeb@gmail.com").ToList();
                // int total = Customers.Count;
                int total = kuda.Count(x => x.firstName != null);
                int sent = 0;
                string firstname_txt = "";
                //var kuda1 = kuda.Where(x => x.accountno == "1100000190").ToList();
                foreach (Account customer in kuda)
                {

                    if (ctx.SentItems.Where(t => t.Subject == subject && t.To == customer.email).FirstOrDefault() == null)
                    {
                        firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.firstName.ToLower());
                        Send(customer.email, from, alias, subject, message, firstname_txt, firstname_txt, template);
                        ctx.SentItems.Add(new SentItems { Alias = alias, From = from, Message = message, Subject = subject, Time = DateTime.Now, To = customer.email });
                        ctx.SaveChanges();
                        sent++;
                    }
                    else { continue; }
                }

                //  await Send2(from, @"help@kudabank.com" , "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from  + "Total:" + " " + total + "|" + "Sent:" + "|" + sent,, "Admin", "Admin", "Template");
                //   await Send2(@"habeeb@kudabank.com", @"help@kudabank.com", "Mail Report", "Report on Mail:" + " " + subject + " " + "by" + " " + from, "Total:" + " " + total + "|" + "Sent:" + "|" + sent, "Admin", "Admin", "Template");
                return;
            }            catch (Exception ex)
            {
                return;
            }
        }
    }
}
