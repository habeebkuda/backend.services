﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using Monitor.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Services
{
    public class AccessRequired : Attribute, IAuthorizationFilter
    {
        private IdentityContext ctx = new IdentityContext();
        public void OnAuthorization(AuthorizationFilterContext context)
        {
             if(context.HttpContext.User.Identity.IsAuthenticated) { 
            var controllerActionDescriptor = context.ActionDescriptor as ControllerActionDescriptor;
            string controllerName = controllerActionDescriptor?.ControllerName;
            string actionName = controllerActionDescriptor?.ActionName;
            var roles = ctx.AppRoles.Where(x => x.Name != null).ToList();
            if(roles.Count() == 0)
                {
                    context.Result = new RedirectToActionResult("Login", "Auth", null);
                    return;
                }
            string rolename;
            string roleid = "";
            foreach (AppRole role in roles)
            {
                    var exist = context.HttpContext.User.IsInRole(role.Name);
                if (context.HttpContext.User.IsInRole(role.Name))
                {
                    rolename = role.Name;
                    roleid = role.Id;
                        var privileges = ctx.Privileges.Where(x => x.RoleID == roleid).ToList();
                        if (privileges.Any(x => x.Action == actionName || x.Controller == controllerName))
                        {
                            return;
                        }
                    }
            }

 
                        context.Result = new RedirectToActionResult("Login", "Auth", null);
                        return;
                
            
        }
             else
            {
                context.Result = new RedirectToActionResult("Login", "Auth", null);
             
                return;
            }

        }

    }
}
