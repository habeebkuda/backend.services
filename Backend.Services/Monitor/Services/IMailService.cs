﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Services
{
    public interface IMailService
    {
        bool SendMail(string to, string from, string subject, string body);
        Task Send(string to, string from, string alias, string subject, string message, string receiverfirstname, string receiverlastname, string template);
        void BulkSend(string from, string alias, string subject, string message, string template);
        void BulkSendToKudaAccounts(string from, string alias, string subject, string message, string template);
        void BulkSendToWaitListNoKuda(string from, string alias, string subject, string message, string template);
        void BulkSendToKudaWithNoCard(string from, string alias, string subject, string message, string template);
        void BulkSendToKudaNotUpgraded(string from, string alias, string subject, string message, string template);
        string readhtmlfile(string templatepath);
        Task<IActionResult> BulkSendToKudaWithNoFunding(string from, string alias, string subject, string message, string template);

    }
}
