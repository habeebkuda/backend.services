﻿using Monitor.ViewModels;
using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Monitor.Models;
namespace Monitoring.Services
{
    public class AccountManagement
    {
        string coreLiveToken = @"DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=";
        DataUtil data = new DataUtil();
        DataUtil2 data2 = new DataUtil2();
        DataUtil3 data3 = new DataUtil3();
        DataContext ctx = new DataContext();
        public async Task<List<Account>> getAccounts()
        {
            var result = new Account();
            var request = new TempRequest() { name = "" };
            string url = "https://kudabankcore.azurewebsites.net/api/Customers/GetUtilityCustomers";
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            List<Account> vlist = new List<Account>();
            try
            {

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.GetAsync(url))
                    {
                        if (message.IsSuccessStatusCode)
                        {
                            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                            //available = true;
                            string resultstring = await message.Content.ReadAsStringAsync();
                            //save result in list
                            vlist = JsonConvert.DeserializeObject<List<Account>>(resultstring);
                            //string url = dm["results"][0].urls.small;
                            //return vlist;


                        }
                    }
                }
               // Account vd = new Account() { accountno = "1100000190", ID= 1, lastName = "Yakubu", middleName = "Damilola", firstName = "Habeeb", email = "princehb56@gmail.com", bvn="2234587980" };
               // Account vd2 = new Account() { accountno = "1100000176", ID= 2, lastName = "Olowo", middleName = "Ayodeji", firstName = "Damilola", email = "dami@gmail.com", bvn = "22343095980" };
               // vlist.Add(vd);
              //  vlist.Add(vd2); 
                //  return vlist;
            }
            catch (Exception ex)
            {
            }
            return vlist;
            //return result;
        }

        public IEnumerable<Account> GetAccounts()
        {
            var retList = new List<Account>();
            var sql = @"select lastName,
	                           middleName,bvn,sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [Customer] INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id
                    ";

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    ID = long.Parse(row["ID"].ToString()),
                    bvn = row["bvn"].ToString(),
                    sex = row["sex"].ToString(),
                    accountno = row["accountno"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public IEnumerable<Account> GetPrimaryAgents()
        {
            var retList = new List<Account>();
            var sql = @"select distinct [SubAccounts].parentCustomerID, lastName,
	                           middleName,bvn,sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [Customer] INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id INNER JOIN [SubAccounts] ON [SubAccounts].parentCustomerID = [Customer].ID
                    ";

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    ID = long.Parse(row["ID"].ToString()),
                    bvn = row["bvn"].ToString(),
                    sex = row["sex"].ToString(),
                    accountno = row["accountno"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public IEnumerable<Account> GetSubAccounts(long parentid)
        {
            var retList = new List<Account>();
            var sql = String.Format(@"select [Customer].lastName,
	                           [Customer].middleName,[Customer].bvn,[Customer].sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno,[Customer_Account].account_name AS accountname, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [SubAccounts] INNER JOIN  [Customer] ON [SubAccounts].ID = [Customer].ID INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id  where [SubAccounts].parentCustomerID = {0};", parentid);
                   

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    ID = long.Parse(row["ID"].ToString()),
                    bvn = row["bvn"].ToString(),
                    sex = row["sex"].ToString(),
                    accountno = row["accountno"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString(),
                    accountname = row["accountname"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public IEnumerable<Account> GetSubAccounts()
        {
            var retList = new List<Account>();
            var sql = @"select [Customer].lastName,
	                           [Customer].middleName,[Customer].bvn,[Customer].sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno,[Customer_Account].account_name AS accountname, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [SubAccounts] INNER JOIN  [Customer] ON [SubAccounts].ID = [Customer].ID INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id
                    ";

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    ID = long.Parse(row["ID"].ToString()),
                    bvn = row["bvn"].ToString(),
                    sex = row["sex"].ToString(),
                    accountno = row["accountno"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString(),
                    accountname = row["account_name"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public Account GetAccountByID(long id)
        {
            var retList = new List<Account>();
            var sql = String.Format(@"select lastName,
	                           middleName,bvn,sex,
                      firstName, [Customer].ID,[Customer_Account].account_no AS accountno, [customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [Customer] INNER JOIN [Customer_Account]
    ON [Customer].ID = [Customer_Account].customer_id  INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id  where [Customer].ID = {0};", id); 
                    

            var result = data.GetData<Account>(sql);

      
                var row = result.Rows[0];

                var temp = new Account()
                {
                    lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    ID = long.Parse(row["ID"].ToString()),
                    bvn = row["bvn"].ToString(),
                    sex = row["sex"].ToString(),
                    accountno = row["accountno"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString()

                };
                //retList.Add(temp);
            
            return temp;
        }
        public async Task<Account> getAccountByID(long id)
        {
            var result = new Account();
            string url = "https://kudabankcore.azurewebsites.net/api/Customers/GetUtilityCustomerByID?=" + id;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            List<Account> vlist = new List<Account>();
            Account vd = new Account();
            try
            {

                using (HttpClient client = new HttpClient())
                {
                    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                    using (HttpResponseMessage message = await client.GetAsync(url))
                    {
                        if (message.IsSuccessStatusCode)
                        {

                            string resultstring = await message.Content.ReadAsStringAsync();
                            vd = JsonConvert.DeserializeObject<Account>(resultstring);



                        }
                    }
                }
                //vlist.Add(vd2);
                //  return vlist;
            }
            catch (Exception ex)
            {
            }
            return vd;
            //return result;
        }

        public IEnumerable<VerificationDetail> GetAllVerifiedIDs()
        {
            var retList = new List<VerificationDetail>();
            var sql = @"select *, [Customer].firstName AS firstName, [Customer].lastName AS lastName, [Customer].middleName AS middlename from [kudiTester].[VerifiedID] INNER JOIN [Customer] ON [Customer].kudiMoneyID = [kudiTester].[VerifiedID].kudiId
                    ";

            var result = data.GetData<VerificationDetail>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new VerificationDetail()
                {
                    username = row["username"].ToString(),
                    dateverified = Convert.ToDateTime(row["dateverified"]),
                    documentUrl = row["documentUrl"].ToString(),
                    ExpirationDate = Convert.ToDateTime(row["ExpirationDate"]),
                    id_number = row["id_number"].ToString(),
                    isApproved = (bool)row["isApproved"],
                    isRejected = (bool)row["isRejected"],
                    pictureUrl = row["pictureUrl"].ToString(),
                    resultCode = row["resultCode"].ToString(),
                    smileJobId = row["smileJobId"].ToString(),
                    dob = row["dob"].ToString(),
                    id_type = row["id_type"].ToString(),
                    firstname = row["firstName"].ToString(),
                    middlename = row["middleName"].ToString(),
                    lastname = row["lastName"].ToString()

                };
                retList.Add(temp);
            }
            return retList;

        }

        public IEnumerable<CardRequest> GetCardRequests()
        {
            var retList = new List<CardRequest>();
            var sql = @"select *, [Customer].firstName AS firstName, [Customer].lastName AS lastName, [Customer].middleName AS middlename,[customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [kudiTester].[CreateCardRequest] INNER JOIN [Customer] ON [Customer].ID = [kudiTester].[CreateCardRequest].customerID INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id where [kudiTester].[CreateCardRequest].status = '2' OR [kudiTester].[CreateCardRequest].status = '4'
                    ";

            var result = data.GetData<CardRequest>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new CardRequest()
                {
                    timeSubmitted = Convert.ToDateTime(row["timeSubmitted"]),
                    status = (int)row["status"],
                    //status_string = ((int)row["status"] == 3) ? "Successful" : "Failed",
                    pinChangedByCustomer = (bool)row["pinChangedByCustomer"],
                    AccountNumber = row["AccountNumber"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    lastName = row["lastName"].ToString(),
                    email = row["email"].ToString(),
                    phone  = row["phone"].ToString(),
                    pinField = (string.IsNullOrEmpty(row["pinField"].ToString()))? "NO": "YES",
                    streetAddressField = row["streetAddressField"].ToString(),
                    streetAddressLine2Field = row["streetAddressLine2Field"].ToString(),
                    cityField = row["cityField"].ToString(),
                    stateField = row["stateField"].ToString(),
                    countryCodeField = row["countryCodeField"].ToString()

                };
                if(temp.status == 2)
                {
                    temp.status_string = "Successful";
                }
                else if(temp.status == 4)
                {
                    temp.status_string = "Activated";
                }
                retList.Add(temp);
            }
            return retList;
        }


        public IEnumerable<WaitListNoKuda> GetWaitListNoKuda()
        {
            var retList = new List<WaitListNoKuda>();
            var sql = @"  select [Email Address], [First Name] from [dbo].[subscribed]
    EXCEPT( select [Email Address], [First Name]   from [dbo].[subscribed]  inner join  [dbo].[customerEmail] on [dbo].[subscribed].[Email Address] = [dbo].[customerEmail].email)
                    ";

            var result = data.GetData<WaitListNoKuda>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new WaitListNoKuda()
                {
                    FirstName = row["First Name"].ToString(),
                    Email = row["Email Address"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }


        public IEnumerable<CardRequest> GetCardRequestLogs()
        {
            var retList = new List<CardRequest>();
            var sql = @"select *, [Customer].firstName AS firstName, [Customer].lastName AS lastName, [Customer].middleName AS middlename,[customerEmail].email AS email,[customerPhones].phoneNumber AS phone from [kudiTester].[CreateCardRequest] INNER JOIN [Customer] ON [Customer].ID = [kudiTester].[CreateCardRequest].customerID INNER JOIN [customerEmail]
    ON [Customer].ID = [customerEmail].customer_id  INNER JOIN [customerPhones]
    ON [Customer].ID = [customerPhones].customer_id 
                    ";

            var result = data.GetData<CardRequest>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new CardRequest()
                {
                    timeSubmitted = Convert.ToDateTime(row["timeSubmitted"]),
                    status = (int)row["status"],
                    status_string = ((int)row["status"] == 2) ? "Successful" : "Failed",
                    pinChangedByCustomer = (bool)row["pinChangedByCustomer"],
                    AccountNumber = row["AccountNumber"].ToString(),
                    firstName = row["firstName"].ToString(),
                    middleName = row["middleName"].ToString(),
                    lastName = row["lastName"].ToString(),
                    email = row["email"].ToString(),
                    phone = row["phone"].ToString(),
                    pinField = (string.IsNullOrEmpty(row["pinField"].ToString())) ? "NO" : "YES",
                    streetAddressField = row["streetAddressField"].ToString(),
                    streetAddressLine2Field = row["streetAddressLine2Field"].ToString(),
                    cityField = row["cityField"].ToString(),
                    stateField = row["stateField"].ToString(),
                    countryCodeField = row["countryCodeField"].ToString()


                };
                retList.Add(temp);
            }
            return retList;
        }

        public IEnumerable<Account> GetKudaWithNoFunding()
        {
            var retList = new List<Account>();
            var sql = @"SELECT distinct account_name,[dbo].[Customer_Account].dateCreated, [dbo].[Customer].firstName, [dbo].[customerEmail].email from [dbo].[Customer_Account] inner join [dbo].[Customer] on [dbo].[Customer_Account].[customer_id] = [dbo].[Customer].ID inner join [dbo].[customerEmail] on [dbo].[customerEmail].customer_id = [dbo].[Customer].ID where datediff(day, [dbo].[Customer_Account].dateCreated, getdate()) >=3 except (
SELECT distinct account_name,[dbo].[Customer_Account].dateCreated, [dbo].[Customer].firstName, [dbo].[customerEmail].email from [dbo].[Customer_Account] inner join [dbo].[Customer] on [dbo].[Customer_Account].[customer_id] = [dbo].[Customer].ID inner join [kudiTester].[DTD] on [kudiTester].[DTD].account_ID = [dbo].[Customer_Account].ID inner join [dbo].[customerEmail] on [dbo].[customerEmail].customer_id = [dbo].[Customer].ID)
                    ";

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                   // lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                   // middleName = row["middleName"].ToString(),
                   // ID = long.Parse(row["ID"].ToString()),
                   // bvn = row["bvn"].ToString(),
                  //  sex = row["sex"].ToString(),
                  //  accountno = row["accountno"].ToString(),
                  email = row["email"].ToString()
                  //  phone = row["phone"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public IEnumerable<Account> GetKudaWithNoCard()
        {
            var retList = new List<Account>();
            var sql = @"select firstName, [Customer].ID, [customerEmail].email from [Customer]
     INNER JOIN [customerEmail] ON [Customer].ID = [customerEmail].customer_id WHERE  datediff(day, [dbo].[Customer].dateCreated, getdate()) >=7 AND [Customer].ID NOT IN
    (SELECT [kudiTester].CreateCardRequest.customerID
     FROM [kudiTester].CreateCardRequest)
                    ";

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    // lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    // middleName = row["middleName"].ToString(),
                    // ID = long.Parse(row["ID"].ToString()),
                    // bvn = row["bvn"].ToString(),
                    //  sex = row["sex"].ToString(),
                    //  accountno = row["accountno"].ToString(),
                    email = row["email"].ToString()
                    //  phone = row["phone"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public IEnumerable<Account> GetKudaNotUpgraded()
        {
            var retList = new List<Account>();
            var sql = @"select firstName, [Customer].ID,[customerEmail].email, [Customer_Account].accountTier from [Customer] inner join [Customer_Account] on [Customer].ID = [Customer_Account].customer_id inner join [dbo].[customerEmail] on [dbo].[customerEmail].customer_id = [dbo].[Customer].ID WHERE  datediff(day, [dbo].[Customer].dateCreated, getdate()) >=14 AND [Customer_Account].accountTier = '0'
                    ";

            var result = data.GetData<Account>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new Account()
                {
                    // lastName = row["lastName"].ToString(),
                    firstName = row["firstName"].ToString(),
                    // middleName = row["middleName"].ToString(),
                    // ID = long.Parse(row["ID"].ToString()),
                    // bvn = row["bvn"].ToString(),
                    //  sex = row["sex"].ToString(),
                    //  accountno = row["accountno"].ToString(),
                    email = row["email"].ToString()
                    //  phone = row["phone"].ToString()

                };
                retList.Add(temp);
            }
            return retList;
        }

        public async Task<IEnumerable<PendingApproval>> GetPendingApprovals()
        {
            var retList = new List<PendingApproval>();
            var sql = String.Format(@"select * from PendingApproval where Status = {0};", 0);

            var result = data3.GetData<PendingApproval>(sql);

            for (int i = 0; i < result.Rows.Count; i++)
            {
                var row = result.Rows[i];

                var temp = new PendingApproval()
                {
                    // lastName = row["lastName"].ToString(),

                    AccountNo = row["AccountNo"].ToString(),
                    Amount = (decimal)row["amount"],
                    DateSubmitted = (DateTime)row["datesubmitted"],
                    ApproveRole = row["approverole"].ToString(),
                    DateTreated = (row["datetreated"] == DBNull.Value) ? default(DateTime) : (DateTime)row["datetreated"],
                    //DateTreated = (DateTime)row?["datetreated"],
                    InitiatedBy = row["initiatedby"].ToString(),
                    Name = row["name"].ToString(),
                    RecordID = row["recordid"].ToString(),
                    Status = (int)row["status"],
                    ID = (long)row["ID"],
                    Action = (row["Action"] == DBNull.Value)? 0: (int)row["Action"],
                    Remark = (row["Remark"] == DBNull.Value) ? "" : row["Remark"].ToString()


                };

                if (temp.Name == "DisputeAction") {

                    temp.Name = "Outward";
                    var MoreDetails = await GetFailedOutward(temp.RecordID);
                    if (MoreDetails != null)
                    {
                        temp.NIBSSRequery = MoreDetails.nibssRequeryResponseField;
                        temp.NIBSSResponse = MoreDetails.nibssResponseField;
                        temp.CoreResponseReversal = MoreDetails.coreReversalResponseField;
                        temp.SN = MoreDetails.SNField.ToString();
                        temp.Fee = MoreDetails.feeField.ToString();
                        temp.CoreResponse = MoreDetails.coreResponseField;
                        temp.CoreResponseReversal = MoreDetails.coreReversalResponseField;
                        temp.DestinationBank = MoreDetails.DestinationBank;
                        temp.Channel = MoreDetails.Channel;
                        temp.SystemMessage = MoreDetails.systemMessageField;
                        temp.PendingRetrial = MoreDetails.pendingRetrialField.ToString();
                        temp.TransactionRef = MoreDetails.CoreRefNumField;
                        temp.BeneficiaryName = MoreDetails.BeneficiaryNameField;
                        temp.OriginatorName = MoreDetails.OriginatorNameField;
                        temp.BeneficiaryBank = MoreDetails.BeneficiaryBankField;
                        temp.BeneficiaryNo = MoreDetails.BeneficiaryNumberField;
                        temp.OriginatorName = MoreDetails.OriginatorNameField;
                        temp.OriginatorBank = MoreDetails.OriginatorBankField;
                        temp.OriginatorNo = MoreDetails.OriginatorNumberField;
                    }
                    if (temp.Action == 0)
                    {
                        temp.ActionName = "General";


                    }
                    if (temp.Action == 1)
                    {
                        temp.ActionName = "ReverseTransaction";


                }
                    if (temp.Action == 2)
                    {
                        temp.ActionName = "MarkAsSuccessful";


                }
                    if (temp.Action == 3)
                    {
                        temp.ActionName = "MarkAsAlreadyReversed";


                }
                    if (temp.Action == 4)
                    {
                        temp.ActionName = "DoNothingAndCLose";


                }
                }
                if (temp.Name == "DisputeActionInward")
                {
                    temp.Name = "Inward";
                    var MoreDetails = await GetFailedInward(temp.RecordID);
                    if (MoreDetails != null)
                    {
                        temp.NIBSSRequery = MoreDetails.nibssRequeryResponseField;
                        temp.NIBSSResponse = MoreDetails.nibssResponseField;
                        temp.SN = MoreDetails.SNField.ToString();
                        temp.Fee = MoreDetails.feeField.ToString();
                        temp.CoreResponse = MoreDetails.coreResponseField;
                        temp.CoreResponseReversal = MoreDetails.coreReversalResponseField;
                        temp.DestinationBank = MoreDetails.DestinationBank;
                        temp.Channel = MoreDetails.Channel;
                        temp.SystemMessage = MoreDetails.systemMessageField;
                        temp.PendingRetrial = MoreDetails.pendingRetrialField.ToString();
                        temp.TransactionRef = MoreDetails.CoreRefNumField;
                        temp.BeneficiaryName = MoreDetails.BeneficiaryNameField;
                        temp.BeneficiaryBank = MoreDetails.BeneficiaryBankField;
                        temp.BeneficiaryNo = MoreDetails.BeneficiaryNumberField;
                        temp.OriginatorName = MoreDetails.OriginatorNameField;
                        temp.OriginatorBank = MoreDetails.OriginatorBankField;
                        temp.OriginatorNo = MoreDetails.OriginatorNumberField;
                    }
                    if (temp.Action == 0)
                    {
                        temp.ActionName = "GeneralAction";


                    }
                    if (temp.Action == 1)
                    {
                        temp.ActionName = "MarkAsSuccessful";


                    }
                    if (temp.Action == 2)
                    {
                        temp.ActionName = "RepostTransaction";


                    }
                    if (temp.Action == 3)
                    {
                        temp.ActionName = "DoNothingAndCLose";


                    }
                 
                }
                retList.Add(temp);
            }
            return retList;

            //NIPService.Service1Client client
        }

        public async Task<IEnumerable<DisputeTransactionVM>> GetInwardTransactions()
        {
            var retList = new List<DisputeTransactionVM>();

            //NIPService.Service1Client client = new NIPService.Service1Client();
            ////var result = await client.InwardHistoryAsync(new NIPService.HistoryRequest() { beneficiaryAccountNumber = "0035352885", fromDate = DateTime.Today.AddDays(-100), toDate = DateTime.Now, senderAccountNumber = "" });
            //if(result.Outwards.Count =)
            //foreach (var item in result.Outwards)
            //{
            //    var temp = new DisputeTransactionVM()
            //    {
            //        Amount = item.Amount,
            //        SessionID = item.SessionId,
            //        AccountNo = item.BeneficiaryNumber,
            //        Time = item.EntryDate,
            //        StatusField = 0
            //    };
                //retList.Add(temp);
            
            return retList;

            
        }


        public async Task<IEnumerable<DisputeTransactionVM>> GetFailedOutward()
        {
            var retList = new List<DisputeTransactionVM>();

            NIPService.Service1Client client = new NIPService.Service1Client();
            var result = await client.GetFailedOutwardAsync();

            if (result.Outwards != null) { 
                foreach (var item in result.Outwards)
                {
                    var temp = new DisputeTransactionVM()
                    {
                        Amount = item.Amount,
                        SessionID = item.SessionId,
                        AccountNo = item.BeneficiaryNumber,
                        Time = item.DateTime,
                        StatusField = item.Status,
                        BeneficiaryBankField = item.BeneficiaryBank,
                        OriginatorNameField = item.OriginatorName,
                        OriginatorNumberField = item.OriginatorNumber,
                        BeneficiaryNumberField = item.BeneficiaryNumber,
                        TranID = item.SN,
                        nibssRequeryResponseField = item.nibssRequeryResponse,
                        nibssResponseField = item.nibssResponse,
                        coreResponseField = item.coreResponse,
                        feeField = item.fee,
                        Channel = item.Channel.ToString(),
                        CoreRefNumField =  item.CoreRefNum,
                        coreReversalResponseField = item.coreReversalResponse,
                        systemMessageField = item.systemMessage,
                        pendingRetrialField  = item.pendingRetrial,
                        DestinationBank = item.destinationBank,
                        TransactionRef = item.CoreRefNum,
                        BeneficiaryNameField = item.BeneficiaryName
                        
                    };

                    if (!isExisting(temp.SessionID))
                    {
                        retList.Add(temp);
                    }
                }
        }
            return retList;


        }

        public bool isExisting(string sessionid)
        {
          
            if(ctx.PendingApproval.Any(x => x.RecordID == sessionid && x.Status != 2))
            {
                       return true;
            }
            return false;
        }

        public bool beenSubmittedBefore(string sessionid)
        {
            if (ctx.PendingApproval.Any(x => x.RecordID == sessionid))
            { 

                return true;
            }
            return false;
        }

        public async Task<IEnumerable<DisputeTransactionVM>> GetFailedInward()
        {
            var retList = new List<DisputeTransactionVM>();

            NIPService.Service1Client client = new NIPService.Service1Client();
            var result = await client.GetFailedInwardAsync();
            if (result.Inwards != null)
            {
                foreach (var item in result.Inwards)
                {
                    var temp = new DisputeTransactionVM()
                    {
                        Amount = item.Amount,
                        SessionID = item.SessionId,
                        AccountNo = item.BeneficiaryNumber,
                        Time = item.EntryDate,
                        BeneficiaryNameField = item.BeneficiaryName,
                        OriginatorNameField = item.OriginatorName,
                        OriginatorNumberField = item.OriginatorNumber,
                        BeneficiaryNumberField = item.BeneficiaryNumber,
                        TranID = item.SN,
                        OriginatorBankField = item.OriginatorBank,
                        SNField = item.SN,
                        TransactionRef = item.CoreRefNum,
                        Channel = item.Channel,
                        coreResponseField = item.CoreResponse


                    };


                  if (!isExisting(temp.SessionID))
                    {
                        retList.Add(temp);
                    }
                }
            }
            return retList;


        }

        public async Task<DisputeTransactionVM> GetFailedInward(string sessionid)
        {
            var retList = new List<DisputeTransactionVM>();

            NIPService.Service1Client client = new NIPService.Service1Client();
            var item = await client.GetSingleFailedInwardAsync(sessionid);
            if (item != null && item.Inward !=null)
            {
                var temp = new DisputeTransactionVM()
                {
                    Amount = item.Inward.Amount,
                    SessionID = item.Inward.SessionId,
                    AccountNo = item.Inward.BeneficiaryNumber,
                    Time = item.Inward.EntryDate,
                    BeneficiaryNameField = item.Inward.BeneficiaryName,
                    OriginatorNameField = item.Inward.OriginatorName,
                    OriginatorNumberField = item.Inward.OriginatorNumber,
                    BeneficiaryNumberField = item.Inward.BeneficiaryNumber,
                    TranID = item.Inward.SN,
                    OriginatorBankField = item.Inward.OriginatorBank,
                    SNField = item.Inward.SN,
                    TransactionRef = item.Inward.CoreRefNum,
                    Channel = item.Inward.Channel,
                    coreResponseField = item.Inward.CoreResponse
                };
                return temp;
            }
            return null;

                    //if (!isExisting(temp.SessionID))
                    //{
                    //    retList.Add(temp);
                    
     

        }

        public async Task<DisputeTransactionVM> GetFailedOutward(string sessionid)
        {
            var retList = new List<DisputeTransactionVM>();

            NIPService.Service1Client client = new NIPService.Service1Client();
            var item = await client.GetSingleFailedOutwardAsync(sessionid);

            if (item != null && item.Outward!=null)
            {
                var temp = new DisputeTransactionVM()
                {
                    Amount = item.Outward.Amount,
                    SessionID = item.Outward.SessionId,
                    AccountNo = item.Outward.BeneficiaryNumber,
                    Time = item.Outward.DateTime,
                    StatusField = item.Outward.Status,
                    BeneficiaryBankField = item.Outward.BeneficiaryBank,
                    OriginatorNameField = item.Outward.OriginatorName,
                    OriginatorNumberField = item.Outward.OriginatorNumber,
                    BeneficiaryNumberField = item.Outward.BeneficiaryNumber,
                    TranID = item.Outward.SN,
                    nibssRequeryResponseField = item.Outward.nibssRequeryResponse,
                    nibssResponseField = item.Outward.nibssResponse,
                    coreResponseField = item.Outward.coreResponse,
                    feeField = item.Outward.fee,
                    Channel = item.Outward.Channel.ToString(),
                    CoreRefNumField = item.Outward.CoreRefNum,
                    coreReversalResponseField = item.Outward.coreReversalResponse,
                    systemMessageField = item.Outward.systemMessage,
                    pendingRetrialField = item.Outward.pendingRetrial,
                    DestinationBank = item.Outward.destinationBank,
                    TransactionRef = item.Outward.CoreRefNum,
                    BeneficiaryNameField = item.Outward.BeneficiaryName,
                    SNField = item.Outward.SN
                };
                return temp;
            }
            return null;

            //if (!isExisting(temp.SessionID))
            //{
            //    retList.Add(temp);



        }


        public async Task<IEnumerable<DisputeTransactionVM>> GetAllOutward(string beneficiaryNo, DateTime from, string senderNo, DateTime to)
        {
            var retList = new List<DisputeTransactionVM>();

            NIPService.Service1Client client = new NIPService.Service1Client();
            var result = await client.OutWardHistoryAsync(new NIPService.HistoryRequest() { beneficiaryAccountNumber = beneficiaryNo, fromDate = from, senderAccountNumber = senderNo, toDate = to });
            if (result.Outwards != null)
            {
                foreach (var item in result.Outwards)
                {
                    var temp = new DisputeTransactionVM()
                    {
                        Amount = item.Amount,
                        SessionID = item.SessionId,
                        AccountNo = item.BeneficiaryNumber,
                        Time = item.DateTime,
                        BeneficiaryNameField = item.BeneficiaryName,
                        OriginatorNameField = item.OriginatorName,
                        OriginatorNumberField = item.OriginatorNumber,
                        BeneficiaryNumberField = item.BeneficiaryNumber,
                        TranID = item.SN,
                        BeneficiaryBankField = item.destinationBank,
                        nibssRequeryResponseField = item.nibssRequeryResponse,
                        nibssResponseField = item.nibssResponse,
                        StatusField = item.Status,
                        coreResponseField = item.coreResponse,
                        feeField = item.fee,
                        TransactionRef = item.CoreRefNum,
                        Channel = item.Channel.ToString(),
                        DestinationBank = item.destinationBank,
                        systemMessageField = item.systemMessage,
                        pendingRetrialField = item.pendingRetrial

                    };


                    retList.Add(temp);
                }
            }
            return retList;


        }

        public async Task<IEnumerable<DisputeTransactionVM>> GetAllInward(string beneficiaryNo, DateTime from, string senderNo, DateTime to)
        {
            var retList = new List<DisputeTransactionVM>();

            NIPService.Service1Client client = new NIPService.Service1Client();
            var result = await client.InwardHistoryAsync(new NIPService.HistoryRequest() { beneficiaryAccountNumber = beneficiaryNo, fromDate = from, senderAccountNumber = senderNo, toDate = to });
            if (result.Inwards != null)
            {
                foreach (var item in result.Inwards)
                {
                    var temp = new DisputeTransactionVM()
                    {
                        Amount = item.Amount,
                        SessionID = item.SessionId,
                        AccountNo = item.BeneficiaryNumber,
                        Time = item.EntryDate,
                        BeneficiaryNameField = item.BeneficiaryName,
                        OriginatorNameField = item.OriginatorName,
                        OriginatorNumberField = item.OriginatorNumber,
                        BeneficiaryNumberField = item.BeneficiaryNumber,
                        TranID = item.SN,
                        OriginatorBankField = item.OriginatorBank,
                        SNField = item.SN,
                        coreResponseField = item.CoreResponse,
                        TransactionRef = item.CoreRefNum,
                        Channel = item.Channel.ToString()
                        //DestinationBank = item,
                        //systemMessageField = item.,
                        //pendingRetrialField = item.pendingRetrial
                    };


                    retList.Add(temp);
                }
            }
            return retList;


        }



        public async Task<OperationsResponse<object>> Approve(string approver,long itemid, string actionname)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();
            try
            {
                var sql = String.Format(@"select Name,RecordID from PendingApproval where ID = {0};", itemid);
                var result = data3.GetData<PendingApproval>(sql);
                var row = result.Rows[0];
                if (actionname == "DisputeAction" || actionname == "Outward")
                {
                    var getRecord = String.Format(@"select * from DisputeActionWorkItem where SessionID = {0};", row["RecordID"].ToString());
                    var getRecordResult = data3.GetData<DisputeActionWorkItem>(getRecord);
                    var disputeAction = getRecordResult.Rows[0];
                    var treat = await TreatRequest(approver, disputeAction["Remark"]?.ToString(), disputeAction["SessionID"]?.ToString(), disputeAction["InitiatedBy"]?.ToString(), (int)disputeAction["DisputeAction"], (long)disputeAction["TranID"]);
                    //  return treat.IsSuccessFul;
                    response.message = treat.ResponseCode + " " + treat.ResponseMessage;
                    response.status = treat.IsSuccessFul;
                    if (response.status)
                    {
                        string updateRecord = "UPDATE PendingApproval SET Status = @status, DateTreated = @datetreated Where ID  = @id";
                        var updateparams = new Dictionary<string, object>()
              {
                  {"status", 1 },
                  {"datetreated", DateTime.Now },
                  {"id", itemid }
              };

                        data3.ExecuteCommand(updateRecord, updateparams);

                    }
                }

                else if (actionname == "DisputeActionInward" || actionname == "Inward")
                {
                    var getRecord = String.Format(@"select * from DisputeActionInwardWorkItem where SessionID = {0};", row["RecordID"].ToString());
                    var getRecordResult = data3.GetData<DisputeActionWorkItem>(getRecord);
                    var disputeAction = getRecordResult.Rows[0];
                    var treat = await TreatRequest(approver, disputeAction["Remark"]?.ToString(), disputeAction["SessionID"]?.ToString(), disputeAction["InitiatedBy"]?.ToString(), (int)disputeAction["DisputeAction"], (long)disputeAction["TranID"]);
                    //  return treat.IsSuccessFul;
                    response.message = treat.ResponseCode + " " + treat.ResponseMessage;
                    response.status = treat.IsSuccessFul;
                    if (response.status)
                    {
                        string updateRecord = "UPDATE PendingApproval SET Status = @status, DateTreated = @datetreated Where ID  = @id";
                        var updateparams = new Dictionary<string, object>()
              {
                  {"status", 1 },
                  {"datetreated", DateTime.Now },
                  {"id", itemid }
              };

                        data3.ExecuteCommand(updateRecord, updateparams);

                    }
                }

                return response;
            }
            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return response;
            }

        }

        public async Task<OperationsResponse<object>> Reject(string approver, long itemid, string actionname)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();
            try
            {
                        string updateRecord = "UPDATE PendingApproval SET Status = @status, DateTreated = @datetreated Where ID  = @id";
                        var updateparams = new Dictionary<string, object>()
                        { 
                  {"status", 2 },
                  {"datetreated", DateTime.Now },
                  {"Id", itemid }
              };

                data3.ExecuteCommand(updateRecord, updateparams);

                response.status = true;
                response.message = "Successfully Rejected This Request";
                return response;
            }
            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return response;
            }

        }

        public async Task<NIPService.NIPManGenericResponse> TreatRequest(string approver, string remark, string sessionid, string initiatedby, int action, long tranid)
        {
            NIPService.NIPManGenericResponse response = new NIPService.NIPManGenericResponse();
            try
            {
                var retList = new List<DisputeTransactionVM>();
              //  NIPService.NIPManGenericResponse response = new NIPService.NIPManGenericResponse();
                NIPService.Service1Client client = new NIPService.Service1Client();
                NIPService.TreatOutwardRequest treatparam = new NIPService.TreatOutwardRequest();
                treatparam.dateRequested = DateTime.Now;
                treatparam.approver = approver;
                treatparam.extraInfoForAction = remark;
                treatparam.sessionID = sessionid;
                treatparam.ID = tranid;
                treatparam.performer = initiatedby;
                if (action == 1)
                {
                    treatparam.actionToPerform = NIPService.ReconciliationAction.ReverseTransaction;
                }
                if (action == 2)
                {
                    treatparam.actionToPerform = NIPService.ReconciliationAction.MarkAsSuccessful;
                }
                if (action == 3)
                {
                    treatparam.actionToPerform = NIPService.ReconciliationAction.MarkAsAlreadyReversed;
                }
                if (action == 4)
                {
                    treatparam.actionToPerform = NIPService.ReconciliationAction.DoNothingAndCLose;
                }

                string tparam = JsonConvert.SerializeObject(treatparam);
                
                var result = await client.ProcessFailedOutwardAsync(treatparam);
                return result;
            }
            catch(Exception ex)
            {
                response.IsSuccessFul = false;
                response.ResponseMessage = "Error:" + " " + ex.ToString();
                return response;
            }

        }

        public async Task<NIPService.NIPManGenericResponse> TreatInwardRequest(string approver, string remark, string sessionid, string initiatedby, int action, long tranid)
        {
            NIPService.NIPManGenericResponse response = new NIPService.NIPManGenericResponse();
            try
            {
                var retList = new List<DisputeTransactionVM>();
                //  NIPService.NIPManGenericResponse response = new NIPService.NIPManGenericResponse();
                NIPService.Service1Client client = new NIPService.Service1Client();
                NIPService.TreatInwardRequest treatparam = new NIPService.TreatInwardRequest();
                treatparam.dateRequested = DateTime.Now;
                treatparam.approver = approver;
                treatparam.extraInfoForAction = remark;
                treatparam.sessionID = sessionid;
                treatparam.ID = tranid;
                treatparam.performer = initiatedby;
                if (action == 1)
                {
                    treatparam.actionToPerform = NIPService.InwardActions.MarkAsSuccessful;
                }
                if (action == 2)
                {
                    treatparam.actionToPerform = NIPService.InwardActions.RepostTransaction;
                }
                if (action == 3)
                {
                    treatparam.actionToPerform = NIPService.InwardActions.DoNothingAndCLose;
                }

                string tparam = JsonConvert.SerializeObject(treatparam);

                var result = await client.TreatInwardAsync(treatparam);
                return result;
            }
            catch (Exception ex)
            {
                response.IsSuccessFul = false;
                response.ResponseMessage = "Error:" + " " + ex.ToString();
                return response;
            }

        }

        public bool UpdateApprovelItem(ApprovalWorkItemRequest req)
        {
          
                string updateRecord = "UPDATE PendingApproval SET Status = @status, Action = @action, Remark = @remark, InitiatedBy = @initiatedby, DateSubmitted = @datesubmitted  Where RecordID  = @id";
                var updateparams = new Dictionary<string, object>()
              {
                  {"status", 0 },
                  {"datesubmitted", DateTime.Now },
                  {"action", req.Action },
                  {"remark", req.Remark },
                  {"id", req.Record },
                  {"initiatedby", req.InitiatedBy }

              };

                data3.ExecuteCommand(updateRecord, updateparams);

            return true;
        }

        public bool CreatePendingApprovalItem(ApprovalWorkItemRequest req)
        {
            try
            {
                string sql = "INSERT INTO PendingApproval (Name,RecordID,InitiatedBy,Amount,Status,DateSubmitted,ApproveRole,AccountNo,Action,Remark) VALUES (@Name,@RecordID,@InitiatedBy,@Amount,@Status,@DateSubmitted,@ApproveRole,@AccountNo,@Action,@Remark)";
                var approverole = "";
                var insertparams = new Dictionary<string, object>()
              {
                  {"Name", req.Name },
                  {"RecordID", req.Record },
                  {"DateSubmitted", DateTime.Now },
                  {"InitiatedBy", req.InitiatedBy },
                  {"Amount", req.Amount },
                  {"Status", 0 },
                  {"ApproveRole", approverole },
                  {"AccountNo", req.AccountNo },
                  {"Action", req.Action },
                  {"Remark", req.Remark }
              };

                data3.ExecuteCommand(sql, insertparams);
                return true;
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {

                return false;
            }
        }

        public bool CreatePNDLogItem(PNDLog req)
        {
            try
            {
                string sql = "INSERT INTO PNDLog (AccountNo,AccountName,Performer,Action,Time) VALUES (@AccountNo,@AccountName,@Performer,@Action,@Time)";
                //var approverole = "";
                var insertparams = new Dictionary<string, object>()
              {
                  {"AccountNo", req.AccountNo },
                  {"Performer", req.Performer },
                  {"Time", DateTime.Now },
                  {"Action", req.Action},
                  {"AccountName", req.AccountName}
              };

                data3.ExecuteCommand(sql, insertparams);
                return true;
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {

                return false;
            }
        }


        public bool CreateDisputeActionWorkItem(DisputeActionWorkItem req)
        {
            try
            {
                string sql = "INSERT INTO DisputeActionWorkItem (DisputeAction,Remark,SessionID,InitiatedBy,Approver,Time,TranID) VALUES (@DisputeAction,@Remark,@SessionID,@InitiatedBy,@Approver,@Time,@TranID)";
                var approverole = "";
                var insertparams = new Dictionary<string, object>()
              {
                  {"DisputeAction", req.DisputeAction },
                  {"Remark", req.Remark },
                  {"SessionID", req.SessionID },
                  {"InitiatedBy", req.InitiatedBy },
                  {"Approver", req.Approver },
                  {"Time", req.Time},
                  {"TranID", req.TranID}
              };

                data3.ExecuteCommand(sql, insertparams);
                return true;
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {

                return false;
            }
        }

        public bool CreateDisputeActionInwardWorkItem(DisputeActionWorkItem req)
        {
            try
            {
                string sql = "INSERT INTO DisputeActionInwardWorkItem (DisputeAction,Remark,SessionID,InitiatedBy,Approver,Time,TranID) VALUES (@DisputeAction,@Remark,@SessionID,@InitiatedBy,@Approver,@Time,@TranID)";
                var approverole = "";
                var insertparams = new Dictionary<string, object>()
              {
                  {"DisputeAction", req.DisputeAction },
                  {"Remark", req.Remark },
                  {"SessionID", req.SessionID },
                  {"InitiatedBy", req.InitiatedBy },
                  {"Approver", req.Approver },
                  {"Time", req.Time},
                  {"TranID", req.TranID}
              };

                data3.ExecuteCommand(sql, insertparams);
                return true;
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {

                return false;
            }
        }
    }
}
