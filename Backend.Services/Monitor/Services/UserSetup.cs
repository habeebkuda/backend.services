﻿using Microsoft.AspNetCore.Identity;
using Monitor.Models;
using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Services
{
    public class UserSetup
    {
        private UserManager<AppUser> _usermgr;
        private RoleManager<AppRole> _userrole;
        private IdentityContext ctx = new IdentityContext();
        DataUtil data = new DataUtil();
        public UserSetup(UserManager<AppUser> usermgr, RoleManager<AppRole> userrole)
        {
            _userrole = userrole;
            _usermgr = usermgr;
        }
        // private UserManager<AppUser> _usermgr;
        public async Task CreateRole(string roleName)
        {

            IdentityResult roleResult;
            //foreach (var roleName in roleNames)
            //{
            //creating the roles and seeding them to the database
            var roleExist = await _userrole.RoleExistsAsync(roleName);
            if (!roleExist)
            {
                roleResult = await _userrole.CreateAsync(new AppRole(roleName));
            }
            //}
        }

        public async Task setupUsers()
        {
           var c =  ctx.CreateUsers.Where(u => u.ID != 0).ToList();
            foreach(CreateUser user in c)
            {
                await CreateUser1(user.UserName, user.EmailAdress, user.Password);
            }
        }

        public async Task setupRoles()
        {
            var c = ctx.CreateRoles.Where(u => u.RoleID != 0).ToList();
            foreach (CreateRole user in c)
            {
                await CreateRole(user.RoleName);
            }
        }

        public async Task setupUserRoles()
        {
            var c = ctx.CreateUserRoles.Where(u => u.ID != 0).ToList();

            foreach (CreateUserRole user in c)
            {
                await CreateUserRole(user.RoleName, user.UserEmail);
            }
        }

        public async Task setupPrivileges()
        {
            var c = ctx.CreatePrivileges.Where(u => u.ID != 0).ToList();

            foreach (CreatePrivilege user in c)
            {
              var role =   await _userrole.FindByNameAsync(user.RoleName);
                await CreatePrivilege(user.ModuleName, user.ModuleGroupName, role.Id);
            }
        }
        public async Task CreateUser(string rolename, string username, string emailaddress, string password)
        {


            IdentityResult roleResult;

            var roleExist = await _userrole.RoleExistsAsync(rolename);
            if (!roleExist)
            {
                roleResult = await _userrole.CreateAsync(new AppRole(rolename));
            }
            string UserPassword = password;
            var _user = await _usermgr.FindByEmailAsync(emailaddress);
            if (_user == null)
            {
                var poweruser = new AppUser
                {
                    UserName = username,
                    Email = emailaddress
                };
                var createPowerUser = await _usermgr.CreateAsync(poweruser, UserPassword);
                if (createPowerUser.Succeeded)
                {

                    await _usermgr.AddToRoleAsync(poweruser, rolename);

                }
            }
        }

        public async Task CreateUserRole(string rolename, string emailaddress)
        {


            //IdentityResult roleResult;

            var _user = await _usermgr.FindByEmailAsync(emailaddress);
            if (_user != null)
            {

                var roleExist = await _userrole.RoleExistsAsync(rolename);
                if (roleExist)
                {
                    //roleResult = await _userrole.CreateAsync(new AppRole(rolename));

                        await _usermgr.AddToRoleAsync(_user, rolename);
                }
            }
        }

        public async Task CreateUser1(string username, string emailaddress, string password)
        {


           // IdentityResult roleResult;

            string UserPassword = password;
            var _user = await _usermgr.FindByEmailAsync(emailaddress);
            if (_user == null)
            {
                var poweruser = new AppUser
                {
                    UserName = username,
                    Email = emailaddress
                };
                var createPowerUser = await _usermgr.CreateAsync(poweruser, UserPassword);
                if (createPowerUser.Succeeded)
                {

                    return;

                }
            }
        }

        public async Task CreatePrivilege(string action, string controller, string roleid)
        {

            var privilege = new Privilege()
            {
                Action = action,
                Controller = controller,
                RoleID = roleid
            };
            if (!ctx.Privileges.Any(p => p.Controller == controller && p.Action == action && p.RoleID == roleid))
            {
                ctx.Privileges.Add(privilege);
                ctx.SaveChanges();
            }
        }

        public async Task<Boolean> CheckApprovePrivilege(string user, string entity)
        {
      
            bool isPrivileged = false;
            var sql = String.Format(@"select [PendingApproval].ID from [PendingApproval] where [PendingApproval].User = {0} AND [PendingApproval].Entity = {1} ;", user, entity);


            var result = data.GetData<PendingApproval>(sql);

            if (result.Rows.Count > 0)
            {
                isPrivileged = true;

            }


            // var getBalanceSQL = GetAccountBalance(temp.accountno);
            //retList.Add(temp);

            return isPrivileged;
        }

        //public async Task<Boolean> CreateWorkItem(string name, string initiatedby, string record)
        //{

        //    bool isPrivileged = false;
        //    var sql = String.Format(@"select [PendingApproval].ID from [PendingApproval] where [PendingApproval].User = {0} AND [PendingApproval].Entity = {1} ;", user, entity);


        //    var result = data.GetData<PendingApproval>(sql);

        //    if (result.Rows.Count > 0)
        //    {
        //        isPrivileged = true;

        //    }


        //    // var getBalanceSQL = GetAccountBalance(temp.accountno);
        //    //retList.Add(temp);

        //    return isPrivileged;
        //}
    }
}
