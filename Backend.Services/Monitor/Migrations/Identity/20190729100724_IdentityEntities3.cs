﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Migrations.Identity
{
    public partial class IdentityEntities3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "CreatePrivileges",
                columns: table => new
                {
                    ModuleName = table.Column<string>(nullable: true),
                    ModuleGroupName = table.Column<string>(nullable: true),
                    RoleName = table.Column<string>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreatePrivileges", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CreateRoles",
                columns: table => new
                {
                    RoleName = table.Column<string>(nullable: true),
                    RoleID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreateRoles", x => x.RoleID);
                });

            migrationBuilder.CreateTable(
                name: "CreateUserRoles",
                columns: table => new
                {
                    UserEmail = table.Column<string>(nullable: true),
                    RoleName = table.Column<string>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreateUserRoles", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "CreateUsers",
                columns: table => new
                {
                    UserName = table.Column<string>(nullable: true),
                    EmailAdress = table.Column<string>(nullable: true),
                    Password = table.Column<string>(nullable: true),
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CreateUsers", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CreatePrivileges");

            migrationBuilder.DropTable(
                name: "CreateRoles");

            migrationBuilder.DropTable(
                name: "CreateUserRoles");

            migrationBuilder.DropTable(
                name: "CreateUsers");
        }
    }
}
