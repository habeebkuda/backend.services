﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Monitoring.Migrations
{
    public partial class initialMigrationLive : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AppzoneCalls",
                columns: table => new
                {
                    ID = table.Column<long>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    EndPoint = table.Column<string>(nullable: true),
                    LastUpdated = table.Column<DateTime>(nullable: false),
                    URL = table.Column<string>(nullable: true),
                    ResponseCode = table.Column<string>(nullable: true),
                    Request = table.Column<string>(nullable: true),
                    Vendor = table.Column<string>(nullable: true),
                    ResponseMessage = table.Column<string>(nullable: true),
                    Available = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AppzoneCalls", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppzoneCalls");
        }
    }
}
