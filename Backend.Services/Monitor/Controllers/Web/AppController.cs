﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Monitor.Models;
using Monitor.Services;
using Monitor.ViewModels;
using Monitor.Controllers.Api;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Identity;
using Monitoring.Services;

namespace Monitor.Controllers.Web
{
    public class AppController : Controller
    {
        public IMailService _mailservice;
        public IAppzoneCallRepository _repository;
        private IBulkMailRepository _bulkmailrepository;
        private ExternalDataContext externalctx = new ExternalDataContext();
        private IdentityContext identityctx = new IdentityContext();
        private UserManager<AppUser> _usermgr;
        private RoleManager<AppRole> _approle;
        

        public AppController(IMailService service, IAppzoneCallRepository repository, IBulkMailRepository repo, UserManager<AppUser> usermgr, RoleManager<AppRole> approle)
        {
            _mailservice = service;
            _repository = repository;
            _bulkmailrepository = repo;
            _usermgr = usermgr;
            _approle = approle;
        }

        public async Task<IActionResult> Index()

        {
            UserSetup userSetup = new UserSetup(_usermgr, _approle);
            //await userSetup.CreateRole("Captain");
            //await userSetup.CreateUser("Captain", "damilola", "damiguy@gmail.com", "P@ssw0rd@");
            // await userSetup.CreatePrivilege("Contact", "", "Captain");
           await userSetup.setupUsers();
           await userSetup.setupRoles();
            await userSetup.setupPrivileges();
            await userSetup.setupUserRoles();
            //externalctx.
            // var trips = _repository.GetAllTrips();
             
            // if (await _usermgr.FindByEmailAsync("habeeb@kudabank.com") == null)
            //  {

            //var newuser = new AppUser()
            //{
            //    UserName = "habeeb",
            //    Email = "habeeb@kudabank.com"
            //};
            //await _usermgr.CreateAsync(newuser, "P@ssw0rd!");

            //}

            return View();
        }

        //[Authorize]
        //public IActionResult Trips()

        //{
        //    var trips = _repository.GetAllTrips();
        //    return View(trips);
        //}
       [Authorize]
       public IActionResult ServiceList()

        { 
            return View();
        }

        [AccessRequired]
        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        [AccessRequired]
        public IActionResult ManageMerchant()
        {
           // return View();
            return Redirect("~/index.html");
        }

        [Authorize]
        public IActionResult SentMails()
        {
            
            try
            {
                var sentemails = _bulkmailrepository.GetAllMails();
                return View(sentemails);
            }
           
            catch (Exception ex) { return View(); }
        }
        [Authorize]
        public async Task<IActionResult> SendEmail(SendEmailViewModel vm)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    string firstname_txt;
                    //_mailservice.SendMail("", "", $"Contact Page from {model.Name} ({model.Email})", model.Message);
                    if(vm.To == "Customers")
                    {
                     
                        _mailservice.BulkSendToKudaAccounts(vm.From, vm.Alias, vm.Subject, vm.Message, vm.Template);

                    }
                    else if (vm.To == "KudimoneyNoKuda")
                    {
                          _mailservice.BulkSend(vm.From, vm.Alias, vm.Subject, vm.Message,vm.Template);
                

                    }
                    else if (vm.To == "WaitListNoKuda")
                    {
                        _mailservice.BulkSendToWaitListNoKuda(vm.From, vm.Alias, vm.Subject, vm.Message,vm.Template);
                    

                    }

                    else if (vm.To == "KudaWithNoFunding")
                    {
                        await _mailservice.BulkSendToKudaWithNoFunding(vm.From, vm.Alias, vm.Subject, vm.Message, vm.Template);


                    }

                    else if (vm.To == "KudaWithNoCard")
                    {
                         _mailservice.BulkSendToKudaWithNoCard(vm.From, vm.Alias, vm.Subject, vm.Message, vm.Template);


                    }

                    else if (vm.To == "KudaNotUpgraded")
                    {
                        _mailservice.BulkSendToKudaNotUpgraded(vm.From, vm.Alias, vm.Subject, vm.Message, vm.Template);


                    }
                    if (vm.To.ToLower().Contains(','))
                    {
                        List<string> addresses = vm.To.Split(',').ToList<string>();
                        foreach(string address in addresses)
                        {
                            var customer = externalctx.Clients.Where(x => x.UserName == address).FirstOrDefault();
                            if (customer != null)
                            { 
                                firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.FirstName.ToLower());
                                _mailservice.Send(address, vm.From, vm.Alias, vm.Subject, vm.Message, firstname_txt, customer.LastName, vm.Template);
                            }

                            else
                            {
                                _mailservice.Send(address, vm.From, vm.Alias, vm.Subject, vm.Message, "Customer", "Customer", vm.Template);
                            }
                        }
                        
                    }
                    else
                    {
                        var customer = externalctx.Clients.Where(x => x.UserName == vm.To).FirstOrDefault();
                        firstname_txt = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(customer.FirstName.ToLower());
                        if (customer != null) { _mailservice.Send(vm.To, vm.From, vm.Alias, vm.Subject, vm.Message, firstname_txt, customer.LastName, vm.Template); }
                        else
                        {
                            _mailservice.Send(vm.To, vm.From, vm.Alias, vm.Subject, vm.Message, "Customer", "Customer", vm.Template);
                        }
                    }
                    vm.Status = 1;
                    _bulkmailrepository.AddMail(new BulkMails { From = vm.From, Subject = vm.Subject, To = vm.To, Message = vm.Message, Alias= vm.Alias, Status=vm.Status});
                    _bulkmailrepository.SaveAll();
                    return RedirectToAction("SentMails");
                    //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
                }
                //foreach( Customers customer in customers)
                //{
                //    _mailservice.Send(to, from, subject, message, customer.FirstName, customer.LastName, "template");
                //}

                catch (Exception ex) { vm.Status = 0;
                    _bulkmailrepository.AddMail(new BulkMails { From = vm.From, Subject = vm.Subject, To = vm.To, Message = vm.Message + " " + ex.ToString(), Alias = vm.Alias, Status = vm.Status });
                    _bulkmailrepository.SaveAll();
                    return RedirectToAction("SentMails");
                }
                //_bulkmailrepository.AddMail(new BulkMails { From = vm.From, Subject = vm.Subject, To = vm.To });
                //_bulkmailrepository.SaveAll();
            }
            
            return View();
            //else return;
        }


        //public async void SendEmailToCustomer()
        //{
        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {

        //                _mailservice.BulkSend("help@kudimoney.com", "The Kudimoney Team", "Kudimoney is now Kuda", "Message");

        //            return;
        //            //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
        //        }

        //        catch (Exception ex)
        //        {
        //            _bulkmailrepository.AddMail(new BulkMails { From = "help@kudimoney.com", Subject = "Kudimoney is now Kuda", To = "Customers", Message = "" + " " + ex.ToString(), Alias = "The Kudimoney Team", Status = 0 });
        //            _bulkmailrepository.SaveAll();
        //            return;
        //        }
        //    }


        //    //else return;
        //}
        [AccessRequired]
        public async Task<IActionResult> Appzone()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
             await monitorLogic.DoRequest();
            var appzonecalls = _repository.GetAllResponse();
            return View(appzonecalls);
        }

        [AccessRequired]
        public async Task<IActionResult> Kudabank()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            await monitorLogic.DoKudabankRequest();
            var kudabankcalls = _repository.GetAllResponse();
            return View(kudabankcalls);
        }

        [AccessRequired]
        public async Task<IActionResult> ZenithBank()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            monitorLogic.DoZenithBankRequest();

            var appzonecalls = _repository.GetAllResponse();
            return View(appzonecalls);
        }

        [AccessRequired]
        public async Task<IActionResult> AccessBank()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            monitorLogic.DoAccessBankRequest();
            var appzonecalls = _repository.GetAllResponse();
            return View(appzonecalls);
        }

        [AccessRequired]
        public async Task<IActionResult> GTBank()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            monitorLogic.DoGTBRequest();
            var appzonecalls = _repository.GetAllResponse();
            return View(appzonecalls);
        }

        [AccessRequired]
        public async Task<IActionResult> Etranzact()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            monitorLogic.DoEtranzactRequest();
            var appzonecalls = _repository.GetAllResponse();
            return View(appzonecalls);
        }

        [AccessRequired]
        public async Task<IActionResult> InfoBip()
        {
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            monitorLogic.DoInfoBipRequest();
            var appzonecalls = _repository.GetAllResponse();
            return View(appzonecalls);
        }

        [AccessRequired]
        public async Task<IActionResult> PendingUpgrade()
        {
            //retrieve ID verifications
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            var details = await monitorLogic.getPendingVerifications();
            return View(details);
        }

        [Authorize]
        public async Task<IActionResult> PendingApproval()
        {
            //retrieve ID verifications
            AccountManagement accmgmt = new AccountManagement();
            var details =  await accmgmt.GetPendingApprovals();
            return View(details);
        }

        [Authorize]
        public async Task<IActionResult> VerificationOverview()
        {
            //retrieve ID verifications
            //retrieve ID verifications
            AccountManagement amgt = new AccountManagement();
            var details = amgt.GetAllVerifiedIDs();
            return View(details);
        }
        

       public async Task<IActionResult> DisputeTransactionInward()
        {
            //retrieve ID verifications
            AccountManagement accmgmt = new AccountManagement();
            var details = await accmgmt.GetFailedInward();
            return View(details);
        }


        // [AccessRequired]
        public async Task<IActionResult> DisputeTransaction()
        {
            //retrieve ID verifications
            AccountManagement accmgmt = new AccountManagement();
            var details = await accmgmt.GetFailedOutward();
            return View(details);
        }

        [AccessRequired]
        public async Task<IActionResult> AccountList()
        {
            //retrieve ID verifications
            AccountManagement amgt = new AccountManagement();
            var details =  amgt.GetAccounts();
            return View(details);
        }

        [Authorize]
        public async Task<IActionResult> AgentList()
        {
            //retrieve ID verifications
            AccountManagement amgt = new AccountManagement();
            var details = amgt.GetPrimaryAgents();
            return View(details);
        }

        [Authorize]
        public async Task<IActionResult> CardManagement()
        {
            //retrieve ID verifications
            AccountManagement amgt = new AccountManagement();
            var details = amgt.GetCardRequests();
            return View(details);
        }
        [HttpPost]
        public async Task<IActionResult> DisputeTransaction(long id)
        {
            //retrieve ID verifications
            AppzoneMonitorLogic monitorLogic = new AppzoneMonitorLogic(_repository, null);
            var details = await monitorLogic.getDisputeTransaction(id);
            return View(details);
        }

        //[HttpPost]
        public async Task<IActionResult> OutwardTransfers(string BeneficiaryNo, string From, string SenderNo, string To)
        {
            //retrieve ID verifications
            AccountManagement amgt = new AccountManagement();
            var details = await amgt.GetAllOutward(BeneficiaryNo, Convert.ToDateTime(From), SenderNo, Convert.ToDateTime(To));
            return View(details);
        }
        //[Route("App/InwardTransfers")]
        //[HttpPost]
        //public async Task<IActionResult> InwardTransfers(SearchReq req)
        //{
        //    //retrieve ID verifications
        //    AccountManagement amgt = new AccountManagement();
        //    var details = await amgt.GetAllInward(req.BeneficiaryNo, req.From, req.SenderNo, req.To);
        //    return View(details);
        //}

        //public async Task<IActionResult> OutwardTransfers(DisputeTransactionVM p)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        AccountManagement amgt = new AccountManagement();
        //        var details = await amgt.GetAllOutward(p.BeneficiaryNo, p.From, p.SenderNo, p.To);
        //        return View(details);
        //    }
        //        return View();
        //}

        //public async Task<IActionResult> InwardTransfers(IEnumerable<DisputeTransactionVM> details)
        //{

        //    //AccountManagement amgt = new AccountManagement();
        //    //var details = await amgt.GetInwardTransactions();
        //    if (details != null)
        //    {

        //        return View(details);
        //    }

        //    else
        //    {
        //        return View();
        //    }
        //}

        //[HttpPost]
        public async Task<IActionResult> InwardTransfers(string BeneficiaryNo, string From, string SenderNo, string To)
        {
            //retrieve ID verifications
            AccountManagement amgt = new AccountManagement();
            var details = await amgt.GetAllInward(BeneficiaryNo, Convert.ToDateTime(From), SenderNo, Convert.ToDateTime(To));
            return View(details);
        }
        // [AccessRequired]
        public async Task<IActionResult> EditDisputeTransaction(long id)
        {

            //var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            //return View(std);
            return View();
        }

        [AccessRequired]
        public async Task<IActionResult> EditAccount(long id)
        {

            //var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            //return View(std);

            AccountManagement amgt = new AccountManagement();
            var details = amgt.GetAccountByID(id);
            return View(details);
        }

        [Authorize]
        public async Task<IActionResult> SubAccount(long id)
        {

            //var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            //return View(std);

            AccountManagement amgt = new AccountManagement();
            var details = amgt.GetSubAccounts(id);
            return View(details);
        }

        // [AccessRequired]
        [HttpPost]
        public async Task<IActionResult> EditDisputeTransaction(DisputeTransactionVM transaction)
        {

            //var std = studentList.Where(s => s.StudentId == Id).FirstOrDefault();

            //return View(std);
            return View();
        }
        [HttpPost]
        public IActionResult Contact(ContactViewModel model)
        {
            _mailservice.SendMail("", "", $"Contact Page from {model.Name} ({model.Email})", model.Message);
            return View();
        }
    }
}
