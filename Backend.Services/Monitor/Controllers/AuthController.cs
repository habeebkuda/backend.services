﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Monitor.Models;
using Monitor.ViewModels;

namespace Monitor.Controllers
{
    public class AuthController : Controller
    {
        private SignInManager<AppUser> _signinmanager;

        public AuthController(SignInManager<AppUser> signinmanager)
        {
            _signinmanager = signinmanager;

        }
        public IActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("ServiceList", "App");
            }
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> Login(LoginViewModel vm, string returnurl)
        {
            if (ModelState.IsValid)
            {
                bool testuser = false;
                var signinresult = await _signinmanager.PasswordSignInAsync(vm.Username, vm.Password, true, false);
                //if (vm.Username == "Habeeb" && vm.Password == "admin") { testuser = true; }
                //if (testuser)
                //{
                //    SuperUser s = new SuperUser
                //    {
                //        UserName = "Habeeb",
                //        Email = "habeeb@gmail.com"

                //    };
                //   await _signinmanager.SignInAsync(s, true);
                //     return RedirectToAction("Trips", "Apps");
                    
                //}

                //else { 
                if (signinresult.Succeeded)
                {
                    if (string.IsNullOrWhiteSpace(returnurl))
                    {
                        return RedirectToAction("ServiceList", "App");
                    }
                    else
                    {
                        return Redirect(returnurl);
                    }

                }
                else
                {
                    ModelState.AddModelError("", "Login details incorrect");
                }
           // }
            }
            return View();
        }

        public async Task<IActionResult> Logout()
        {
            if (User.Identity.IsAuthenticated)
            {
                await _signinmanager.SignOutAsync();
            }
            return RedirectToAction("Index", "App");
        }
    }
}