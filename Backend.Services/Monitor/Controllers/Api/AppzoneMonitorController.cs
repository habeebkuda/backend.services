﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Monitor.Models;
using Monitor.ViewModels;
using Monitor.Services;
using System.Web.Http;
using Newtonsoft.Json;

namespace Monitor.Controllers.Api
{ [Route("api/appzonemonitorsvc")]

    public class AppzoneMonitorController : Controller
    {
        private ILogger<AppzoneMonitorController> _logger;
        private IAppzoneCallRepository _repository;
        private IAppzoneMonitorLogic _appzoneMonitorLogic;
        public AppzoneMonitorController(IAppzoneCallRepository repository, IAppzoneMonitorLogic appzoneMonitorLogic, ILogger<AppzoneMonitorController> logger)
        {
            _logger = logger;
            _repository = repository;
            _appzoneMonitorLogic = appzoneMonitorLogic;
        }
        [HttpGet("GetAllResponse")]
        [Route("GetAllResponse")]
        public JsonResult Get()
        {
            var results = Mapper.Map<IEnumerable<AppzoneCalls>>( _repository.GetAllResponse());
            return Json(results);
        }
        
        [Route("DoRequest")]
        [ProducesResponseTypeAttribute(typeof(CreateCardRequestResponse), 200)]
        [HttpPost]
        public async Task<IActionResult> DoRequest()
        {
            //AppzoneMonitorLogic lg = null;
            CreateCardRequestVM crvm = new CreateCardRequestVM()
            {
                AccountNumber = "1234567890"
                
            };

            await _appzoneMonitorLogic.activatecard_appzone(crvm);
            await _appzoneMonitorLogic.deactivatecard_appzone(crvm);
           await _appzoneMonitorLogic.getcustomercard_appzone(crvm);
            await _appzoneMonitorLogic.unblock_appzone(crvm);
            await _appzoneMonitorLogic.CreateCustomerAndAccount_appzone(crvm);
            await _appzoneMonitorLogic.GetCustomerAccountByTrackingRef_appzone(crvm);
            await _appzoneMonitorLogic.AppZoneNameEnquiry_appzone(crvm);
            await _appzoneMonitorLogic.GetAccountTransaction_appzone(crvm);
            await _appzoneMonitorLogic.PlaceLien_appzone(crvm);
            await _appzoneMonitorLogic.UnPlaceLien_appzone(crvm);
            await _appzoneMonitorLogic.FreezeAccount_appzone(crvm);
            await _appzoneMonitorLogic.UnFreezeAccount_appzone(crvm);
            await _appzoneMonitorLogic.CheckFreezeStatus_appzone(crvm);
            await _appzoneMonitorLogic.GLToCustomer_appzone(crvm);
            await _appzoneMonitorLogic.CustomerToGL_appzone(crvm);
            await _appzoneMonitorLogic.LocalFundsTransfer_appzone(crvm);
            await _appzoneMonitorLogic.TransactionStatus_appzone(crvm);
            var results = await _appzoneMonitorLogic.cardRequest_appzone(crvm);
            return new ObjectResult(results);
        }





            //else return;
        
        //[HttpPost("")]
        //public JsonResult Post([FromBody] TripViewModel vm)
        //{
        //    try {
        //        if (ModelState.IsValid) {
        //            var newTrip = Mapper.Map<Trip>(vm);
        //            //saving to DB

        //            _logger.LogInformation("Attempting to save new trip");

        //            _repository.AddTrip(newTrip);
        //            if (_repository.SaveAll()) { 
        //            Response.StatusCode = (int)HttpStatusCode.Created;
        //            return Json(Mapper.Map<TripViewModel>(newTrip));
        //        }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("Failed to save new trip", ex);
        //        Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //        return Json( new { Message = ex.Message});
        //    }
        //    Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //    return Json("Failed");
        //}
    }
}
