﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Monitor.Models;
using Monitor.ViewModels;
using Monitor.Services;
using System.Web.Http;
using Newtonsoft.Json;
using Monitoring.Services;
using System.Threading;
using System.Net.Http;

namespace Monitor.Controllers.Api
{ [Route("api/utilservice")]

    public class ServiceController : Controller
    {
        private IMailService _mailservice;
        private IAppzoneCallRepository _repository;
        private IBulkMailRepository _bulkmailrepository;
        private ExternalDataContext externalctx = new ExternalDataContext();
        string from = "hello@kudabank.com";
        string alias = "Kuda";
        string fundAccountTemplate = "MailTemplate/fundAccountTemplate.txt";
        string getCardTemplate = "MailTemplate/getCardTemplate.txt";
        string upgradeAccountTemplate = "MailTemplate/upgradeAccountTemplate.txt";
        AccountManagement accmgmt = new AccountManagement();
        DataUtil3 data = new DataUtil3();
        string coreLiveToken = @"DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=";


        public ServiceController(IMailService service, IAppzoneCallRepository repository, IBulkMailRepository repo)
        {
            _mailservice = service;
            _repository = repository;
            _bulkmailrepository = repo;
        }

        [Route("CreateWorkItem")]
        [HttpPost]
        public async Task<IActionResult> CreateWorkItem([FromBody]ApprovalWorkItemRequest req)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();
            try
            {
                if (accmgmt.beenSubmittedBefore(req.Record))
                {
                    accmgmt.UpdateApprovelItem(req);
                    response.status = true;
                    response.message = "Successfully Re-Created Approval Entries";
                    return Ok(response);
                }
                if (accmgmt.CreatePendingApprovalItem(req))
                {
                    if (req.Name == "DisputeAction")
                    {
                        var actionEntryCreated = accmgmt.CreateDisputeActionWorkItem(new DisputeActionWorkItem() { InitiatedBy = req.InitiatedBy, DisputeAction = req.Action, SessionID = req.Record, Time = DateTime.Now, TranID = req.TranID, Remark = req.Remark });
                        if (actionEntryCreated)
                        {
                            response.status = true;
                            response.message = "Successfully Created Approval Entries";
                        }
                    }
                   else if (req.Name == "DisputeActionInward")
                    {
                        var actionEntryCreated = accmgmt.CreateDisputeActionInwardWorkItem(new DisputeActionWorkItem() { InitiatedBy = req.InitiatedBy, DisputeAction = req.Action, SessionID = req.Record, Time = DateTime.Now, TranID = req.TranID, Remark = req.Remark });
                        if (actionEntryCreated)
                        {
                            response.status = true;
                            response.message = "Successfully Created Approval Entries";
                        }
                    }

                    return Ok(response);
                }
                else
                {
                    return Ok(response);
                }
            }

            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return Ok(response);
            }




        }

        [Route("ApproveWorkItem")]
        [HttpPost]
        public async Task<IActionResult> ApproveWorkItem([FromBody]ApproveRequest req)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();

            try
            {

                     response =await accmgmt.Approve(req.Approver,req.ID, req.Name);
             
                    
                

                return Ok(response);
            }

            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return Ok(response);
            }




        }

        [Route("RejectWorkItem")]
        [HttpPost]
        public async Task<IActionResult> RejectWorkItem([FromBody]ApproveRequest req)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();

            try
            {

                response = await accmgmt.Reject(req.Approver, req.ID, req.Name);




                return Ok(response);
            }

            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return Ok(response);
            }




        }

        [Route("LogPND")]
        [HttpPost]
        public async Task<PNDResponse> LogPND(PNDLog request)
        {
            var result = new PNDResponse();
            //PNDRequest req = new PNDRequest() { AccountNo = request.AccountNo, AuthenticationCode = "" };
            //string url = "https://kudabankcore.azurewebsites.net/api/Utility/ActivatePND";
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            try
            {
                bool isCreated= accmgmt.CreatePNDLogItem(request);
                if(isCreated)
                result.ResponseDescription = "Success";
                //using (HttpClient client = new HttpClient())
                //{
                //    client.DefaultRequestHeaders.Add("Token", coreLiveToken);
                //    // client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue(;
                //    using (HttpResponseMessage message = await client.PostAsJsonAsync(url, req))
                //    {
                //        if (message.IsSuccessStatusCode)
                //        {
                //            //  List<VerificationDetail> vlist = new List<VerificationDetail>();
                //            //available = true;
                //            string resultstring = await message.Content.ReadAsStringAsync();
                //            //save result in list
                //            AccountManagement accmgmt = new AccountManagement();
                //            accmgmt.CreatePNDLogItem(request);
                //            result = JsonConvert.DeserializeObject<PNDResponse>(resultstring);


                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                result.ResponseDescription = ex.ToString();
            }
            return result;
            //return result;
        }


        [Route("InwardTransfers")]
        [HttpPost]
        public async Task<IActionResult> InwardTransfers([FromBody]SearchReq req)
        {
            try
            {
                //retrieve ID verifications
                AccountManagement amgt = new AccountManagement();
                //var details = "";
                var details = await amgt.GetAllInward(req.BeneficiaryNo, Convert.ToDateTime(req.From), req.SenderNo, Convert.ToDateTime(req.To));
                RedirectToAction("InwardTransfers", "App", details);
               // RedirectToAction("InwardsTransfers")
                return View("App/InwardTransfers", details);
            }
            catch(Exception ex)
            {
                return View();
            }

        }


        [Route("SendEmailToCustomer")]
        [ProducesResponseTypeAttribute(typeof(CreateCardRequestResponse), 200)]
        [HttpPost]
        public async void SendEmailToCustomer()
        {
           
                try
                {

                _mailservice.BulkSend("help@kudimoney.com", "The Kudimoney Team", "Kudimoney is now Kuda", "Message", "Template");

                return;
                    //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
                }

                catch (Exception ex)
                {
                _bulkmailrepository.AddMail(new BulkMails { From = "help@kudabank.com", Subject = "Error:Kudimoney is now Kuda", To = "tech@kudabank.com", Message = "" + " " + ex.ToString(), Alias = "The Kuda Team", Status = 0 });
                _bulkmailrepository.SaveAll();
                    return;
                }
            


          
        }


        [Route("SendToKudaWithNoFunding")]
        [HttpPost]
        public async Task<IActionResult> SendToKudaWithNoFunding()
        {
            
            try
            {
                
                
              var resi =  await _mailservice.BulkSendToKudaWithNoFunding(from, alias, "How To Add Money To Your Kuda Account", null, _mailservice.readhtmlfile(fundAccountTemplate));
                Thread t2 = new Thread(() => _mailservice.BulkSendToKudaWithNoFunding(from, alias, "How To Add Money To Your Kuda Account", null, _mailservice.readhtmlfile(fundAccountTemplate)));
                t2.Start();
                Thread.Sleep(10);
                return new ObjectResult("Success");
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {
                _bulkmailrepository.AddMail(new BulkMails { From = "help@kudabank.com", Subject = "Error: How To Add Money To Your Kuda Accoun", To = "tech@kudabank.com", Message = "" + " " + ex.ToString(), Alias = "The Kuda Team", Status = 0 });
                return new ObjectResult("Error");
            }




        }

        [Route("SendToKudaWithNoCard")]
        [HttpPost]
        public async void SendToKudaWithNoCard()
        {

            try
            {


                Thread t2 = new Thread(() => _mailservice.BulkSendToKudaWithNoFunding(from, alias, "Your Kuda Card Is Freedom", null, _mailservice.readhtmlfile(getCardTemplate)));
                t2.Start();
                Thread.Sleep(10);

                return;
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {
                _bulkmailrepository.AddMail(new BulkMails { From = "help@kudabank.com", Subject = "Error: Your Kuda Card Is Freedom", To = "tech@kudabank.com", Message = "" + " " + ex.ToString(), Alias = "The Kuda Team", Status = 0 });
                _bulkmailrepository.SaveAll();
                return;
            }




        }


        [Route("SendToKudaNotUpgraded")]
        [HttpPost]
        public async void SendToKudaNotUpgraded()
        {

            try
            {


                Thread t2 = new Thread(() => _mailservice.BulkSendToKudaNotUpgraded(from, alias, "You Can Do A Lot More With Kuda", null, _mailservice.readhtmlfile(upgradeAccountTemplate)));
                t2.Start();
                Thread.Sleep(10); 

                return;
                //var customers = externalctx.Customers.OrderBy(t => t.ID).ToList();
            }

            catch (Exception ex)
            {
                _bulkmailrepository.AddMail(new BulkMails { From = "help@kudabank.com", Subject = "Error: You Can Do A Lot More With Kuda", To = "tech@kudabank.com", Message = "" + " " + ex.ToString(), Alias = "The Kuda Team", Status = 0 });
                _bulkmailrepository.SaveAll();
                return;
            }




        }

        [Route("GetFailedInwardDetails")]
        [HttpPost]
        public async Task<IActionResult> GetFailedInwardDetails([FromBody]DetailsRequest req)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();

            try
            {
              
               
               




                return Ok(response);
            }

            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return Ok(response);
            }




        }

        [Route("GetFailedInwardDetails")]
        [HttpPost]
        public async Task<IActionResult> GetFailedOutwardDetails([FromBody]DetailsRequest req)
        {
            OperationsResponse<object> response = new OperationsResponse<object>();

            try
            {
              





                return Ok(response);
            }

            catch (Exception ex)
            {

                response.status = false;
                response.message = "Error:" + " " + ex.ToString();
                return Ok(response);
            }




        }


        //[HttpPost("")]
        //public JsonResult Post([FromBody] TripViewModel vm)
        //{
        //    try {
        //        if (ModelState.IsValid) {
        //            var newTrip = Mapper.Map<Trip>(vm);
        //            //saving to DB

        //            _logger.LogInformation("Attempting to save new trip");

        //            _repository.AddTrip(newTrip);
        //            if (_repository.SaveAll()) { 
        //            Response.StatusCode = (int)HttpStatusCode.Created;
        //            return Json(Mapper.Map<TripViewModel>(newTrip));
        //        }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.LogError("Failed to save new trip", ex);
        //        Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //        return Json( new { Message = ex.Message});
        //    }
        //    Response.StatusCode = (int)HttpStatusCode.BadRequest;
        //    return Json("Failed");
        //}
    }
}
