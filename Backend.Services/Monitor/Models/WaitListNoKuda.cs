﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class WaitListNoKuda
    {
        public string Email { get; set; }
        public string FirstName { get; set; }
    }
}
