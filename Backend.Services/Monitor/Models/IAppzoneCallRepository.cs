﻿using System.Collections.Generic;

namespace Monitor.Models
{
    public interface IAppzoneCallRepository
    {

        IEnumerable<AppzoneCalls> GetAllResponse();
        //IEnumerable<Trip> GetAllTripsWithStops();
        //void AddTrip(Trip newTrip);
        bool SaveAll();
        AppzoneCalls GetResponseByEndPoint(string endppoint);
        void AddResponse(AppzoneCalls newCall);
        // void AddStop(string tripName, Stop newStop);
    }
}