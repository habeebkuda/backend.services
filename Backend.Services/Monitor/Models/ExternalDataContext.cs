﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class ExternalDataContext : DbContext
    // : IdentityDbContext<SuperUser>
    {
        //public DbSet<AppzoneCalls> AppzoneCalls { get; set; }
        //public DbSet<EtranzactCalls> EtranzactCalls { get; set; }
        public DbSet<Clients> Clients { get; set; }
        public DbSet<NoKudaAccount> NoKudaAccount { get; set; }
        public DbSet<KudaAccount> KudaAccount { get; set; }
        // public DbSet<Stop> stops { get; set; }


        public ExternalDataContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = Startup.Configuration["Data:externalContextConnection"];
            optionsBuilder.UseSqlServer(connString);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
