﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class PendingApproval
    {
        public long  ID { get; set; }
        public string Name { get; set; }
        public string InitiatedBy { get; set; }
        public int Status { get; set; }
        public string RecordID { get; set; }
        public decimal Amount { get; set; }
        public string ApproveRole { get; set; }
        public string AccountNo { get; set; }
        public string ActionName { get; set; }
        public string Remark { get; set; }
        public string Fee { get; set; }
        public string NIBSSRequery { get; set; }
        public string NIBSSResponse { get; set; }
        public string SN { get; set; }
        public string TransactionRef { get; set; }
        public string CoreResponse { get; set; }
        public string CoreResponseReversal { get; set; }
        public string DestinationBank { get; set; }
        public string Channel { get; set; }
        public string SystemMessage { get; set; }
        public string PendingRetrial { get; set; }
        public string OriginatorName { get; set; }
        public string BeneficiaryName { get; set; }
        public string BeneficiaryNo { get; set; }
        public string BeneficiaryBank { get; set; }
        public string OriginatorBank { get; set; }
        public string OriginatorNo { get; set; }
        public int Action { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime DateTreated { get; set; }
    }
}
