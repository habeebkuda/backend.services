﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class CardRequest
    {
        public string AccountNumber { get; set; }
        public string CardProfileBIN { get; set; } = "506196";
        public string Token { get; set; }

        public bool specifyaddress { get; set; } // netgruru
        public string streetAddressField { get; set; } //user
        public string status_string { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string middleName { get; set; }
        public string sex { get; set; }
        public long ID { get; set; }
        public DateTime timeSubmitted { get; set; }
        public bool pinChangedByCustomer { get; set; }
        public int status { get; set; }
        public string pinField { get; set; }

        public string streetAddressLine2Field { get; set; } //user

        public string cityField { get; set; } //user

        public string stateField { get; set; } //user

        public string postalCodeField { get; set; } // user

        public string countryCodeField { get; set; } // user
    }
}
