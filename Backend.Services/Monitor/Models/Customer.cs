﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Monitor.Models
{

    public class Customer
    {

        [Required]
        [StringLength(50)]
        public string firstName { get; set; }

        [Key]
        public long ID { get; set; }

    }
}