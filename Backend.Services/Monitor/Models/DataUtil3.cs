﻿//using SQL.Interface;
using Microsoft.CodeAnalysis.CSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace Monitor.Models
    {
        public class DataUtil3
        {
            private static readonly string Constr  = Startup.Configuration["Data:liveContextConnection"];

            public SqlDataReader GetData(string query, SqlParameter[] param)
            {
                SqlConnection con = new SqlConnection(Constr);
                SqlCommand cmd = new SqlCommand(query, con);
                if (param != null)
                {
                    cmd.Parameters.AddRange(param);
                }
                con.Open();
                SqlDataReader dr = cmd.ExecuteReader();
                return dr;
            }

            /// <inheritdocs />
            public DataTable GetData<T>(string sql)
            {

                using (var connection = new SqlConnection(Constr))
                {
                    try
                    {
                        var datatable = new DataTable();

                        SqlDataAdapter sda = new SqlDataAdapter(sql, connection);
                        connection.Open();
                        sda.Fill(datatable);
                        connection.Close();
                    return datatable;
                    }
                    catch
                    {
                        CloseConnectionCheck(connection);
                        throw;
                    }
                }
            }

        public void ExecuteCommand(string inssql, Dictionary<string, Object> parameters)
        {
            using (var connection = new SqlConnection(Constr))
            {
                try
                {
                    SqlCommand cmd = new SqlCommand(inssql, connection);
                    connection.Open();
                    foreach (KeyValuePair<String, Object> p in parameters)
                    {
                        cmd.Parameters.AddWithValue("@" + p.Key, p.Value);
                    }
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }
                catch
                {
                    CloseConnectionCheck(connection);
                    throw;
                }
            }
        }

        /// <inheritdocs />
        public Task<int> ExecuteCommandAsync(string sSQL, params SqlParameter[] parameters)
        {
            // can be called as
            //await ExecuteCommandAsync(sSQL, sqlParams);
            return Task.Run(() =>
            {
                using (var connection = new SqlConnection(Constr))
                using (var cmd = new SqlCommand(sSQL, connection))
                {
                    cmd.CommandType = CommandType.Text;
                    if (parameters != null) cmd.Parameters.AddRange(parameters);

                    connection.Open();
                    return cmd.ExecuteNonQuery();
                }
            });
        }
        private void CloseConnectionCheck(SqlConnection conn)
        {
            if (conn.State != ConnectionState.Closed)
            {
                conn.Close();
            }
        }
    }
}
