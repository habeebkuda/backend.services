﻿using System;

namespace Monitor.Models
{

    public class VerificationDetail
    {
        public long ID { get; set; }
        public string userID { get; set; }
        public string username { get; set; }
        public string id_type { get; set; }
        public string country { get; set; }
        public string id_number { get; set; }
        public string dob { get; set; }
        public DateTime dateverified { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool isUsed { get; set; }
        public string checkSum { get; set; }
        public bool isApproved { get; set; }
        public string documentUrl { get; set; }
        public string kudiId { get; set; }
        public bool isRejected { get; set; }
        public string resultCode { get; set; }
        public string pictureUrl { get; set; }
        public string smileJobId { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
        public string middlename { get; set; }
    }

}

