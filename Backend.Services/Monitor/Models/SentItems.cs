﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class SentItems
    {
        
        [EmailAddress]
        public string From { get; set; }

        
        public string To { get; set; }

       
        [StringLength(255, MinimumLength = 5)]
        public string Subject { get; set; }

        
        [StringLength(5000, MinimumLength = 5)]
        public string Message { get; set; }


        public string Alias { get; set; }

        public DateTime? Time { get; set; } = DateTime.Now;

        public long ID { get; set; }
    }
}
