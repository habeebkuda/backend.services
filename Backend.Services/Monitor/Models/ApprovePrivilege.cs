﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class ApprovePrivilege
    {
        public long ID { get; set; }
        public string User { get; set; }
        public string Entity { get; set; }
    }
}
