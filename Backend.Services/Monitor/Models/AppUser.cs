﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Monitor.Models
{
    public class AppUser: IdentityUser
    {
        public DateTime RegisteredTime { get; set; }
        public string Alias { get; set; }
    }
}