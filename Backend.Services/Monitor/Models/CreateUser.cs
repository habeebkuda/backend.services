﻿using System.ComponentModel.DataAnnotations;

namespace Monitor.Models
{
    public class CreateUser
    {
        public string UserName { get; set; }
        public string EmailAdress { get; set; }
        public string Password { get; set; }
        [Key]
        public int ID { get; set; }
    }
}