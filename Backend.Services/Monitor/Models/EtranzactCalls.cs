﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class EtranzactCalls
    {
        public long ID { get; set; }
        public string EndPoint { get; set; }
        public DateTime LastUpdated { get; set; }
        public string URL { get; set; }
        public string ResponseCode { get; set; }
        public string Request { get; set; }
        public string Vendor { get; set; }
        public string ResponseMessage { get; set; }
        public string Available { get; set; }
    }
}
