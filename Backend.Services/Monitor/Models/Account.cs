﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class Account
    {
        public long ID { get; set; }

        [Required]
        public string lastName { get; set; }
        [Required]
        public string firstName { get; set; }

        public string middleName { get; set; }
        [Required]
        public string sex { get; set; }
        [Required]
        public string address { get; set; }
        [Required]
        public string state { get; set; }
        [Required]
        public string country { get; set; }
        [Required]
        public string bvn { get; set; }
        [Required]
        public string city { get; set; }
        [Required]
        public string KudiMoneyID { set; get; }
        [Required]
        public Nullable<System.DateTime> date_of_birth { get; set; }
        [Required]
        public string createdBy { get; set; }

        public System.DateTime dateCreated { get; set; }
        public string trackingRef { get; set; }

        [Required]
        public bool isApproved { get; set; }
        public Nullable<System.DateTime> dateApproved { get; set; }
        public string approvedBy { get; set; }
        public bool? appzoneIdUpdated { get; set; }

        [Required]
        public string email { set; get; }

        [Required]
        public string phone { set; get; }

        public string accountno { set; get; }

        public string accountname { set; get; }
    }
}
