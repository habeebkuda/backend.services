﻿using System.Collections.Generic;

namespace Monitor.Models
{
    public interface IBulkMailRepository
    {

        IEnumerable<BulkMails> GetAllMails();
        //IEnumerable<Trip> GetAllTripsWithStops();
        //void AddTrip(Trip newTrip);
        bool SaveAll();
        void AddMail(BulkMails newMail);
        //BulkMails GetResponseByEndPoint(string endppoint);
        //void AddResponse(AppzoneCalls newCall);
        // void AddStop(string tripName, Stop newStop);
    }
}