﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class ExternalDataContext2 : DbContext
    // : IdentityDbContext<SuperUser>
    {
        //public DbSet<AppzoneCalls> AppzoneCalls { get; set; }
        //public DbSet<EtranzactCalls> EtranzactCalls { get; set; }
        public DbSet<Customer> Customer { get; set; }


        public ExternalDataContext2()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = Startup.Configuration["Data:externalContextConnectiontwo"];
            optionsBuilder.UseSqlServer(connString);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
