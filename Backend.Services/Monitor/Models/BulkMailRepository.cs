﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class BulkMailRepository : IBulkMailRepository
    {
        private DataContext _context;
        private ILogger<BulkMailRepository> _logger;

        public BulkMailRepository(DataContext context, ILogger<BulkMailRepository> logger)
        {
            _context = context;
            _logger = logger;
        }


        public IEnumerable<BulkMails> GetAllMails()
        {
            try
            {
                var mails = _context.BulkMails.OrderByDescending(t => t.Time).ToList();
                return mails;
            }
            catch( Exception ex)
            {
                _logger.LogError("Could not get mails from DB", ex);
                return null;
            }
        }

        public void AddMail(BulkMails newMail)
        {
            //throw new NotImplementedException();
            _context.BulkMails.Add(newMail);
        }

        //public AppzoneCalls GetResponseByEndPoint(string endpoint)
        //{
        //    return _context.AppzoneCalls.Where(t => t.EndPoint == endpoint).FirstOrDefault();

        //   // throw new NotImplementedException();
        //}


        public bool SaveAll()
        {
            // throw new NotImplementedException();
            return _context.SaveChanges() > 0;
        }

    }
}
