﻿using Microsoft.AspNetCore.Identity;
using System;

namespace Monitor.Models
{
    public class SuperUser : IdentityUser
    {
        public DateTime Access { get; set; }
    }
}