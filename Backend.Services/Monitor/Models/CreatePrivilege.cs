﻿using System.ComponentModel.DataAnnotations;

namespace Monitor.Models
{
    public class CreatePrivilege
    {
        public string ModuleName { get; set; }
        public string ModuleGroupName { get; set; }
        public string RoleName { get; set; }
        [Key]
        public int ID { get; set; }
    }
}