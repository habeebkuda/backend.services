﻿using System.ComponentModel.DataAnnotations;

namespace Monitor.Models
{
    public class CreateRole
    {
        public string RoleName { get; set; }
        [Key]
        public int RoleID { get; set; }
    }
}