﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class AppzoneCallRepository : IAppzoneCallRepository
    {
        private DataContext _context;
        private ILogger<AppzoneCallRepository> _logger;

        public AppzoneCallRepository(DataContext context, ILogger<AppzoneCallRepository> logger)
        {
            _context = context;
            _logger = logger;
        }


        public IEnumerable<AppzoneCalls> GetAllResponse()
        {
            try
            {
                return _context.AppzoneCalls.OrderBy(t => t.LastUpdated).ToList();
            }
            catch( Exception ex)
            {
                _logger.LogError("Could not get appzone calls from DB", ex);
                return null;
            }
        }

        public void AddResponse(AppzoneCalls newCall)
        {
            //throw new NotImplementedException();
            _context.AppzoneCalls.Add(newCall);
        }

        public AppzoneCalls GetResponseByEndPoint(string endpoint)
        {
            return _context.AppzoneCalls.Where(t => t.EndPoint == endpoint).FirstOrDefault();

           // throw new NotImplementedException();
        }


        public bool SaveAll()
        {
            // throw new NotImplementedException();
            return _context.SaveChanges() > 0;
        }

    }
}
