﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Monitor.Models
{

    public class NoKudaAccount
    {

        public string Email { get; set; }

        public string FirstName { get; set; }

        [Key]
        public int ID { get; set; }

    }
}