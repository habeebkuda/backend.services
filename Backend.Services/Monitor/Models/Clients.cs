﻿using System;

namespace Monitor.Models
{

    public class Clients
    {
        public Guid ID { get; set; }
        // public Guid BankAccountID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime DateJoined { get; set; }
        public decimal? Income { get; set; }
        public bool BankAdded { get; set; }
        public string BVN { get; set; }
        // public List<ModifyPendingGoalRequest> ModifyRequests { get; set; }
        //bills should be an ICollection
        public decimal? Bills { get; set; }
        public bool? CardVerified { get; set; }
        public decimal? LeisureExpenses { get; set; }
        //public List<PayStackCredentials> CardDetails { get; set; }

        public DateTime CreatedOn { get; set; }
        public decimal? SafeToSpend { get; set; }
        //public ICollection<Goal> Goals { get; set; }
        //public List<Bank> Banks { get; set; }
        //public ICollection<Transaction> Transactions { get; set; }
        //public ICollection<InstantSave> Savings { get; set; }

        public bool PhoneNumberVerified { get; set; }
        public bool BankSecretQuestionSet { get; set; }
        public bool SubmitBankDetails { get; set; }
        public bool BVNVerification { get; set; }


        //public virtual SpendAndSave SpendAndSave { get; set; }


        public DateTime? BankDetailsSubmitTime { get; set; }
        public DateTime? TimeOfPhoneVerification { get; set; }
        public DateTime? TimeSecretQuestionSet { get; set; }
        public DateTime? BVNVerificationTime { get; set; }

        public bool TempStatus { get; set; }
        public int TempRegNumber { get; set; }
    }
}