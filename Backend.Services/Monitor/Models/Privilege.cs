﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitoring.Models
{
    public class Privilege
    {
        public int PrivilegeID { get; set; }
        public string RoleID { get; set; }
        public string Action { get; set; }
        public string Controller { get; set; }
    }
}
