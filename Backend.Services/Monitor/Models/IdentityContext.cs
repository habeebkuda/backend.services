﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class IdentityContext : IdentityDbContext<AppUser>
    // : IdentityDbContext<SuperUser>
    {
        //public DbSet<AppzoneCalls> AppzoneCalls { get; set; }
        //public DbSet<EtranzactCalls> EtranzactCalls { get; set; }
        //public DbSet<BulkMails> BulkMails { get; set; }
        //public DbSet<SentItems> SentItems { get; set; }
       public DbSet<AppRole> AppRoles { get; set; }
        public DbSet<Privilege> Privileges { get; set; }
        public DbSet<CreateRole> CreateRoles { get; set; }
        public DbSet<CreateUser> CreateUsers { get; set; }
        public DbSet<CreatePrivilege> CreatePrivileges { get; set; }
        public DbSet<CreateUserRole> CreateUserRoles { get; set; }


        public IdentityContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = Startup.Configuration["Data:identiyContextConnection"];
            optionsBuilder.UseSqlServer(connString);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
