﻿using System.ComponentModel.DataAnnotations;

namespace Monitor.Models
{
    public class CreateUserRole
    {
        public string UserEmail { get; set; }
        public string RoleName { get; set; }
        [Key]
        public int ID { get; set; }
    }
}