﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Monitoring.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.Models
{
    public class DataContext: DbContext
    // : IdentityDbContext<SuperUser>
    {
        public DbSet<AppzoneCalls> AppzoneCalls { get; set; }
        public DbSet<EtranzactCalls> EtranzactCalls { get; set; }
        public DbSet<BulkMails> BulkMails { get; set; }
        public DbSet<SentItems> SentItems { get; set; }
        public DbSet<PendingApproval> PendingApproval { get; set; }
        // public DbSet<Stop> stops { get; set; }


        public DataContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var connString = Startup.Configuration["Data:liveContextConnection"];
            optionsBuilder.UseSqlServer(connString);
            base.OnConfiguring(optionsBuilder);
        }
    }
}
