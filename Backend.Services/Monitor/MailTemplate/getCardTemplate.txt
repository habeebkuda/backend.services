﻿<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Your Kuda Card Is Freedom</title>
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,600,700&amp;display=swap"
      rel="stylesheet" />
    <style type="text/css">
    @media (min-width: 550px) {
      body
      {
        font-family: 'Muli', sans-serif;
      }
        .hero-image {
          top: 0 !important;
        }
      
        table[class="body"] {
          padding-bottom: 50px !important;
          padding-top: 50px !important;
        }
      
        .email-logo-masthead {
          display: inline !important;
          height: 35px !important;
          margin-left: 0 !important;
          margin-right: 0 !important;
        }
      
        .email-content {
          border-left: 1px solid #dadfe1 !important;
          border-right: 1px solid #dadfe1 !important;
        }
      
        .email-content-block {
          padding-left: 50px !important;
          padding-right: 50px !important;
        }
      }
      .email-social-bar-copy p, .email-social-bar-copy a, .email-social-bar-copy .ios-no-link {
        color: white !important;
        text-decoration: none !important;
      }
      .email-social-bar-icons img{
        /* width: 20px !important; */
        height: 20px !important;
      }
  </style>
  </head>
  <body style="background-color: #f6f8f8; height: 100%; margin: 0; padding: 0;">

      <table class="body" style="background-color: #f6f8f8; height: 100%; padding-bottom: 25px; padding-left: 0; padding-right: 0; padding-top: 25px;"
        width="100%"
        cellspacing="0"
        cellpadding="0"
        border="0"
        align="center">
        <tbody>
          <tr>
            <td>
              <table cellspacing="0" cellpadding="0" border="0" align="center">
                <tbody>
                    <tr>
                        <td width="550">
                            <table width="100%" cellspacing="0" cellpadding="0" border="0"
                                align="center">
                                <tbody>
                                <tr>
                                        <td style="height: 45px;  text-align: center; margin-bottom: 20px; padding-bottom: 10px;">
                                            <img class="email-logo-masthead"
                                            src="https://kuda-subscribe.herokuapp.com/asset/logo-purple.png"
                                            style="width: auto; max-width: 100% !important; height: 30px; margin: auto; margin-bottom: 5px;"
                                            alt="Kuda"
                                            title="Kuda Logo"
                                            height="35" /></td>
                                        <td style="height: 45px;">
                                          <meta charset="utf-8" />
                                </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                  <tr>
                    <td width="550">
                      <table width="100%" cellspacing="0" cellpadding="0" border="0"
                        align="center">
                        <tbody>
                          <tr>
                            <td class="email-content-border" style="border-top: 1px solid #dadfe1;">
                            </td>
                          </tr>
                          <tr>
                            <td class="email-content" style="background-color: #FFFFFF;">
                              <table class="" width="100%" cellspacing="0" cellpadding="0"
                                border="0"
                                align="center">
                                <tbody>
                                  <tr>
                                    <td class="email-content-block copy" style="font-family: 'Muli', sans-serif !important; padding-left: 25px; padding-right: 25px; padding-top: 50px; padding-bottom: 50px;">
                                        <!-- Email content here -->
                                        <span style="text-align: left; display: block; margin-top: 20px;">Hi ##N1##,<br ><br >
What you do with your money
is up to you, but you should see
all the things you can do with it
when you have a Kuda Card.<br><br>
Your Kuda Card will set your
money free to pay for anything
everywhere Verve cards are
accepted.<br><br>
Payments are safe, you'll get
instant notifications and you
can track your spending on the
app.<br><br>
Best of all, you can block and
unblock your card from the app
whenever you need to.<br><br>
Tap 'Cards' on the app and
request your beautiful Kuda
Card now.<br><br>
We'll make sure it's in your
hands soon.<br><br><br>
                                        <span style="text-align: left; display: block; margin-top: 20px;">Love,<br ><br >The Kuda Team</span>
                                    </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <table class="email-social-bar" style="padding-left: 50px; padding-right: 50px; background: #40196d; padding-bottom: 25px; padding-top: 25px;"
                              width="100%"
                              cellspacing="0"
                              cellpadding="0"
                              border="0"
                              align="center">
                              <tbody>
                                <tr>
                                  
                                  </td>
                                </tr>
                                <tr>
                                  <td class="email-social-bar-copy copy" style="font-family: &quot;Muli&quot;,sans-serif !important; height: 50px;">
                                    <p class="ios-no-link" style="margin-bottom: 15px; font-family: 'Muli', sans-serif !important; font-weight: 400; font-size: 11px; line-height: 1.5; color: white;">151
                                      Herbert Macaulay Way <br style="font-family: &quot;Avenir Next&quot;, &quot;Avenir&quot;, &quot;Helvetica&quot;, sans-serif !important;" />
                                      Yaba, Lagos</p>
                                  </td>
                                </tr>
                                <tr>
                                  <td class="email-social-bar-icons" style="height: 50px;">
                                    <table class="" width="100%" cellspacing="0"
                                      cellpadding="0"
                                      border="0"
                                      align="center">
                                      <tbody>
                                        <tr>
                                          <td align="left"> <a class="email-social-bar-social-icon"
                                              href="https://twitter.com/kudabank"
                                              style="-moz-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -o-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -webkit-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); color: #4EAACC; padding: 0 5px; text-decoration: none;">
                                              <img class="auto-width" src="https://imgplaceholder.com/20x20/transparent/ffffff/fa-twitter"
                                                style="width: auto; max-width: 100% !important; border: 0;" />
                                            </a> <a class="email-social-bar-social-icon"
                                              href="https://www.facebook.com/kudabank"
                                              style="-moz-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -o-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -webkit-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); color: #4EAACC; padding: 0 5px; text-decoration: none;">
                                              <img class="auto-width" src="https://imgplaceholder.com/20x20/transparent/ffffff/fa-facebook"
                                                style="width: auto; max-width: 100% !important; border: 0;" />
                                            </a> <a class="email-social-bar-social-icon"
                                              href="https://www.instagram.com/kudabank/"
                                              style="-moz-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -o-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -webkit-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); color: #4EAACC; padding: 0 5px; text-decoration: none;">
                                              <img class="auto-width" src="https://imgplaceholder.com/20x20/transparent/ffffff/fa-instagram"
                                                style="width: auto; max-width: 100% !important; border: 0;" />
                                            </a> <a class="email-social-bar-social-icon"
                                              href="https://www.linkedin.com/company/kudimoney/about/"
                                              style="-moz-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -o-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -webkit-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); color: #4EAACC; padding: 0 5px; text-decoration: none;">
                                              <img class="auto-width" src="https://imgplaceholder.com/20x20/transparent/ffffff/fa-linkedin"
                                                style="width: auto; max-width: 100% !important; border: 0;" />
                                            </a> <a class="email-social-bar-social-icon"
                                              href="https://medium.com/@kudabank"
                                              style="-moz-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -o-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); -webkit-transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); transition: color 0.175s cubic-bezier(0.215, 0.61, 0.355, 1); color: #4EAACC; padding: 0 5px; text-decoration: none;">
                                              <img class="auto-width" src="https://imgplaceholder.com/20x20/transparent/ffffff/fa-medium"
                                                style="width: auto; max-width: 100% !important; border: 0;" />
                                            </a> </td>
                                        </tr>
                                      </tbody>
                                    </table>
                                  </td>
                                </tr>
                              </tbody>
                            </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </td>
                </tr>
                <tr>
                    <td width="550">
                        <table width="100%" cellspacing="0" cellpadding="0" border="0"
                            align="center">
                            <tbody>
                            <tr>
                                <td style="text-align: center; font-size: 0.8em; padding-top: 30px; color: #a4a5a5; padding: 20px;">
                                    © 2019 Kuda MF Bank (RC796975). All rights reserved. 
                                    <br /><br />
                                    All deposits are insured by the Nigerian Deposit Insurance Corporation (NDIC). Kuda MF Bank is licensed by the Central Bank of Nigeria. “Kuda” and “Kudabank” are trademarks of Kuda Technologies LTD. 151 Herbert Macaulay Way, Yaba, Lagos. Nigeria. 131 Finsbury Pavement, London, EC2A 1NT, UK
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
      </tbody>
    </table>

    </div>
  </body>