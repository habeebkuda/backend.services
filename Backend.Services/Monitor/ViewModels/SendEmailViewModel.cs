﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.ViewModels
{
    public class SendEmailViewModel
    {
        [Required]
        [EmailAddress]
        public string From { get; set; }

        [Required]
        public string To { get; set; }

        [Required]
        [StringLength(255, MinimumLength = 5)]
        public string Subject { get; set; } 

        
        public string Message { get; set; }

        public string Template { get; set; }


        public string Send { get; set; }

        public int Status { get; set; }

        public string Alias { get; set; }

        public DateTime? Time { get; set; } = DateTime.Now;

        [Required]
        public long ID { get; set; }
    }
}
