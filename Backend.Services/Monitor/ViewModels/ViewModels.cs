﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitor.ViewModels
{

    public class CreateCustomerAndAccountModelWithoutBVN
    {
        public string TransactionTrackingRef { get; set; } = @"f358c5a5-3c7f-46e2-aadc-a51396cb9c6c";
        public string CustomerID { get; set; }
        public string AccountOpeningTrackingRef { get; set; } = @"8882d1be-75e7-43dd-9e52-66fa81ded660";
        public string ProductCode { get; set; } = @"301";
        public string LastName { get; set; } = "Test";
        public string OtherNames { get; set; } = "Tester";
        public string BVN { get; set; } = "0000";
        public string FullName { get; set; } = "Test Tester";
        public string PhoneNo { get; set; } = "1234567890";
        public int Gender { get; set; } = 1;

        public string Address { get; set; } = "Test";
        public string AccountOfficerCode { get; set; } = "SN000002";
    }

    public class CreateCustomerAndAccountModel : CreateCustomerAndAccountModelWithoutBVN
    {

        //public string TransactionTrackingRef { get; set; }
        //public string CustomerID { get; set; }
        //public string AccountOpeningTrackingRef { get; set; }
        //public string ProductCode { get; set; }
        //public string LastName { get; set; }
        //public string OtherNames { get; set; }
        //public string BVN { get; set; }
        //public string FullName { get; set; }
        //public string PhoneNo { get; set; }
        //public int Gender { get; set; }
        public DateTime DateOfBirth { get; set; }
        //public string Address { get; set; }
        //public string AccountOfficerCode { get; set; }
    }
        public class CreateCustomerAndAccountResponse
        {
            public bool IsSuccessful { get; set; }
            public string CustomerIDInString { get; set; }
            public string Message { get; set; }
            public string TransactionTrackingRef { get; set; }
        }

        public class GetAccountResponse
        {

            public string AccessLevel { get; set; }
            public string AccountNumber { get; set; }
            public string AccountStatus { get; set; }
            public string AccountType { get; set; }
            public string AvailableBalance { get; set; }
            public string Branch { get; set; }
            public string CustomerID { get; set; }
            public string CustomerName { get; set; }
            public string DateCreated { get; set; }
            public string LastActivityDate { get; set; }
            public string NUBAN { get; set; }
            public bool PNDStatus { get; set; }
            public string AccountTier { get; set; }

        }
        public class TransactionResponse
        {
            public bool IsSuccessful { get; set; }
            public string CustomerIDInString { get; set; }
            public List<Transaction> Message { get; set; }

        }
        public class Transaction
        {

            public bool IsReversed { get; set; }
            public string UniqueIdentifier { get; set; }
            public string InstumentNo { get; set; }
            public DateTime? TransactionDate { get; set; }
            public string ReferenceID { get; set; }
            public string Narration { get; set; }
            public string Amount { get; set; }
            public string Debit { get; set; }
            public string Credit { get; set; }
            public DateTime CurrentDate { get; set; }

        }

    public class SearchReq
    {
        public string BeneficiaryNo { get; set; }
        public string SenderNo { get; set; }
        public string To { get; set; }
        public string From { get; set; }


    }
    public class NameEnquiryResponse
        {
            public string Name { get; set; }
            public string MaximumBalance { get; set; }
        }
        public class NameEnquiryRequest
        {
            public string AccountNo { get; set; }
            public string AuthenticationCode { get; set; }
        }
        public class FundTransferRequest
        {
            public string AuthenticationKey { get; set; }
            public string TransactionReference { get; set; }
            public string FromAccountNumber { get; set; }
            public string ToAccountNumber { get; set; }
            public string Amount { get; set; }
            public string Narration { get; set; }

        }
    public class GenericResponse
    {
        public bool IsSuccessFul { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponseCode { get; set; }

    }

    public class SearchParam
    {
        public string SenderNo { get; set; }
        public string BeneficiaryNo { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

    }
    public class FundsTransferResp : GenericResponse
        {
            public string TransactionReference { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescritpion { get; set; }
            public string Reference { get; set; }
            //public bool ResponseStatus { get; set; }

        }
        public class CustomerGLPostingRequest
        {
            public string RetrievalReference { get; set; }
            public string Token { get; set; }
            public string Narration { get; set; }
            public string AccountNumber { get; set; }
            public string Amount { get; set; }
            public string GLCode { get; set; }
        }

        public class CreateCardRequestVM
        {

            public string AccountNumber { get; set; }
            public string CardProfileBIN { get; set; } = "506184081";
            public string Token { get; set; }

            public bool specifyaddress { get; set; } // netgruru
            public string streetAddressField { get; set; } //user

            public string streetAddressLine2Field { get; set; } //user

            public string cityField { get; set; } //user

            public string stateField { get; set; } //user

            public string postalCodeField { get; set; } // user

            public string countryCodeField { get; set; } // user
        public string trackingRef { get; internal set; } = @"c26482b9-e0b0-425c-b88a-1e9711e5d9f4";
        public string mfbCode { get; internal set; } = "0371";
        public string TransactionTrackingRef { get; set; } = @"f358c5a5-3c7f-46e2-aadc-a51396cb9c6c";
        public string CustomerID { get; set; }
        public string AccountOpeningTrackingRef { get; set; } = @"8882d1be-75e7-43dd-9e52-66fa81ded660";
        public string ProductCode { get; set; } = @"301";
        public string LastName { get; set; } = "Test";
        public string OtherNames { get; set; } = "Tester";
        public string BVN { get; set; } = "0000";
        public string FullName { get; set; } = "Test Tester";
        public string PhoneNo { get; set; } = "1234567890";
        public int Gender { get; set; } = 1;
        public string Address { get; set; } = "Test";
        public string AccountOfficerCode { get; set; } = "SN000002";
        public string CardPAN { get; set; } = "SN000002";
        public string Narration { get; set; } = "NARRATION";
        public string AuthenticationKey { get; set; } = @"43DC449C-F91A-49C1-B7A5-9935FA8612AD";
        public string TransactionReference { get; set; } = "TRAN1";
        public string FromAccountNumber { get; set; } = "12345678";
        public string ToAccountNumber { get; set; } = "12345678";
        public string Amount { get; set; } = "0";
        public string AccountNo { get; set; } = "123456789";
        public string AuthenticationCode { get; set; } = "43DC449C-F91A-49C1-B7A5-9935FA8612AD";

    }

        public class CreateCardRequestResponse
        {
        public bool RequestStatus { get; set; } = true;
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class ActivateCardRequest
        {

            public string AccountNumber { get; set; }
            public string CardPAN { get; set; }
            public string Token { get; set; }
        }

        public class ActivateCardResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

    public class DeActivateCardRequest
        {

            public string AccountNumber { get; set; }
            public string CardPAN { get; set; }
            public string ReferenceID { get; set; }
            public string HotlistReason { get; set; }
            public string Token { get; set; }

        }

        public class DeActivateCardResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class GetCustomerCardRequest
        {

            public string AccountNumber { get; set; }
            public string ReferenceID { get; set; }
            public string Token { get; set; }

        }

        public class GetCustomerCardResponse
        {

            public List<CardDetails> CardDetails { get; set; }
            public string ReferenceID { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }


        }

        public class CardDetails
        {

            public string AccountNumber { get; set; }
            public string CardHolderName { get; set; }
            public string CardStatus { get; set; }
            public string LinkedDate { get; set; }
            public string PrimaryAccountNumber { get; set; }
            public string LastFour { get; set; }
            //  public string ExpiryDate { get; set; }
            public string CardType { get; set; }
            public string Cvv { get; set; }
            public string CardExp { get; set; }

        }

        public class BlockCardRequest
        {

            public string AccountNumber { get; set; }
            public string CardPAN { get; set; }
            public string ReferenceID { get; set; }
            public string HotlistReason { get; set; }
            public string Token { get; set; }

        }

        public class BlockCardResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class UnBlockCardRequest
        {

            public string AccountNumber { get; set; }
            public string CardPAN { get; set; }
            public string ReferenceID { get; set; }
            public string HotlistReason { get; set; }
            public string Token { get; set; }

        }

        public class UnBlockCardResponse
        {
            public bool IsSuccessful { get; set; }
            public string Status { get; set; }
            //public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public string TransactionReference { get; set; }


        }

        public class PlaceLienRequest
        {

            public string AccountNo { get; set; }
            public string AuthenticationCode { get; set; }
            public string ReferenceID { get; set; }
            public string Reason { get; set; }
            public decimal Amount { get; set; }

        }

        public class UnPlaceLienRequest
        {

            public string AccountNo { get; set; }
            public string AuthenticationCode { get; set; }
            public string ReferenceID { get; set; }
            public string Reason { get; set; }
            public decimal Amount { get; set; }

        }

        public class PlaceLienResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class UnPlaceLienResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class FreezeAccountRequest
        {
            public string AccountNo { get; set; }
            public string AuthenticationCode { get; set; }
        }

        public class UnFreezeAccountRequest
        {
            public string AccountNo { get; set; }
            public string AuthenticationCode { get; set; }
        }

        public class FreezeAccountResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class FreezeStatusResp
        {
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }

        public class UnFreezeAccountResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }
        public class ReversalPayload
        {
            public string RetrievalReference { get; set; }
            public string Token { get; set; }
            public string TransactionDate { get; set; }
            public string TransactionType { get; set; }
            public string Amount { get; set; }



        }

        public class CancelCardRequest
        {

            public string AccountNumber { get; set; }
            public string CardPAN { get; set; }
            public string ReferenceID { get; set; }
            public string HotlistReason { get; set; }
            public string Token { get; set; }

        }

        public class CancelCardResponse
        {
            public bool status { get; set; }
            public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
        }


        public class GetTransactionStatusRequest
        {

            public string RetrievalReference { get; set; }
            public string TransactionDate { get; set; }
            public string Amount { get; set; }
            public string Token { get; set; }
        }

        public class GetTransactionStatusResponse
        {

            public bool IsSuccessful { get; set; }
            public string Status { get; set; }
            //public string StatusDetails { get; set; }
            public bool RequestStatus { get; set; }
            public string ResponseDescription { get; set; }
            public string ResponseStatus { get; set; }
            public string ResponseCode { get; set; }
            public string ResponseMessage { get; set; }
            public string TransactionReference { get; set; }

        }

    public class AccessTransferRequest
    {

        public string debitAccount { get; set; } = "0765908025";
        public string beneficiaryAccount { get; set; } = "0738478524";
        public string beneficiaryName { get; set; } = "IDOMU AFAM HARUNA";
        public string amount { get; set; } = "5";
        public string narration { get; set; } = "Test";
        public string auditId { get; set; } = DateTime.Now.ToString();
        public string appId { get; set; } = "0000-0000";

    }
    //public class GenericResponse
    //{
    //    public bool IsSuccessFul { get; set; }
    //    public string ResponseMessage { get; set; }
    //    public string ResponseCode { get; set; }

    //}
    public class AccessFundsTransferResp : GenericResponse
    {
        public int errorcode { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public Payment payment { get; set; }
        //public bool ResponseStatus { get; set; }

    }

    public class Payment
    {
        public string transactionid { get; set; }
        public int status { get; set; }
        public string information { get; set; }

    }


    public class AccessGetTransactionStatusRequest
    {


        public string paymentAuditId { get; set; } = "08 - Jun - 19 9:28:22 AM";
        public string ftDate { get; set; } = "08-Jun-19";
        public string auditId { get; set; } = DateTime.Now.ToString();
        public string appId { get; set; } = "0000-0000";
    }

    public class AcessGetTransactionStatusResponse
    {

        public int errorcode { get; set; }
        public string message { get; set; }
        public bool success { get; set; }
        public Payment payment { get; set; }
        //public bool ResponseStatus { get; set; }

    }

    public class AcessGetBankAccountNameResponse
    {

        public string accountNumber { get; set; }
        public string accountName { get; set; }
        public string accountCurrency { get; set; }
        public int errorcode { get; set; }
        public string message { get; set; }
        public bool success { get; set; }

    }

    public class PayBillResponse
    {
        public bool IsSuccessFul { get; set; }
        public string ResponseMessage { get; set; }
        public string ResponseCode { get; set; }
    }

    public class TempRequest
    {
        public string name { get; set; }
    }

    public class OperationsResponse<Object> where Object : class
    {
        public bool status { get; set; }
        public string message { get; set; }
        public Object data { get; set; }
    }

    public class ApiLoginViewModel2
    {
    
        //  [EmailAddress]
        //  [Display(Name = "Email")]
        public string username { get; set; }

    
        public string password { get; set; }
       
        public string grant_type { get; set; }
        
        public RegisterDevicePrintDTO device { get; set; }
       
        public LoginType LoginType { get; set; }
        public string FMCToken { get; set; }

    }

    public class RegisterDevicePrintDTO
    {

        
        public string deviceId { get; set; }
       
        public string deviceName { get; set; }
    }
    public enum LoginType { UserIdentity = 1, DeviceIdentity }

    public class ApprovalWorkItemRequest
    {


        public long ID { get; set; }
        public string Name { get; set; }
        public string InitiatedBy { get; set; }
        public decimal Amount { get; set; }
        public string Status { get; set; }
        public string Record { get; set; }
        public string AccountNo { get; set; }
        public int Action { get; set; }
        public long TranID { get; set; }
        public DateTime DateSubmitted { get; set; }
        public DateTime DateTreated { get; set; }
        public string Remark { get; set; }
    }

    public class DisputeActionWorkItem
    {


        public long ID { get; set; }
        public string Approver { get; set; } = "System";
        public string Remark { get; set; } = "Action";
        public string SessionID { get; set; }
        public string InitiatedBy { get; set; }
        public int DisputeAction { get; set; }
        public long TranID { get; set; }
        public DateTime Time { get; set; }
        public DateTime DateTreated { get; set; }
    }

    public class ApproveRequest
    {


        public long ID { get; set; }
        public string Name { get; set; }
        public string Approver { get; set; }
    }

    public class DetailsRequest
    {

        public string RecordID { get; set; }
        public string TransactionType { get; set; }
    }

    public class PNDResponse
    {
        public bool status { get; set; }
        public string StatusDetails { get; set; }
        public bool RequestStatus { get; set; }
        public string ResponseDescription { get; set; }
        public string ResponseStatus { get; set; }
    }
    public class PNDRequest
    {
        public string AccountNo { get; set; }
        public string AuthenticationCode { get; set; }
    }

    public class PNDLog
    {
        public string AccountNo { get; set; }
        public string AccountName { get; set; }
        public string Performer { get; set; }
        public string Time { get; set; }
        public string Action { get; set; }
    }

    public class GenericVM
    {


        public string Status { get; set; }
    }
}






