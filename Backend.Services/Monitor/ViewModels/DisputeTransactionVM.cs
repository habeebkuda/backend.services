﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Monitor.ViewModels
{
    public class DisputeTransactionVM
    {


        [Required]
        [EmailAddress]
        public string TransactionRef { get; set; }

        [Required]
        public string AccountNo { get; set; }

        [Required]
        public decimal Amount { get; set; }

        [Required]
        public string SessionID { get; set; }

        public long TranID { get; set; }

        public DateTime? Time { get; set; } = DateTime.Now;

        public string Channel { get; set; }

        public string DestinationBank { get; set; }

        [Required]
    public long ID { get; set; }


        public decimal AmountField { get; set; }

        public string BeneficiaryBankField { get; set; }

        public string BeneficiaryNameField { get; set; }

        public string BeneficiaryNumberField { get; set; }

        public int ChannelField { get; set; }

        public string CoreRefNumField;

        public System.DateTime DateTimeField { get; set; }

        public string OriginatorNameField { get; set; }

        public string OriginatorNumberField { get; set; }

        public long SNField { get; set; }

        public string SessionIdField { get; set; }
        public string SenderNo { get; set; }
        public string BeneficiaryNo { get; set; }
        public DateTime From { get; set; }
        public DateTime To { get; set; }

        public string SolIdField;

        public int StatusField;

        public string coreResponseField;

        public string coreReversalResponseField;

        public decimal feeField;

        public string nibssRequeryResponseField;

        public string nibssResponseField;

        public int pendingRetrialField;

        public string systemMessageField;

        public string OriginatorBankField;

        public string Remark { get; set; }


    }
}
