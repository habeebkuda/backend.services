﻿
$(document).ready(function () {
    //var c = $("#username");
    //c.text("Damilola2");
    //var main = $("#content");
    //main.on("mouseenter", function () {
    //    main.style = "background-color: #888;";

    //});

    //main.on("mouseleave", function () {
    //    main.style = "";
    //});

    //var menuitems = $("ul.menu li a");
    //menuitems.on("click", function () {
    //    var me = $(this);
    //    alert(me.text());
    //});

/*setTimeout(function(){
    searchInward("121212","09-09-2019","9900990099","10-10-2019");

}, 2000);*/
//searchInward(beneficiaryno,fromdate,senderno,todate)

    var treatModalDialog = {}; 
    $('#utable').DataTable();
    $('.dataTables_length').addClass('bs-select');
    var $sidebarandwrapper = $("#sidebar,#wrapper");
    var $icon = $("#sidebarToggle i.fa")
    $("#sidebarToggle").on("click", function () {
        $sidebarandwrapper.toggleClass("hide-sidebar");
        if ($sidebarandwrapper.hasClass("hide-sidebar")) {
            $icon.removeClass("fa-angle-left");
            $icon.addClass("fa-angle-right");
        }
        else {
            $icon.addClass("fa-angle-left");
            $icon.removeClass("fa-angle-right");
        }
    });
    $(document.forms['selectDialog']).on('submit', function(evt) {
            evt.preventDefault();
            var formData = $(document.forms['selectDialog']).serializeArray();
            console.log(formData);
            alert(JSON.stringify(formData));
        }); 
    });
    $('#submitFrm').click(function(e){
            e.preventDefault();
            e.stopPropagation()
            console.log('Submit clicked');
            treatModalDialog.Action = $('#ddown').val();
            treatModalDialog.Remark = $('#area').val();
            logWorkItem();
            //$(document.forms['selectDialog']).submit();
    });

function upgradeAccount(id) {
    if (confirm("Are you sure you want to upgrade this account to the next tier?")) {
        var dataObj = {
            ID: id,
            approve: true
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/ApproveDocs",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31",
                "Access-Control-Allow-Origin": "http://localhost"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {
            
           alert(response.message);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    }
    return false;
}

function upgradeAccountReject(id) {
    if (confirm("Are you sure you want to reject this request?")) {
        var reason = prompt("Please enter reason for rejection");
        if (reason != null) {

        var dataObj = {
            ID: id,
            approve: false,
            rejectionMessage: reason
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        document.querySelector('.loader').classList.remove('hideLoader');
        document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/ApproveDocs",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {

            alert(response.message);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    }
}
    return false;
}

function retryIDValidation(id) {
    if (confirm("Revalidate this ID?")) {
        var dataObj = {
            smileJobId: id,
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        document.querySelector('.loader').classList.remove('hideLoader');
        document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/RetryIDValidation",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {

            alert(response.message);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    }
    return false;
}

function activatePND(id) {
    if (confirm("Activate Post NO Debit for this account?")) {
        var dataObj = {
            AccountNo: id,
            AuthenticationCode: ""
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        document.querySelector('.loader').classList.remove('hideLoader');
        document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/ActivatePND",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {

            alert(response.ResponseDescription);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    }
    return false;
}

function activatePND2(id,accountname,action,performer) {
        var dataObj = {
            AccountNo: id,
            AuthenticationCode: "",
            AccountName: accountname,
            Performer: performer,
            Action: action
        }
     //   document.getElementById('id_' + id).setAttribute("disabled", "disabled");
       // document.querySelector('.loader').classList.remove('hideLoader');
        //document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "http://kudabankmonitor.azurewebsites.net/api/UtilService/LogPND",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings);
        //});
    
    return false;
}

function activatePND3(id,accountname,action,performer) {
    if (confirm("Activate Post NO Debit for this account?")) {
        var dataObj = {
            AccountNo: id,
            AuthenticationCode: ""
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        document.querySelector('.loader').classList.remove('hideLoader');
        document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/ActivatePND",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {
         activatePND2(id,accountname,action,performer)
            alert(response.ResponseDescription);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    }
    return false;
}


function getPNDStatus(id) {
        var dataObj = {
            AccountNo: id,
            AuthenticationCode: ""
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        document.querySelector('.loader').classList.remove('hideLoader');
        document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/CheckPNDStatus",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {

            alert(response.ResponseDescription);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    
    return false;
}

function deactivatePND(id) {
    if (confirm("REMOVE PND for this account?")) {
        var dataObj = {
            AccountNo: id,
            AuthenticationCode: ""
        }
        document.getElementById('id_' + id).setAttribute("disabled", "disabled");
        document.querySelector('.loader').classList.remove('hideLoader');
        document.querySelector('.loader').classList.add('showLoader');
        var settings = {
            "async": true,
            "crossDomain": true,
            "url": "https://kudabankcore.azurewebsites.net/api/Utility/DeactivatePND",
            "method": "POST",
            "headers": {
                "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                "cache-control": "no-cache",
                "Content-Type": "application/json",
                "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
            },
            "data": JSON.stringify(dataObj),
            "error": () => {
                document.getElementById('id_' + id).removeAttribute("disabled");
            }
        }

        $.ajax(settings).done(function (response) {

            alert(response.ResponseDescription);
            var url = $("#RedirectTo").val();
            location.href = url;
        });
    }
    return false;
}
function setDefaultPin(id) {
    if (confirm("Set Default CARD PIN for this account?")) {
        var pin = prompt("Please enter the default PIN");
        if (pin != null) {
            var dataObj = {
                accountNumber: id,
                defaultPin: pin
            }
            document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://kudabankcore.azurewebsites.net/api/Utility/UpdateDefaultPin",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }
    }
    return false;
}

function placeLIEN(id) {
    if (confirm("Place LIEN on this account?")) {
        var amount = prompt("Please enter the amount");
        if (pin != null) {
            var dataObj = {
                AccountNo: id,
                Amount: amount
            }
            document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://kudabankcore.azurewebsites.net/api/Utility/PlaceLien",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }
    }
    return false;
}

function unplaceLIEN(id) {
    if (confirm("Unplace LIEN on this account?")) {
        var amount = prompt("Please enter the amount");
        if (pin != null) {
            var dataObj = {
                AccountNo: id,
                Amount: amount
            }
            document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://kudabankcore.azurewebsites.net/api/Utility/UnplaceLien",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }
    }
    return false;
}
function reverseTransaction(id) {
    if (confirm("Reverse this item?")) {
        var amount = prompt("Please enter the amount");
        if (pin != null) {
            var dataObj = {
                AccountNo: id,
                Amount: amount
            }
            document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://kudabankmonitor.azurewebsites.net/api/Service/isPrivileged",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }
    }
    return false;
}

function showTreatModal(tranid,name,record,initiatedby,amount,accountno,remark, tranref,resp, reqresp, status,sn,fee,coreresp,corereversal,destination,systemmessage,pendingretrial,channel) {
    
        treatModalDialog = {
            Name: name,
            Amount: amount,
            Record: record,
            InitiatedBy: initiatedby,
            Action: 0,
            TranID: tranid,
            Remark: '',
            AccountNo: accountno
        }
 $('#area').text(remark);
        $('#tranref').text(tranref);
        $('#resp').text(resp);
        $('#reqresp').text(reqresp);
        $('#status').text(status);
        $('#sn').text(sn);
        $('#fee').text(fee);
        $('#coreresp').text(coreresp);
$('#corereversal').text(corereversal);
$('#destination').text(destination);
$('#systemmessage').text(systemmessage);
$('#pendingretrial').text(pendingretrial);
$('#channel').text(channel);

        $('#modelId1').modal('show');

        
}

function showMoreDetails(remark, tranref,resp, reqresp, status,sn,fee,coreresp,corereversal,destination,systemmessage,pendingretrial,channel) {
    
        //treatModalDialog = {
            //Name: name,
          //  Amount: amount,
           // Record: record,
          //  InitiatedBy: initiatedby,
          //  Action: 0,
          //  TranID: tranid,
          //  Remark: '',
          //  AccountNo: accountno
        //}
        $('#area').text(remark);
        $('#tranref').text(tranref);
        $('#resp').text(resp);
        $('#reqresp').text(reqresp);
        $('#status').text(status);
        $('#sn').text(sn);
        $('#fee').text(fee);
        $('#coreresp').text(coreresp);
$('#corereversal').text(corereversal);
$('#destination').text(destination);
$('#systemmessage').text(systemmessage);
$('#pendingretrial').text(pendingretrial);
$('#channel').text(channel);
            
        $('#modelId').modal('show');

        
}

function showMoreDetailsPendingApproval(remark, tranref,resp, reqresp, status,sn,fee,coreresp,id,action,user,corereversal,destination,systemmessage,pendingretrial,channel,bname,bno,bbank,sname,sno,sbank) {
    
        //treatModalDialog = {
            //Name: name,
          //  Amount: amount,
           // Record: record,
          //  InitiatedBy: initiatedby,
          //  Action: 0,
          //  TranID: tranid,
          //  Remark: '',
          //  AccountNo: accountno
        //}
        $('#area').text(remark);
        $('#tranref').text(tranref);
        $('#resp').text(resp);
        $('#reqresp').text(reqresp);
        $('#status').text(status);
        $('#sn').text(sn);
        $('#fee').text(fee);
        $('#coreresp').text(coreresp);
$('#corereversal').text(corereversal);
$('#destination').text(destination);
$('#systemmessage').text(systemmessage);
$('#pendingretrial').text(pendingretrial);
$('#channel').text(channel);
$('#bname').text(bname);
$('#bno').text(bno);
$('#bbank').text(bbank);
$('#sname').text(sname);
$('#sno').text(sno);
$('#sbank').text(sbank);
        $('#id').val(id);
         $('#action').val(action);
        $('#user').text(user);
            
        $('#modelId').modal('show');

        
}


function showDetailsModal(recordid,recordtype) {
    
if(recordtype == "Inward") {
           var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://kudabankmonitor.azurewebsites.net/api/UtilService/GetFailedInwardDetails",
        "method": "POST",
        "headers": {
            "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
            "cache-control": "no-cache",
            "Content-Type": "application/json",
            "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31",
        },
        "data": JSON.stringify(treatModalDialog),
        "error": () => {
            document.getElementById('id_' + record).removeAttribute("disabled");
        }
    }

    $.ajax(settings).done(function (response) {

        alert(response.message);
        //$('#area').text(remark);
        //$('#tranref').text(tranref);
        //$('#resp').text(resp);
        //$('#reqresp').text(reqresp);
        //$('#status').text(status);
        //$('#sn').text(sn);
        //$('#fee').text(fee);
        //$('#coreresp').text(coreresp);
        //var url = $("#RedirectTo").val();
        //location.href = url;
    });

        $('#modelInward').modal('show');
}

        
}

function approveOnModal()
{


approveWorkItem2($('#id').val(),$('#action').val(),$('#user').val());

}

function rejectOnModal()
{


rejectWorkItem($('#id').val(),$('#action').val(),$('#user').val());

}

function showRemark(remark) {
    
        //treatModalDialog = {
            //Name: name,
          //  Amount: amount,
           // Record: record,
          //  InitiatedBy: initiatedby,
          //  Action: 0,
          //  TranID: tranid,
          //  Remark: '',
          //  AccountNo: accountno
        //}
        $('#area').text(remark);
            
        $('#modelId1').modal('show');

        
}

function logWorkItem() {

    //document.getElementById('id_' + record).setAttribute("disabled", "disabled");
    //document.querySelector('.loader').classList.remove('hideLoader');
    //document.querySelector('.loader').classList.add('showLoader');
    var settings = {
        "async": true,
        "crossDomain": true,
        "url": "http://kudabankmonitor.azurewebsites.net/api/UtilService/CreateWorkItem",
        "method": "POST",
        "headers": {
            "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
            "cache-control": "no-cache",
            "Content-Type": "application/json",
            "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31",
        },
        "data": JSON.stringify(treatModalDialog),
        "error": () => {
            document.getElementById('id_' + record).removeAttribute("disabled");
        }
    }

    $.ajax(settings).done(function (response) {

        alert(response.message);
        var url = $("#RedirectTo").val();
        location.href = url;
    });
}

function approveWorkItem(id,name,approver) {
    if (confirm("Approve this item")) {
            var dataObj = {
                Name: name,
                ID: id,
                Approver: approver
            }
            document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://kudabankmonitor.azurewebsites.net/api/UtilService/ApproveWorkItem",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }

 return false;
    }

function approveWorkItem2(id,name,approver) {
    if (confirm("Approve this item")) {
            var dataObj = {
                Name: name,
                ID: id,
                Approver: approver
            }
            document.getElementById('id').setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://kudabankmonitor.azurewebsites.net/api/UtilService/ApproveWorkItem",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }

 return false;
    }


function rejectWorkItem(id,name,approver) {
    if (confirm("Reject this item")) {
            var dataObj = {
                Name: name,
                ID: id,
                Approver: approver
            }
            document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            document.querySelector('.loader').classList.remove('hideLoader');
            document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://kudabankmonitor.azurewebsites.net/api/UtilService/RejectWorkItem",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": () => {
                    document.getElementById('id_' + id).removeAttribute("disabled");
                }
            }

            $.ajax(settings).done(function (response) {

                alert(response.message);
                var url = $("#RedirectTo").val();
                location.href = url;
            });
        }

 return false;
    }


function searchInward(beneficiaryno,senderno,todate,fromdate) {
            var dataObj = {
                BeneficiaryNo: beneficiaryno,
                From: fromdate,
                SenderNo: senderno,
                To:todate
            }
            //document.getElementById('id_' + id).setAttribute("disabled", "disabled");
            //document.querySelector('.loader').classList.remove('hideLoader');
            //document.querySelector('.loader').classList.add('showLoader');
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "http://localhost/servicemonitor/api/UtilService/InwardTransfers",
                "method": "POST",
                "headers": {
                    "Token": "DtoZ/6nlTmu0iQxvz0n4ocyIF/1r65VBvdYFry2GI1+6xemWh6W2fmVS0wFpimX4s0YtmsmPrxtjVhan2Www8MSodNDuIeeUjvtJ0AAteJw=",
                    "cache-control": "no-cache",
                    "Content-Type": "application/json",
                    "Postman-Token": "18559414-e96b-4610-a198-64cdcd961e31"
                },
                "data": JSON.stringify(dataObj),
                "error": (e) => {
console.log(e);
                }
            }

            $.ajax(settings).done(function (response) {
                
                //alert(response.message);
                //var url = $("#RedirectTo").val();
                //location.href = url;
            });
        

 return false;
    }
   



